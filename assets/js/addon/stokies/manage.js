
$(document).ready(function(){
	App.initHelpers(['datepicker']);
	$('#noid').mask('aaa99999');
	$(".number").number(true,0);
	$("#noidcabang").select2();
	$("#kodebank2").select2();
	// $('.number').number( true, 0 );
		
		
	
	if ($('#haverec').is(":checked"))
	{
		var radio=document.getElementsByName("kodebank");
		   var len=radio.length;
		   for(var i=0;i<len;i++)
		   {
			   radio[i].disabled=false;
		   }
	}else{
		disable_rek();
	}

	$('input[name="kodebank"]').change(function(e) { // Select the radio input group
		var inputan=$(this).val();
		if (inputan=='5'){
			$("#kodebank2").attr('disabled', false).trigger("liszt:updated");
		}else{
			$("#kodebank2").attr('disabled', true).trigger("liszt:updated"); 					
			
		}
	});

	$("#kodebank2").change(function(){
		if($(this).val() != ''){
			$("#lblbank_err").hide();
		}else{
			$("#lblbank_err").show();
		}
	});


})
$('#haverec').click(function(){
    if($(this).is(':checked')){
        enable_rek();
    } else {
        disable_rek();
    }
});
$('.angka').keydown(function(event){
	keys = event.keyCode;
	// Untuk: backspace, delete, tab, escape, and enter
	if (event.keyCode == 116 || event.keyCode == 46 || event.keyCode == 188 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || 
		 // Untuk: Ctrl+A
		(event.keyCode == 65 && event.ctrlKey === true) || 
		 // Untuk: home, end, left, right
		(event.keyCode >= 35 && event.keyCode <= 39) || event.keyCode == 190 || event.keyCode == 110|| event.keyCode == 188) {
			 // melanjutkan untuk memunculkan angka
			return;
	}
	else {
		// jika bukan angka maka tidak terjadi apa-apa
		if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
			event.preventDefault(); 
		}   
	}
    
});
function disable_rek(){
	var radio=document.getElementsByName("kodebank");
	var len=radio.length;
	for(var i=0;i<len;i++)
	{
	   radio[i].disabled=true;
	}
	$("#norek").attr('disabled', true);
	$("#atasnama").attr('disabled', true);
	$("#cabang").attr('disabled', true);
}
function enable_rek(){
	var radio=document.getElementsByName("kodebank");
	var len=radio.length;
	for(var i=0;i<len;i++)
	{
	   radio[i].disabled=false;
	}
	$("#norek").attr('disabled', false);
	$("#atasnama").attr('disabled', false);
	$("#cabang").attr('disabled', false);
}

$("#namastokies").blur(function(){
	$("#div_loading").show();
	if ($(this).val()!='' && $(this).val().length >= 3){
		var values = $(this).val();
		// var title=jQuery('#namastokies').val();
		$.ajax({
		url: site_url+'stokies/find_stokis/',
		dataType: "JSON",
		method: "POST",
		data : {nama:values},
		success: function(data) {
			console.log(data);
			if (data=='ok'){
				$("#div_ok").show();
				$("#div_error").hide();
			}else{
				$("#div_ok").hide();
				$("#div_error").show();
			}
			$("#div_loading").hide();
		}
		});
	}   
});
$("#noid").blur(function(){
	
	if ($(this).val()!=''){
		var values = $(this).val();
		// var title=jQuery('#namastokies').val();
		$.ajax({
		url: site_url+'stokies/find_noid/',
		dataType: "JSON",
		method: "POST",
		data : {cari:values},
		success: function(data) {
			// console.log(data);
			if (data=='error'){
				$("#div_info").html('<button class="js-swal-error btn btn-danger" type="button"><i class="fa fa-times push-5-r"></i> Nama Member Tidak Ditemukan</button>');
			}else{
				$("#div_info").html('<button class="js-swal-success btn btn-success" type="button"><i class="fa fa-check push-5-r"></i>'+data+'</button>');
			}
			$("#div_loading").hide();
		}
		});
	}else{
		
	}   
});

$("#kota").select2({
	minimumInputLength: 2,
	noResults: 'Tidak Ditemukan.',          
	// allowClear: true
	// tags: [],
	ajax: {
		url: site_url+'All/s2_kota/',
		dataType: 'json',
		type: "POST",
		quietMillis: 50,
		
	 data: function (params) {
		  var query = {
			search: params.term,                
		  }
		  return query;
		}, 
		processResults: function (data) {
			return {
				results: $.map(data, function (item) {
					return {
						text: item.kota + ' - ('+item.propinsi+')',
						id: item.idkota
					}
				})
			};
		}
	}
});

var BaseFormValidation = function() {
    // Init Bootstrap Forms Validation, for more examples you can check out https://github.com/jzaefferer/jquery-validation
    var initValidationBootstrap = function(){
        jQuery('.js-validation-bootstrap').validate({
            ignore: [],
            errorClass: 'help-block animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group > div').append(error);
            },
            highlight: function(e) {
                var elem = jQuery(e);

                elem.closest('.form-group').removeClass('has-error').addClass('has-error');
                elem.closest('.help-block').remove();
            },
            success: function(e) {
                var elem = jQuery(e);

                elem.closest('.form-group').removeClass('has-error');
                elem.closest('.help-block').remove();
            },
            rules: {
                'namastokies': {
                    required: true,
                    minlength: 3
                },
                'alamat': {
                    required: true,
                    minlength: 3
                },
                'kota': {
                    required: true
                    
                },
				'noidcabang': {
                    required: true
                    
                },
				'noid': {
                    required: true
                    
                },
                'email': {
                    required: true,
                    email: true
                },
                'hp': {
                    required: true,
                    minlength: 9
                }
                
            },
            messages: {
                'namastokies': {
                    required: 'Harus diisi',
                    minlength: 'Minimal 3 Karakter'
                },
                'alamat': {
                    required: 'Harus diisi',
                    minlength: 'Minimal 3 Karakter'
                },
                'kota': {
                    required: 'Harus diisi',
                    
                },
				'noidcabang': {
                    required: 'Harus diisi',
                    
                },
				'noid': {
                    required: 'Harus diisi',
                    
                },
                'email': 'Silahkan Isi Email dengan benar',
                'hp': {
                    required: 'Silahkan Isi HP ',
                    minlength: 'Minimal no HP 10 Digit'
                }
            }
        });
    };

    // Init Material Forms Validation, for more examples you can check out https://github.com/jzaefferer/jquery-validation
    var initValidationMaterial = function(){
        jQuery('.js-validation-material').validate({
            ignore: [],
            errorClass: 'help-block text-right animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group > div').append(error);
            },
            highlight: function(e) {
                var elem = jQuery(e);

                elem.closest('.form-group').removeClass('has-error').addClass('has-error');
                elem.closest('.help-block').remove();
            },
            success: function(e) {
                var elem = jQuery(e);

                elem.closest('.form-group').removeClass('has-error');
                elem.closest('.help-block').remove();
            },
            rules: {
                'namastokies': {
                    required: true,
                    minlength: 3
                },
                'alamat': {
                    required: true,
                    minlength: 3
                },
                'kota': {
                    required: true
                    
                },
				'noidcabang': {
                    required: true
                    
                },
				'noid': {
                    required: true
                    
                },
                'email': {
                    required: true,
                    email: true
                },
                'hp': {
                    required: true,
                    minlength: 9
                }
            },
            messages: {
                'namastokies': {
                    required: 'Harus diisi',
                    minlength: 'Minimal 3 Karakter'
                },
                'alamat': {
                    required: 'Harus diisi',
                    minlength: 'Minimal 3 Karakter'
                },
                'kota': {
                    required: 'Harus diisi',
                    
                },
				'noidcabang': {
                    required: 'Harus diisi',
                    
                },
				'noid': {
                    required: 'Harus diisi',
                    
                },
                'email': 'Silahkan Isi Email dengan benar',
                'hp': {
                    required: 'Silahkan Isi HP ',
                    minlength: 'Minimal no HP 10 Digit'
                }
            }
        });
    };

    return {
        init: function () {
            // Init Bootstrap Forms Validation
            initValidationBootstrap();

            // Init Material Forms Validation
            initValidationMaterial();

            // Init Validation on Select2 change
            jQuery('.js-select2').on('change', function(){
                jQuery(this).valid();
            });
        }
    };
}();

// Initialize when page loads
jQuery(function(){ BaseFormValidation.init(); });
var table;
$(document).ready(function(){
	// alert('masuk sini');

// $('#datatable_index tfoot th').each( function () {
        
        // var title = $(this).text();
        // $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    // });
	
table = $('#datatable_index').DataTable({
    "pageLength": 10,  
    "processing": true,
    "serverSide": true,
    "stateSave": true,
    "order": [ 1, 'ASC' ],
    "ajax": { "url": site_url + ajax_url, "type": "POST" },
    "columns": [
        {"data": "noid", visible: true},
        {"data": "namamembers", searchable : true, orderable: true},
        {"data": "namakota"},
        {"data": "tgldaftar", searchable : false},
        {"data": "downline"},
        {"data": "nilai", searchable : false, orderable: false, className : 'not_search'},
        {"data": "noupline"},
        {"data": "action", searchable : false, orderable: false, className : 'not_search'},
       
    ],
    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

        var action = '<div class="btn-group"><button data-toggle="dropdown" class="btn btn-primary">Action <span class="caret"></span></button><ul class="dropdown-menu">';
        action += '<li><a href="'+ site_url +'mmember/editor/' + aData['noid'] + '">Edit</a></li>';
        action += '<li class="divider"></li>';
        action += '<li><a href="'+ site_url +'mmember/viewnet/' + aData['noid'] + '">Jaringan</a></li>';
        action += '<li><a href="'+ site_url +'mmember/viewinfonet/' + aData['noid'] + '">Info Jaringan</a></li>';
		action += '<li class="divider"></li>';
        action += '<li><a href="'+ site_url +'mmember/viewkomisi/' + aData['noid'] + '">Bonus Belanja</a></li>';
        action += '<li><a href="'+ site_url +'mmember/viewkomisinet/' + aData['noid'] + '">Bonus Leveling</a></li>';
        action += '<li><a href="'+ site_url +'mmember/viewkomisiother/' + aData['noid'] + '">Bonus Lainnya</a></li>';
		action += '<li class="divider"></li>';
        action += '<li><a href="'+ site_url +'mmember/belanja/' + aData['noid'] + '">Belanja</a></li>';
		action += '</ul></div>'							
								
        action += '</div>';

        $("td:eq(7)", nRow).html(action);
		
		var nilai=cek_nilaitotal(aData['downline'],1);
		$("td:eq(5)", nRow).html(nilai);
        return nRow;            
    }        
})


$('#datatable_index_filter input').unbind();
$('#datatable_index_filter input').bind('keyup', function(e) {
   if(e.keyCode == 13) {
    table.search(this.value).draw();   
   }
}); 

$("#refresh_list").click(function(){
    table.state.clear();
    window.location.reload();
});        


	function cek_nilaitotal($tot,$nol=0)
	{	
		$nilainol='<img src="'+site_url+'assets/img/rank/starjemred.png'+'">';
		$nilaijem='<img src="'+site_url+'assets/img/rank/starjem.png'+'">';
		$nilaibin='<img src="'+site_url+'assets/img/rank/starrank.png'+'"> ';
		$nilaigol='<img src="'+site_url+'assets/img/rank/stargold.png'+'"> ';			
		$jml='';
		if (parseFloat($tot)>0){	
			
			
			if (parseFloat($tot)>0 && parseFloat($tot)<=5){
				$jml =$nilaijem;
				
			}else if (parseFloat($tot)>5 && parseFloat($tot)<=30){
				// $jml = str_repeat($nilaijem, 2); 
				$jml = $nilaijem + $nilaijem;
				
			}else if (parseFloat($tot)>30 && parseFloat($tot)<=70){
				$jml = $nilaijem+$nilaijem+$nilaijem;
				
			}else if (parseFloat($tot)>70 && parseFloat($tot)<=134){
				// $jml = str_repeat($nilaijem, 4);
				$jml = $nilaijem+$nilaijem+$nilaijem+$nilaijem;
			}else if (parseFloat($tot)>134 && parseFloat($tot)<=236){
				// $jml = str_repeat($nilaijem, 5);
				$jml = $nilaijem+$nilaijem+$nilaijem+$nilaijem+$nilaijem;
			}else if (parseFloat($tot)>236 && parseFloat($tot)<=402){
				$jml = $nilaibin;
				
			}else if (parseFloat($tot)>402 && parseFloat($tot)<=669){
				// $jml = str_repeat($nilaibin, 2);
				$jml = $nilaibin + $nilaibin;
			}else if (parseFloat($tot)>669 && parseFloat($tot)<=1100){
				// $jml = str_repeat($nilaibin, 3);
				$jml = $nilaibin + $nilaibin + $nilaibin;
				
			}else if (parseFloat($tot)>1100 && parseFloat($tot)<=1797){
				// $jml = str_repeat($nilaibin, 4);
				$jml = $nilaibin + $nilaibin + $nilaibin + $nilaibin;
			}else if (parseFloat($tot)>1797 && parseFloat($tot)<=2924){
				// $jml = str_repeat($nilaibin, 5);
				$jml = $nilaibin + $nilaibin + $nilaibin + $nilaibin + $nilaibin;
			}else if (parseFloat($tot)>2924 && parseFloat($tot)<=4747){
				$jml = $nilaigol;
				
			}else if (parseFloat($tot)>4747 && parseFloat($tot)<=7696){
				// $jml = str_repeat($nilaigol, 2);
				$jml = $nilaigol + $nilaigol;
			}else if (parseFloat($tot)>7696 && parseFloat($tot)<=12465){
				// $jml = str_repeat($nilaigol, 3);
				$jml = $nilaigol + $nilaigol + $nilaigol;
			}else if (parseFloat($tot)>12465 && parseFloat($tot)<=20181){
				// $jml = str_repeat($nilaigol, 4);
				$jml = $nilaigol + $nilaigol + $nilaigol + $nilaigol;	
			}else if (parseFloat($tot)>20181){
				// $jml = str_repeat($nilaigol, 5);
				$jml = $nilaigol + $nilaigol + $nilaigol + $nilaigol + $nilaigol;						
			}
		}else{
			if ($nol){$jml =$nilainol;}else{$jml = '';}
		}    
	return $jml;		
	}
})



// datatable
function get_datatable(id,url) {
    $(id).DataTable({
        "pageLength": 10, 
        "ordering": true, 
        "processing": true, 
        "serverSide": true, 
        "order": [],
        "ajax": { "url": url, "type": "POST" },
        "columnDefs": [ { "targets": [ 0 ], "visible": false }, { "targets": [ 1 ], "orderable": true } ]
    })
}

function add_select(id, form_label, form_id, data) {
    var html = '';
    html    += '<div class="form-group c_select" id="f'+form_id+'">';
    html    += '<label class="col-md-3 control-label">'+form_label+'</label>';
    html    += '<div class="col-md-6">';
    html    += '<select id="'+form_id+'" class="select form-control" data-live-search="true" style="width: 100%;" data-placeholder="Choose one..">';
    html    +=  data;
    html    += '</select>';
    html    += '</div></div>';
    $(id).append(html);
}

function add_input(id,label,form_id,form_value,form_type='text',icon='fa-pencil') {
    var html = '';
    html    += '<div class="form-group c_input" id="f'+form_id+'">';
    html    += '<label class="col-md-3 control-label" for="'+form_id+'">'+label+'</label>';
    html    += '<div class="col-md-6">';
    html    += '<div class="input-group">';
    html    += '<span class="input-group-addon"><i class="fa '+icon+'"></i></span>';
    html    += '<input type="'+form_type+'" class="form-control" id="'+form_id+'" placeholder="'+label+'" name="'+form_id+'" value="'+form_value+'" required="" aria-required="true">';
    html    += '</div></div></div>';
    return $(id).append(html);
}

function removeClass(id) {
	return $(id).remove()
}

function disabled(id) {
	return $(id).attr('disabled',true)
}

function select_refresh(id) {
	return $(id).selectpicker('refresh')
}

function focus(id) {
	return $(id).focus()
}

function hidden(id) {
	return $(id).hide()
}

function append(id,data) {
    return $(id).append(data)
}

function value(id,data) {
    return $(id).val(data)
}

function empty(id,data) {
    return $(id)
}

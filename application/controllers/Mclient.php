<?php
defined('BASEPATH') OR exit('Hacking attempt: Out of System!');

  /**
   * Develop by Deni Purnama
   * denipurnama371@gmail.com
   */
class Mclient extends CI_Controller {
private $table       = 'clients'; 
private $tblClients  = 'clients';
private $tblLoading  = 'kit_loading';
private $jTable1     = 'kit_propinsi'; 
private $jTable2     = 'kit_kabupaten';
public $field        = 'client_';
public $folder       = 'Mclient';
public $label        = 'Clients';
public $link         = 'client';
    public function __construct()
    {
        parent::__construct();
        // PermissionUserLoggedIn($this->session);
            $this->load->model($this->folder.'_model','model');
            // $_SESSION['logged_in']=false;
            // $this->form_validation->set_error_delimiters('<label>', '</label>');
    }
    
    function index()
    {
exit('Hacking attempt: Out of System!');
    }

    function showingData()
    {
// $data array() for basic HTML
        $data = array();
        $data['title']          = 'Setting - '.$this->label;
        $data['template']       = $this->folder.'/index';
        $data['tJudul']         = $this->label;
        $data['dJudul']         = $this->link;
        $data['url_index']      = site_url().'setting/'.$this->link;
        $data['url_addnew']     = site_url().'setting/new-'.$this->link;
        $data['url_kedua']      = 'new-'.$this->link;
        $data['url_ajax']       = site_url().'ajax/'.$this->link;
        $data['url_buyer']      = site_url().'ajax/buyer';
        $data['url_supplier']   = site_url().'ajax/supplier_';
        $data['url_sortimen']   = site_url().'ajax/sortimen_';
        $data['url_produk']     = site_url().'ajax/produk_';
        $data['url_wip']        = site_url().'ajax/wip_';
        $data['url_delete']     = site_url().'ajax/delClient';
        $data['delete_row']     = site_url().'ajax/delRow';
        $data['url_uStatus']    = site_url().'ajax/upStatusClient';
        $data['url_saveSetting']= site_url().'setting/saveSetting';
        $data['json_detail1']   = site_url().'json/detailclient1/';
        $data['json_detail2']   = site_url().'json/detailclient2/';
        $data['json_buyer']     = site_url().'json/buyer';
        $data['json_sortimen']  = site_url().'json/sortimen';
        $data['json_supplier_'] = site_url().'json/supplier';
        $data['json_wip']       = site_url().'json/wip';
        $data['json_produk']    = site_url().'json/produk';
        $data['breadcrum']      = array(
                                array("Area Admin",'#'),
                                array($this->label,''),
                                array("List",'setting/'.$this->link)
                              );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    function insertBaru()
    {
// $data array() for basic HTML
        $data = array();
        $data['title']            = 'Setting - New '.$this->label;
        $data['template']         = $this->folder.'/manage';
        $data['tJudul']           = $this->label;
        $data['dJudul']           = $this->link;
        $data['url_index']        = site_url().'setting/'.$this->link;
        $data['url_addnew']       = site_url().'setting/new-'.$this->link;
        $data['url_kedua']        = 'new-'.$this->link;
        $data['url_ajax']         = site_url().'ajax/'.$this->link;
        $data['json_provinsi']    = site_url().'json/provinsi';
        $data['json_kabupaten']   = site_url().'json/kabupaten';
        $data['json_users']       = site_url().'json/users';
        $data['json_table']       = site_url().'json/iui';
        $data['url_proses']       = site_url().'setting/new-'.$this->link.'/proses';
        $data['breadcrum']        = array(
                                array("Area Admin",'#'),
                                array($this->label,'#'),
                                array("New",'setting/new-'.$this->link)
                              );

// $data array() for value database
//         $Lid=getLastID($this->table,$this->field.'id');
// $data['hide-ID']=encryptURL($Lid[$this->field.'id']+1);
$data['new'.$this->field.'nick']='';
$data['new'.$this->field.'nama']='';
$data['new'.$this->field.'alamat']='';
        for($p=1;$p<=5;$p++){
$data['new'.$this->field.'alamatpabrik'.$p]      = '';
                            }
$data['new'.$this->field.'telp']='';
$data['new'.$this->field.'fax']='';
$data['new'.$this->field.'email']='';
$data['new'.$this->field.'website']='';
// Contact Person
$data['new'.$this->field.'namacp']='';
$data['new'.$this->field.'telpcp']='';
$data['new'.$this->field.'emailcp']='';
$data['new'.$this->field.'jabatancp']='';
// Divisi
$data['new'.$this->field.'namadv']='';
$data['new'.$this->field.'telpdv']='';
$data['new'.$this->field.'emaildv']='';
$data['new'.$this->field.'jabatandv']='';
// Data Perijinan
$data['new'.$this->field.'aktapendirian']='';
$data['new'.$this->field.'aktaperubahan']='';
    // SIUP
$data['new'.$this->field.'siup']='';
$data['new'.$this->field.'siuptgl']='';
$data['new'.$this->field.'siuptglkadaluarsa']='';
    // TDP
$data['new'.$this->field.'tdp']='';
$data['new'.$this->field.'tdptgl']='';
$data['new'.$this->field.'tdptglkadaluarsa']='';
    // NPWP
$data['new'.$this->field.'npwp']='';
$data['new'.$this->field.'npwpkantor']='';
$data['new'.$this->field.'npwpsppkp']='';
$data['new'.$this->field.'npwpskt']='';
    // ETPIK
$data['new'.$this->field.'etpik']='';
$data['new'.$this->field.'etpiktgl']='';
$data['new'.$this->field.'etpikproduk']='';

$data['new'.$this->field.'sertifikat']='';
$data['newstatus']='';
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    function indexUpdate()
    {
$id=decryptURL($this->uri->segment(3));
$get=rowWhere($this->field.'id',$id,$this->table);
$getP=rowWhere('propid',$get->client_propinsi,$this->jTable1);
$getK=rowWhere('kabid',$get->client_kabupaten,$this->jTable2);
$getU=rowArray('users',array('user_id'=>$get->client_userid));
// $data array() for basic HTML
        $data = array();
        $data['title']          = 'Setting - Edit '.$this->label;
        $data['template']       = $this->folder.'/manage';
        $data['tJudul']         = $this->label;
        $data['dJudul']         = $this->link;
        $data['url_index']      = site_url().'setting/'.$this->link;
        $data['url_addnew']     = site_url().'setting/new-'.$this->link;
        $data['url_kedua']      = 'new-'.$this->link;
        $data['url_ajax']       = site_url().'ajax/'.$this->link;
        $data['json_provinsi']    = site_url().'json/provinsi';
        $data['json_kabupaten']   = site_url().'json/kabupaten';
        $data['json_users']       = site_url().'json/users';
        $data['json_table']       = site_url().'json/iui';
        $data['url_proses']     = site_url().'setting/update-'.$this->link.'/proses';
        $data['breadcrum']      = array(
                                array("Area Admin",'#'),
                                array($this->label,'#'),
                                array("Edit",'setting/'.$this->link.'/'.$this->uri->segment(3))
                              );

// $data array() for value database
$data['new'.$this->field.'nick']                =$get->client_nick;
$data['new'.$this->field.'nama']                =$get->client_nama;
$data['new'.$this->field.'alamat']              =$get->client_alamat;
$data['new'.$this->field.'alamatpabrik1']       =$get->client_alamatpabrik1;
$data['new'.$this->field.'alamatpabrik2']       =$get->client_alamatpabrik2;
$data['new'.$this->field.'alamatpabrik3']       =$get->client_alamatpabrik3;
$data['new'.$this->field.'alamatpabrik4']       =$get->client_alamatpabrik4;
$data['new'.$this->field.'alamatpabrik5']       =$get->client_alamatpabrik5;
$data['new'.$this->field.'telp']                =$get->client_telp;
$data['new'.$this->field.'fax']                 =$get->client_fax;
$data['new'.$this->field.'email']               =$get->client_email;
$data['new'.$this->field.'website']             =$get->client_website;
$data['new'.$this->field.'propinsi']            =$get->client_propinsi.','.$getP->nama;
$data['new'.$this->field.'kabupaten']           =$get->client_kabupaten.','.$getK->nama;
// Contact Person
$data['new'.$this->field.'namacp']              =$get->client_namacp;
$data['new'.$this->field.'telpcp']              =$get->client_telpcp;
$data['new'.$this->field.'emailcp']             =$get->client_emailcp;
$data['new'.$this->field.'jabatancp']           =$get->client_jabatancp;
// Divisi
$data['new'.$this->field.'namadv']              =$get->client_namadv;
$data['new'.$this->field.'telpdv']              =$get->client_telpdv;
$data['new'.$this->field.'emaildv']             =$get->client_emaildv;
$data['new'.$this->field.'jabatandv']           =$get->client_jabatandv;
// Data Perijinan
$data['new'.$this->field.'aktapendirian']       =$get->client_aktapendirian;
$data['new'.$this->field.'aktaperubahan']       =$get->client_aktaperubahan;
    // SIUP
$data['new'.$this->field.'siup']                =$get->client_siup;
$data['new'.$this->field.'siuptgl']             =encode_date($get->client_siuptgl);
$data['new'.$this->field.'siuptglkadaluarsa']   =encode_date($get->client_siuptglkadaluarsa);
    // TDP
$data['new'.$this->field.'tdp']                 =$get->client_tdp;
$data['new'.$this->field.'tdptgl']              =encode_date($get->client_tdptgl);
$data['new'.$this->field.'tdptglkadaluarsa']    =encode_date($get->client_tdptglkadaluarsa);
    // NPWP
$data['new'.$this->field.'npwp']                =$get->client_npwp;
$data['new'.$this->field.'npwpkantor']          =$get->client_npwpkantor;
$data['new'.$this->field.'npwpsppkp']           =$get->client_npwpsppkp;
$data['new'.$this->field.'npwpskt']             =$get->client_npwpskt;
    // ETPIK
$data['new'.$this->field.'etpik']               =$get->client_etpik;
$data['new'.$this->field.'etpiktgl']            =encode_date($get->client_etpiktgl);
$data['new'.$this->field.'etpikproduk']         =$get->client_etpikproduk;

$data['new'.$this->field.'sertifikat']          =$get->client_sertifikat;
$data['new'.$this->field.'userid']              =$get->client_userid.','.$getU['name'].' ['.$getU['user_name'].']';
$data['newstatus']='';
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

function saveNew(){
$insert_id=$this->model->addNewItem();
        if($insert_id){
$msg='ToastrSukses("'.$this->label.' baru telah ditambahkan","Info")';
$reload='window.location.href="'.site_url().'setting/'.$this->link.'#success"';
        echo '{'.$msg.','.$reload.'}';
        }
}

function FupdateData(){
$where =array($this->field.'id' =>decryptURL($this->input->post('hide-ID')));
        if($this->model->updateItemById($where)){
$msg='ToastrSukses("'.$this->label.' telah diedit","Info")';
$reload='window.location.href="'.site_url().'setting/'.$this->link.'#success"';
        echo '{'.$msg.','.$reload.'}';
    }
}
 
    function editRow() {
        $id= $this->input->post("id");
        $where =array($this->field.'id' =>$id);
        $value= $this->input->post("value");
        $modul= $this->input->post("modul");
        $data=array($modul=>$value);
$msg='ToastrSukses("Data Perijinan '.$this->label.' telah ditambahkan","Info")';
        echo $msg;
    } 

function saveSetting(){
    $data['idclient'] = $this->input->post('idclient');
    $data['idbuyer'] = $this->input->post('idbuyer');
    $data['idsupplier'] = $this->input->post('idsupplier');
    $data['idsortimen'] = $this->input->post('idsortimen');
    $data['idwip'] = $this->input->post('idwip');
    $data['idproduk'] = $this->input->post('idproduk');

        $simpan=saveData('kit_setting',$data);
        if($simpan){
$msg='ToastrSukses("Setting Client telah ditambahkan","Info")';
        }
        echo $msg;
}
        public function getListDT(){
            $table      = $this->table; 
            $field      = $this->field;
            $primaryKey = $this->field.'id';
            $sql_details = sql_connect();

                $columns = array(
                    array('db' => $primaryKey, 'dt' => 0, 'field' => $primaryKey),
                    array('db' => $field.'nama', 'dt' => 1, 'field' => $field.'nama', 'formatter' => function( $d, $row ) {
                           return '<a href="#" class="bolder _500" data-toggle="modal" data-target="#modal-detail1" data-id="'.encryptURL($row[$this->field.'id']).'">'.$d.'</a>';}),
                    array('db' => $field.'alamat', 'dt' => 2, 'field' => $field.'alamat','formatter'=>function($d,$row){
                        return (strlen($d)<=75)?$d:substr($d, 0, 75).'...';
                    }),
                    array('db' => $field.'telp', 'dt' => 3, 'field' => $field.'telp'),
                    array('db' => $field.'fax', 'dt' => 4, 'field' => $field.'fax'),
                    array('db' => $field.'email', 'dt' => 5, 'field' => $field.'email'),
                    array('db' => $field.'aktif', 'dt' => 6, 'field' => $field.'aktif','formatter'=>function($d,$row){
                        return '<a href="javascript:void(0)" data-id="'.encryptURL($row[$this->field.'id']).'" class="statusAlamat">'.stUser($row[$this->field.'aktif']).'</a>';
                    }),
                    array('db' => $primaryKey, 'dt' => 7, 'field' => $primaryKey, 'formatter' => function( $d, $row ) {
                return '<a href="'.site_url('setting/'.$this->link.'/').encryptURL($d).'" class="btn btn-xs btn-info" title="Edit '.$this->label.'">
                <i class="far fa-edit"></i></a>
                <a href="javascript:void(0)" class="btn btn-xs btn-danger delete-user" data-id="'.encryptURL($d).'" title="Delete '.$this->label.'"><i class="far fa-trash-alt"></i></a>
                <a href="#" class="btn btn-xs btn-warning modal-setting" data-toggle="modal" data-target="#modal-detail2" data-id="'.encryptURL($row[$this->field.'id']).'" data-id="'.encryptURL($d).'" title="Delete '.$this->label.'"><i class="fa fa-cogs"></i></a>';
                                   }),
                );
            $joinQuery  = "";
            // $joinQuery  .= "left join ".$this->jTable2." as t3 on (".$this->field3."=".$this->field3.")";
            $extraWhere = $field."stdelete=1";
            $groupBy    = "";
            $ordercus   = "";
            $having     = "";
            echo json_encode(
                SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having, $ordercus )
            );
        }

        public function getListDT2(){
            $table      = 'kit_buyer'; 
            $field      = 'buyer';
            $primaryKey = 'id'.$field;
            $sql_details = sql_connect();

                $columns = array(
                    array('db' => $primaryKey, 'dt' => 0, 'field' => $primaryKey),
                    array('db' => 'idnegara', 'dt' => 0, 'field' => 'idnegara'),
                    array('db' => 'idx', 'dt' => 0, 'field' => 'idx'),
                    array('db' => $field, 'dt' => 1, 'field' => $field, 'formatter' => function( $d, $row ) {
                return $d.' ['.$row['idnegara'].']';
                                   }),
                    array('db' => 'idclient', 'dt' => 2, 'field' => 'idclient', 'formatter' => function( $d, $row ) {
                return '<a href="javascript:void(0)" class="btn btn-xs btn-danger delete-row" data-tabel="kit_buyer" data-idx="'.encryptURL($row['idx']).'" data-id="'.encryptURL($d).'" data-idz="'.encryptURL($row['idbuyer']).'" title="Delete Buyer"><i class="far fa-trash-alt"></i></a>';
                                   }),
                );
            $joinQuery  = "from `$table` as `t1` INNER JOIN `kit_setting` using(`idbuyer`)";
            $extraWhere = $field."stdelete=1 and `kit_setting`.`idclient`=".decryptURL($_GET['idclient']);
            $groupBy    = "";
            $ordercus   = "";
            $having     = "";
            echo json_encode(
                SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having, $ordercus )
            );
        }

        public function getListDT3(){
            $table      = 'kit_supplier'; 
            $field      = 'supplier';
            $primaryKey = 'id'.$field;
            $sql_details = sql_connect();

                $columns = array(
                    array('db' => $primaryKey, 'dt' => 0, 'field' => $primaryKey),
                    array('db' => 'idnegara', 'dt' => 0, 'field' => 'idnegara'),
                    array('db' => 'idx', 'dt' => 0, 'field' => 'idx'),
                    array('db' => $field, 'dt' => 1, 'field' => $field, 'formatter' => function( $d, $row ) {
                return $d.' ['.$row['idnegara'].']';
                                   }),
                    array('db' => 'idclient', 'dt' => 2, 'field' => 'idclient', 'formatter' => function( $d, $row ) {
                return '<a href="javascript:void(0)" class="btn btn-xs btn-danger delete-row" data-tabel="kit_supplier" data-idx="'.encryptURL($row['idx']).'" data-id="'.encryptURL($d).'" data-idz="'.encryptURL($row['idsupplier']).'" title="Delete Supplier"><i class="far fa-trash-alt"></i></a>';
                                   }),
                );
            $joinQuery  = "from `$table` as `t1` INNER JOIN `kit_setting` using(`idsupplier`)";
            $extraWhere = $field."stdelete=1 and `kit_setting`.`idclient`=".decryptURL($_GET['idclient']);
            $groupBy    = "";
            $ordercus   = "";
            $having     = "";
            echo json_encode(
                SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having, $ordercus )
            );
        }

        public function getListDT4(){
            $table      = 'kit_sortimen'; 
            $field      = 'sortimen';
            $primaryKey = 'id'.$field;
            $sql_details = sql_connect();

                $columns = array(
                    array('db' => 'idx', 'dt' => 0, 'field' => 'idx'),
                    array('db' => $primaryKey, 'dt' => 0, 'field' => $primaryKey),
                    array('db' => $field, 'dt' => 1, 'field' => $field),
                    array('db' => 'idclient', 'dt' => 2, 'field' => 'idclient', 'formatter' => function( $d, $row ) {
                return '<a href="javascript:void(0)" class="btn btn-xs btn-danger delete-row" data-tabel="kit_sortimen" data-idx="'.encryptURL($row['idx']).'" data-id="'.encryptURL($d).'" data-idz="'.encryptURL($row['idsortimen']).'" title="Delete Sortimen"><i class="far fa-trash-alt"></i></a>';
                                   }),
                );
            $joinQuery  = "from `$table` as `t1` INNER JOIN `kit_setting` using(`idsortimen`)";
            $extraWhere = $field."stdelete=1 and `kit_setting`.`idclient`=".decryptURL($_GET['idclient']);
            $groupBy    = "";
            $ordercus   = "";
            $having     = "";
            echo json_encode(
                SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having, $ordercus )
            );
        }

        public function getListDT5(){
            $table      = 'kit_wip'; 
            $field      = 'wip';
            $primaryKey = 'id'.$field;
            $sql_details = sql_connect();

                $columns = array(
                    array('db' => 'idx', 'dt' => 0, 'field' => 'idx'),
                    array('db' => $primaryKey, 'dt' => 0, 'field' => $primaryKey),
                    array('db' => $field, 'dt' => 1, 'field' => $field),
                    array('db' => 'idclient', 'dt' => 2, 'field' => 'idclient', 'formatter' => function( $d, $row ) {
                return '<a href="javascript:void(0)" class="btn btn-xs btn-danger delete-row" data-tabel="kit_wip" data-idx="'.encryptURL($row['idx']).'" data-id="'.encryptURL($d).'" data-idz="'.encryptURL($row['idwip']).'" title="Delete Wip"><i class="far fa-trash-alt"></i></a>';
                                   }),
                );
            $joinQuery  = "from `$table` as `t1` INNER JOIN `kit_setting` using(`idwip`)";
            $extraWhere = $field."stdelete=1 and `kit_setting`.`idclient`=".decryptURL($_GET['idclient']);
            $groupBy    = "";
            $ordercus   = "";
            $having     = "";
            echo json_encode(
                SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having, $ordercus )
            );
        }

        public function getListDT6(){
            $table      = 'kit_produk'; 
            $field      = 'produk';
            $primaryKey = 'id'.$field;
            $sql_details = sql_connect();

                $columns = array(
                    array('db' => $primaryKey, 'dt' => 0, 'field' => $primaryKey),
                    array('db' => 'idx', 'dt' => 0, 'field' => 'idx'),
                    array('db' => 'kodehs', 'dt' => 0, 'field' => 'kodehs'),
                    array('db' => $field, 'dt' => 1, 'field' => $field, 'formatter' => function( $d, $row ) {
                return $d.' ['.$row['kodehs'].']';
                                   }),
                    array('db' => 'idclient', 'dt' => 2, 'field' => 'idclient', 'formatter' => function( $d, $row ) {
                return '<a href="javascript:void(0)" class="btn btn-xs btn-danger delete-row" data-tabel="kit_produk" data-idx="'.encryptURL($row['idx']).'" data-id="'.encryptURL($d).'" data-idz="'.encryptURL($row['idproduk']).'" title="Delete Produk"><i class="far fa-trash-alt"></i></a>';
                                   }),
                );
            $joinQuery  = "from `$table` as `t1` INNER JOIN `kit_setting` using(`idproduk`)";
            $extraWhere = $field."stdelete=1 and `kit_setting`.`idclient`=".decryptURL($_GET['idclient']);
            $groupBy    = "";
            $ordercus   = "";
            $having     = "";
            echo json_encode(
                SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having, $ordercus )
            );
        }



function delRow() {
    if (!is_numeric($this->input->post("id")) || !is_numeric($this->input->post("idx")) || !is_numeric($this->input->post("idz"))) {
$id     = decryptURL($this->input->post("id"));
$idx    = decryptURL($this->input->post("idx"));
$idZ    = decryptURL($this->input->post("idz"));
    } else {
$id     = $this->input->post("id");
$idx    = $this->input->post("idx");
$idZ    = $this->input->post("idz");
    }
$tabel  = $this->input->post("tabel");
    switch ($tabel) {
        case 'kit_buyer':
                $aa=$this->model->delRow($tabel,array('idclient'=>$id,'idx'=>$idx),'idbuyer');
                $get=rowWhere('idbuyer',$idZ,$tabel);
                $teks= $get->buyer;
                $output='["Buyer","'.$teks.'"]';
            break;
        case 'kit_supplier':
                $aa=$this->model->delRow($tabel,array('idclient'=>$id,'idx'=>$idx),'idsupplier');
                $get=rowWhere('idsupplier',$idZ,$tabel);
                $teks= $get->supplier;
                $output='["Supplier","'.$teks.'"]';
            break;
        case 'kit_sortimen':
                $aa=$this->model->delRow($tabel,array('idclient'=>$id,'idx'=>$idx),'idsortimen');
                $get=rowWhere('idsortimen',$idZ,$tabel);
                $teks= $get->sortimen;
                $output='["Sortimen","'.$teks.'"]';
            break;
        case 'kit_wip':
                $aa=$this->model->delRow($tabel,array('idclient'=>$id,'idx'=>$idx),'idwip');
                $get=rowWhere('idwip',$idZ,$tabel);
                $teks= $get->wip;
                $output='["Wip","'.$teks.'"]';
            break;
        case 'kit_produk':
                $aa=$this->model->delRow($tabel,array('idclient'=>$id,'idx'=>$idx),'idproduk');
                $get=rowWhere('idproduk',$idZ,$tabel);
                $teks= $get->produk;
                $output='["Produk","'.$teks.'"]';
            break;
    }
    print_r($output);exit();
}
function delAkun() {
$id= decryptURL($this->input->post("id")); 
$aa=$this->model->delAkun($id);
$get=rowWhere($this->field.'id',$id,$this->table);
// $getC=rowWhere('client_id',$get->client_id,$this->tblClients);
echo $get->client_nama;
}
function gantiStatus() {
$id= decryptURL($this->input->post("id")); 
$aa=$this->model->gantiStatus($id);
$bb = $this->model->dataStatus($id);
// $dataStatus_json = $bb;
echo stUser($bb);
}

}

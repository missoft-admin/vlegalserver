<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Barang extends CI_Controller {
	var $infobar;
   public function __construct()
   {
   	  parent::__construct();
      if (!$online=$this->infobar_model->cek_setting()){
		redirect('offline');
   	  }
      // if ( $this->session->userdata('logged_in')!=TRUE ) {
   	       // redirect('login');
      // }
 
      $this->load->model('barang_model','barang'); 
//      check_infobar();
	  $this->infobar=$this->infobar_model->get_infobar(); 
   }

	public function index()
	{	$this->session->set_userdata('page', 'Barang - List');
		// if ($query=$this->barang->get_listbarang($this->uri->segment(3),10)){
			// $data['records'] = $query;
			// $this->pagination->initialize(paging_admin($this->barang->count(),'barang/index'));
			// $data['pagination'] =  $this->pagination->create_links();
		// }
		if ($this->session->userdata('cabang')){
			$row =$this->barang->get_cabang();
			$data['cabang'] ="Cabang ".$row->namacabang." ( Zona ".$row->propinsi." )";
		}else{
			$data['cabang'] ='PUSAT';
		}
		$data['error'] 			= '';
		$data['ajax_url'] = 'barang/getDataList/'; 
		$data['title'] = 'Barang - List'; 
		$data['active'] =211;
		// print_r('data');exit();
		$data['breadcrum'] = array(array("Master","#","frames"),array("Barang",'#',"active"));
		$data['template'] = 'barang/index';	
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	} 
	function getDataList()
    {
        $query = $this->barang->get_listbarang();
        $this->output->set_output($query);
    }
	public function editor($noid=0,$namabarang=0,$suk=0)
	{//if ( $this->session->userdata('level')!=1 ) {redirect('dashboard');}
		// print_r($noid);exit();
		if ($noid){
			$this->session->set_userdata('page', 'Barang - Edit');
			if($row = $this->barang->get_barang($noid))
			{
				$data['noid'] = $noid;
				$data['namabarang'] = $row->namabarang;
				$data['hargabeli'] = $row->hargabeli;
				$data['jenis'] = $row->jenis;
				$data['hargajualstokis'] = $row->hargajualstokis;
				$data['hargajualmember'] = $row->hargajualmember;
				$data['ket'] = $row->ket;
				if($row->createdby){$data['namac'] = $row->namac;}else{$data['namac'] = " - "; }
				if($row->editedby){$data['namae'] = $row->namae;}else{$data['namae'] = " - "; }
			}
		}else{
				$this->session->set_userdata('page', 'Barang - Addnew');
				$data['noid'] = 0;
				$data['namabarang'] = "";
				$data['hargabeli'] = "";
				$data['jenis'] = 1;
				$data['hargajualstokis'] = "";
				$data['hargajualmember'] = "";
				$data['ket'] = "";		
		}
		if ($namabarang){
			if (substr($namabarang,0,5)=='10001'){
				$data['namabarang'] = str_replace('10001', '', $namabarang);
				$data['error'] ="Harga Jual Harus Lebih Besar dari Harga Beli";
			}else{
			$namabarang=strtoupper(str_replace('%20', ' ',$namabarang));
			if ($this->barang->check_namabarang($noid,$namabarang)==1){	
				$data['error'] ="Nama Barang \"<span style=\"color: white; font-weight: bold;\">".$namabarang."</span>\" sudah ada.";
			}else{
				$data['error'] ="Nama Barang \"<span style=\"color: white; font-weight: bold;\">".$namabarang."</span>\" sudah ada, namun sudah dihapus sementara, untuk lebih jelas silahkan hubungi administrator.";
			}}
		}else{
			$data['error'] =0;
		}
		$data['suk'] =$suk;
		$data['active'] =212;
		if ($noid){
			$data['breadcrum'] = array(array("Master","#","frames"),array("Barang",'barang',"point_right"),array("Edit",'#',"active"));
		}else{
			$data['breadcrum'] = array(array("Master","#","frames"),array("Barang",'barang',"point_right"),array("Input Baru",'#',"active"));
		}
		$data['template'] = 'barang/add_new_view';
		$data['title'] = 'Barang - Edit Barang';
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}

	public function simpan()
	{	
		$baru=$this->input->post('noid');
		$namabarang=strtolower($this->input->post('namabarang'));
		$hrgbeli=$this->input->post('hargabeli');
		$hrgjuals=$this->input->post('hargajualstokis');
		$hrgjualm=$this->input->post('hargajualmember');
		if ($this->barang->check_namabarang($baru,$namabarang)){
			redirect("barang/editor/$baru/$namabarang");
		}elseif ($hrgbeli>=$hrgjualm){
			redirect("barang/editor/$baru/10001".$namabarang);
		}else{
			if ($baru){
				$datarec = array( 
					'namabarang' => ucwords($namabarang),
					'hargabeli' => $hrgbeli,
					'hargajualstokis' => $hrgjuals,
					'hargajualmember' => $hrgjualm,
					'jenis' => (int)($this->input->post('jenis')),
					'ket' => ucwords(strtolower($this->input->post('ket'))),
					'editedby' => $this->session->userdata('userid')
					);
				$noid=$this->barang->update_record($baru,$datarec);
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','Data Tersimpan.');
				redirect("barang/editor/$baru/0/1");				
			}else{
				$datarec = array( 
					'namabarang' => ucwords($namabarang),
					'hargabeli' => $hrgbeli,
					'hargajualstokis' => $hrgjuals,
					'hargajualmember' => $hrgjualm,
					'jenis' => (int)($this->input->post('jenis')),
					'ket' => ucwords(strtolower($this->input->post('ket'))),
					'available' => date("Y-m-d H:t:s"),
					'createdby' => $this->session->userdata('userid')
					);
				$noid=$this->barang->add_record($datarec);
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','Data Tersimpan.');
				redirect("barang/editor/$noid/0/1");
			}
		}
	}

	function deletebrg($id,$uri="")
	{
		$this->barang->deletebarang($id);
		if ($uri>=$this->barang->count()){
			$uri=$uri-5;
			if ($uri<1){$uri="";}
		}				
		redirect("barang/index/$uri",'location');
	}	
	public function stokcabang($kodebarang)
	{	$this->session->set_userdata('page', 'Barang - Stok Cabang');
		if ($query=$this->barang->get_stokcabang($kodebarang)){
			$data['records'] = $query;
		}
		$data['active'] =213;
		$data['breadcrum'] = array(array("Master","#","frames"),array("Barang",'barang',"point_right"),array("Stok Cabang",'#',"active"));
		$data['template'] = 'barang/stok_cabang';
		$data['title'] = 'Barang - Stok Cabang';
		
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
		// $data = array_merge($data, backend_info());
	} 	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
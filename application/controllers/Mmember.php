<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mmember extends CI_Controller {


	function __construct()
  {
		parent::__construct();
		// PermissionUserLoggedIn($this->session);
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('mmember_model');
  }

	function index(){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'All Member';
		$data['template'] 		= 'mmember/index';
		$data['ajax_url'] = 'mmember/getDataList/'; 
		$data['breadcrum'] 	= array(
								array("Area Admin",'#'),
								array("Member",'#'),
								array("List",'Mmember')
								);
	
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function getDataList()
    {
        $query = $this->mmember_model->data_list();
        $this->output->set_output($query);
    }
	function newmembers(){
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'New Member';
		$data['template'] 		= 'mmember/index';
		$data['ajax_url'] = 'mmember/getDataListNew/'; 
		$data['breadcrum'] 	= array(
								array("Area Admin",'#'),
								array("Member",'#'),
								array("List",'Mmember')
								);
	
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function getDataListNew()
    {
        $query = $this->mmember_model->data_list_new();
        $this->output->set_output($query);
    }
	function test(){
		// print_r('sini');exit();
		$data=array();
		$row=$this->mmember_model->data_member();
		print_r($row);exit();
	}
	
	
	
	function editor()
	{	
			//print_r($this->uri->segment(3));exit();
			$noid=$this->uri->segment(3);
			$data=array();
			$this->session->unset_userdata('searchkey');			
			$this->session->set_userdata('page', 'Members - Edit');
			if($row = $this->mmember_model->get_members($this->uri->segment(3)))
			{
				$data['noid'] = $row->noid;
				$data['edit'] = '1';
				$data['namamembers'] = $row->namamembers;
				$data['noupline'] = $row->noupline;
				$data['namaupline'] = $row->namaupline;
				$data['nosponsor'] = $row->nosponsor;
				$data['namasponsor'] = $row->namasponsor;
				$data['namastokies'] = $row->namastokies;
				$data['noidstokies'] = $row->noidstokies;
				$data['alamat'] = $row->alamat;
				$data['propinsi'] = $row->propinsi;
				$data['kota'] = $row->kota;
				$data['nama_kota'] = $row->nama_kota;
				$data['telepon'] = $row->telepon;
				$data['hp'] = $row->hp;
				// $data['hp2'] = $row->hp2;
				$data['norek'] = $row->norekening;
				$data['atasnama'] = $row->atasnama;
				$data['bank'] = $row->bank;
				$data['cabang'] = $row->cabang;
				$data['tgldaftar'] = $row->tgldaftar;
				$data['email'] = $row->email;
				if ($row->norekening){$data['haverec'] =1;}else{$data['haverec'] =0;} 
				if($row->createdby){$data['namac'] = $row->namac;}else{$data['namac'] = " Activation "; }
				if($row->editedby){
					if($row->editedby!=1001){
						$data['namae'] = $row->namae." [ Admin ]";
					}else{$data['namae'] = $row->namamembers." [ Members ]"; }
				}else{$data['namae'] = " - "; }
				//$data['namaupline'] = $this->mmember_model->get_namamembers($row->noupline);
				//new
				$data['noktp'] = $row->noktp;
				$data['tempatlahir'] = $row->tempatlahir;
				$data['tgllahir'] = date_format(date_create($row->tgllahir),'m/d/Y');
				$data['jeniskelamin'] = $row->jeniskelamin;
				$data['agama'] = $row->agama;
				$data['status'] = $row->status;
				$data['tanggungan'] = $row->tanggungan;
				$data['ahliwaris'] = $row->ahliwaris;
				$data['hubungan'] = $row->hubungan;
				$data['desa'] = $row->desa;
				$data['kecamatan'] = $row->kecamatan;
				$data['kodepos'] = $row->kodepos;
			}
				
		if ($noid){
			$data['breadcrum'] = array(array("Master","#","frames"),array("Members",'members',"point_right"),array("Edit",'#',"active"));
		}else{
			$data['breadcrum'] = array(array("Master","#","frames"),array("Members",'members',"point_right"),array("Input Baru",'#',"active"));
		}
		$data['error'] = '';
		$data['title'] = 'Members Edit';
		$data['template'] = 'mmember/manage';	
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function save(){
		$noid =  ($this->input->post('noid'));
		$edit = (int)($this->input->post('edit'));
		// $haverec = (int)($this->input->post('haverec'));
		//print_r($edit);exit();
		// if ($haverec){
			// $datarek = array(
				// 'norekening' => $this->input->post('norek'),
				// 'atasnama' => ucwords(strtolower($this->input->post('atasnama'))),
				// 'bank' => strtoupper($this->input->post('bank')),
				// 'cabang' => ucwords(strtolower($this->input->post('cabang')))
			// );
		// }else{
			// $datarek = array(
				// 'norekening' => '',
				// 'atasnama' => '',
				// 'bank' => '',
				// 'cabang' => ''
			// );
		// }
		

		if ($edit){
			
			$datarec = array( 
				'namamembers' => ltrim(ucwords(strtolower($this->input->post('namamembers')))),
				'noidstokies' => $this->input->post('noidstokies'),
				'alamat' => ucwords(strtolower($this->input->post('alamat'))),
				'kota' => ucwords(strtolower($this->input->post('kota'))),
				'telepon' => $this->input->post('telepon'),
				'hp' => $this->input->post('hp'),
				// 'hp2' => $this->input->post('hp2'),
				'email' => $this->input->post('email'),
				'editedby' => $this->session->userdata('userid'),
			    'noktp' => $this->input->post('noktp'),
			    'tempatlahir' => ucwords(strtolower($this->input->post('tempatlahir'))),
			    'tgllahir' => date_format(date_create($this->input->post('tgllahir')),'Y-m-d'),
			    'jeniskelamin' => (int)($this->input->post('jeniskelamin')),
			    'agama' => (int)($this->input->post('agama')),
			    'status' => (int)($this->input->post('status')),
			    'tanggungan' => $this->input->post('tanggungan'),
			    'ahliwaris' => ucwords(strtolower($this->input->post('ahliwaris'))),
			    'hubungan' => ucwords(strtolower($this->input->post('hubungan'))),
			    'desa' => ucwords(strtolower($this->input->post('desa'))),
			    'kecamatan' => ucwords(strtolower($this->input->post('kecamatan'))),
			    'kodepos' => ucwords(strtolower($this->input->post('kodepos')))
				);
			// print_r($datarec);exit();
			
			// $datarec=array_merge($datarec,$datarek);
			if ($this->mmember_model->update_record($noid,$datarec)){
				$this->session->set_flashdata('confirm',true);
				$this->session->set_flashdata('message_flash','Data Tersimpan.');
				redirect("mmember/editor/$noid");				
			}else{
				
			}
		}else{
			$noid=$this->mmember_model->get_newnoid();
			$datarec = array( 
				'noid' => $noid,
				'namamembers' => ucwords(strtolower($this->input->post('namamembers'))),
				'noupline' => strtoupper($this->input->post('noupline')),
				'nosponsor' => strtoupper($this->input->post('nosponsor')),
				'noidstokies' => $this->input->post('noidstokies'),
				'alamat' => ucwords(strtolower($this->input->post('alamat'))),
				'kota' => ucwords(strtolower($this->input->post('kota'))),
				'telepon' => $this->input->post('telepon'),
				'hp' => $this->input->post('hp'),
				'hp2' => $this->input->post('hp2'),
				'email' => $this->input->post('email'),
				'passkey' => md5(sha1(md5($noid))),
				'tgldaftar' => date("Y-m-d H:t:s"),
				'createdby' => $this->session->userdata('userid'),
			//NEW
			    'noktp' => $this->input->post('noktp'),
			    'tempatlahir' => ucwords(strtolower($this->input->post('tempatlahir'))),
			    'tgllahir' => date_format(date_create($this->input->post('tgllahir')),'Y-m-d'),
			    'jeniskelamin' => (int)($this->input->post('jeniskelamin')),
			    'agama' => (int)($this->input->post('agama')),
			    'status' => (int)($this->input->post('status')),
			    'tanggungan' => $this->input->post('tanggungan'),
			    'ahliwaris' => ucwords(strtolower($this->input->post('ahliwaris'))),
			    'hubungan' => ucwords(strtolower($this->input->post('hubungan'))),
			    'desa' => ucwords(strtolower($this->input->post('desa'))),
			    'kecamatan' => ucwords(strtolower($this->input->post('kecamatan'))),
			    'kodepos' => ucwords(strtolower($this->input->post('kodepos')))
				);
			$datarec=array_merge($datarec,$datarek);
			$this->mmember_model->add_record($datarec);
			redirect("mmember/editor/$noid/1/0/1");
		}
		
	}
	function viewinfonet($noid=0)
	{
		$noid=$this->uri->segment(3);
		$data['noid'] = $noid;
		if($query = $this->mmember_model->cek_upline($noid)){
			$data['namamembers'] = $query->namamembers;
		}
		$data['page_title'] ="Info Jaringan";
		$data['nilainol']='<img src="'.site_url('assets/img/rank/starjemred.png').'">';
		$data['nilaijem']='<img src="'.site_url('assets/img/rank/starjem.png').'">';
		$data['nilaibin']='<img src="'.site_url('assets/img/rank/starrank.png').'"> ';
		$data['nilaigol']='<img src="'.site_url('assets/img/rank/stargold.png').'"> ';
		if($row = $this->mmember_model->get_infonet($noid)){
			$data['jmldownline'] = $row->jdownline;
			$data['jmllevel1'] = $row->jlevel1;
			$data['jmllevel2'] = $row->jlevel2;
			$data['jmllevel3'] = $row->jlevel3;
			$data['jmllevel4'] = $row->jlevel4;
			$data['jmllevel5'] = $row->jlevel5;
			$data['jmllevel6'] = $row->jlevel6;
			$data['jmllevel7'] = $row->jlevel7;
			$data['jmllevel8'] = $row->jlevel8;
			$data['jmllevel9'] = $row->jlevel9;
			$data['jmllevel10'] = $row->jlevel10;
			$valcur=$row->jnilai;
			$gradecur=$row->jgrade;			
			$data['mlevel1'] = 5 - $row->jlevel1;
			$data['mlevel2'] = 25 - $row->jlevel2;
			$data['mlevel3'] = 125 - $row->jlevel3;
			$data['mlevel4'] = 625 - $row->jlevel4;
			$data['mlevel5'] = 3125 - $row->jlevel5;
			$data['mlevel6'] = 15625 - $row->jlevel6;
			$data['mlevel7'] = 78125 - $row->jlevel7;
			$data['mlevel8'] = 390625 - $row->jlevel8;
			$data['mlevel9'] = 1953125 - $row->jlevel9;
			$data['mlevel10'] = 9765625 - $row->jlevel10;
			$data['mtotal'] = 12207025 - $row->jdownline;
			if ($data['jmllevel1']==0 ){
				$data['ket1']="<font color='red'>Belum Ada Recruitment</font>";
				$data['nilai1']=$data['nilainol'];
			}elseif ($data['jmllevel1']>0 && $data['jmllevel1']<5){
				$data['ket1']="<font color='blue'>".number_format($data['jmllevel1']/5*100,4,",",".")."%</font>";
				$data['nilai1']=cek_nilai($data['jmllevel1'],5);
			}elseif ($data['jmllevel1']>=5){
				$data['ket1']="<font color='green'>100%</font>";
				$data['nilai1']=str_repeat($data['nilaigol'],5);
			}
			if ($data['jmllevel2']==0 ){
				$data['ket2']="<font color='red'>Belum Ada Recruitment</font>";
				$data['nilai2']=$data['nilainol'];
			}elseif ($data['jmllevel2']>0 && $data['jmllevel2']<25){
				$data['ket2']="<font color='blue'>".number_format($data['jmllevel2']/25*100,4,",",".")."%</font>";
				$data['nilai2']=cek_nilai($data['jmllevel2'],25);
			}elseif ($data['jmllevel2']>=25){
				$data['ket2']="<font color='green'>100%</font>";
				$data['nilai2']=str_repeat($data['nilaigol'],5);
			}
			if ($data['jmllevel3']==0 ){
				$data['ket3']="<font color='red'>Belum Ada Recruitment</font>";
				$data['nilai3']=$data['nilainol'];
			}elseif ($data['jmllevel3']>0 && $data['jmllevel3']<125){
				$data['ket3']="<font color='blue'>".number_format($data['jmllevel3']/125*100,4,",",".")."%</font>";
				$data['nilai3']=cek_nilai($data['jmllevel3'],125);
			}elseif ($data['jmllevel3']>=125){
				$data['ket3']="<font color='green'>100%</font>";
				$data['nilai3']=str_repeat($data['nilaigol'],5);
			}
			if ($data['jmllevel4']==0 ){
				$data['ket4']="<font color='red'>Belum Ada Recruitment</font>";
				$data['nilai4']=$data['nilainol'];
			}elseif ($data['jmllevel4']>0 && $data['jmllevel4']<625){
				$data['ket4']="<font color='blue'>".number_format($data['jmllevel4']/625*100,4,",",".")."%</font>";
				$data['nilai4']=cek_nilai($data['jmllevel4'],625);
			}elseif ($data['jmllevel4']>=625){
				$data['ket4']="<font color='green'>100%</font>";
				$data['nilai4']=str_repeat($data['nilaigol'],5);
			}
			if ($data['jmllevel5']==0 ){
				$data['ket5']="<font color='red'>Belum Ada Recruitment</font>";
				$data['nilai5']=$data['nilainol'];
			}elseif ($data['jmllevel5']>0 && $data['jmllevel5']<3125){
				$data['ket5']="<font color='blue'>".number_format($data['jmllevel5']/3125*100,4,",",".")."%</font>";
				$data['nilai5']=cek_nilai($data['jmllevel5'],3125);
			}elseif ($data['jmllevel5']>=3125){
				$data['ket5']="<font color='green'>100%</font>";
				$data['nilai5']=str_repeat($data['nilaigol'],5);
			}
			if ($data['jmllevel6']==0 ){
				$data['ket6']="<font color='red'>Belum Ada Recruitment</font>";
				$data['nilai6']=$data['nilainol'];
			}elseif ($data['jmllevel6']>0 && $data['jmllevel6']<15625){
				$data['ket6']="<font color='blue'>".number_format($data['jmllevel6']/15625*100,4,",",".")."%</font>";
				$data['nilai6']=cek_nilai($data['jmllevel6'],15625);
			}elseif ($data['jmllevel6']>=15625){
				$data['ket6']="<font color='green'>100%</font>";
				$data['nilai6']=str_repeat($data['nilaigol'],5);
			}
			if ($data['jmllevel7']==0 ){
				$data['ket7']="<font color='red'>Belum Ada Recruitment</font>";
				$data['nilai7']=$data['nilainol'];
			}elseif ($data['jmllevel7']>0 && $data['jmllevel7']<78125){
				$data['ket7']="<font color='blue'>".number_format($data['jmllevel7']/78125*100,4,",",".")."%</font>";
				$data['nilai7']=cek_nilai($data['jmllevel7'],78125);
			}elseif ($data['jmllevel7']>=78125){
				$data['ket7']="<font color='green'>100%</font>";
				$data['nilai7']=str_repeat($data['nilaigol'],5);
			}
			if ($data['jmllevel8']==0 ){
				$data['ket8']="<font color='red'>Belum Ada Recruitment</font>";
				$data['nilai8']=$data['nilainol'];
			}elseif ($data['jmllevel8']>0 && $data['jmllevel8']<390625){
				$data['ket8']="<font color='blue'>".number_format($data['jmllevel8']/390625*100,4,",",".")."%</font>";
				$data['nilai8']=cek_nilai($data['jmllevel8'],390625);
			}elseif ($data['jmllevel8']>=390625){
				$data['ket8']="<font color='green'>100%</font>";
				$data['nilai8']=str_repeat($data['nilaigol'],5);
			}
			if ($data['jmllevel9']==0 ){
				$data['ket9']="<font color='red'>Belum Ada Recruitment</font>";
				$data['nilai9']=$data['nilainol'];
			}elseif ($data['jmllevel9']>0 && $data['jmllevel9']<1953125){
				$data['ket9']="<font color='blue'>".number_format($data['jmllevel9']/1953125*100,4,",",".")."%</font>";
				$data['nilai9']=cek_nilai($data['jmllevel9'],1953125);
			}elseif ($data['jmllevel9']>=1953125){
				$data['ket9']="<font color='green'>100%</font>";
				$data['nilai9']=str_repeat($data['nilaigol'],5);
			}
			if ($data['jmllevel10']==0 ){
				$data['ket10']="<font color='red'>Belum Ada Recruitment</font>";
				$data['nilai10']=$data['nilainol'];
			}elseif ($data['jmllevel10']>0 && $data['jmllevel10']<9755625){
				$data['ket10']="<font color='blue'>".number_format($data['jmllevel10']/9755625*100,4,",",".")."%</font>";
				$data['nilai10']=cek_nilai($data['jmllevel10'],9755625);
			}elseif ($data['jmllevel10']>=9755625){
				$data['ket10']="<font color='green'>100%</font>";
				$data['nilai10']=str_repeat($data['nilaigol'],5);
			}
			$valnow=number_format($data['jmldownline']/12207025*100,10);
			if ($data['jmldownline']==0 ){
				$data['kettot']="<font color='red'>Belum Ada Recruitment</font>";
				$data['nilaitot']=$data['nilainol'];
			}elseif ($data['jmldownline']>0 && $data['mtotal']<12207025){
				$data['kettot']="<font color='blue'>".number_format($valnow,4,",",".")."%</font>";
				$data['nilaitot']=cek_nilaitotal($data['jmldownline'],1);
			}elseif ($data['jmldownline']>=12207025){
				$data['kettot']="<font color='green'>100%</font>";
				$data['nilaitot']=str_repeat($data['nilaigol'],5);
			}
			if ($valnow!=$valcur){
				$this->mmember_model->check_value($data['jmldownline'],$gradecur,$noid);
			}
		}else{
			$data['jmldownline'] = $data['jmllevel1'] = $data['jmllevel2'] = $data['jmllevel3'] = $data['jmllevel4'] = $data['jmllevel5'] = $data['jmllevel6'] = $data['jmllevel7'] = $data['jmllevel8'] = $data['jmllevel9'] = $data['jmllevel10'] = 0;
			$data['mtotal'] = 12207025;
			$data['mlevel1'] = 5;
			$data['mlevel2'] = 25;
			$data['mlevel3'] = 125;
			$data['mlevel4'] = 625;
			$data['mlevel5'] = 3125;
			$data['mlevel6'] = 15625;
			$data['mlevel7'] = 78125;
			$data['mlevel8'] = 390625;
			$data['mlevel9'] = 1953125;
			$data['mlevel10'] = 9765625;
			$data['ket1']= $data['ket2']= $data['ket3']= $data['ket4']= $data['ket5']= $data['ket6']= $data['ket7']= $data['ket8']= $data['ket9']= $data['ket10']= $data['kettot']="<font color='red'>Belum Ada Recruitmen</font>";
		}		
		
		$data['template']='mmember/infonet_view';
		$data['title'] = 'INFO JARINGAN MEMBERS';
		$data['breadcrum'] = array(array("TSI",'home'),
						   array('Master Data','master'),
						   array('Members','members'),						
						   array('Info Jaringan','#')						
					 );	
		$data['active'] =228;
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function viewkomisi($noid=0,$bln=0,$thn=0)
	{	
		$this->session->set_userdata('page', 'Members - Bonus Belanja');
		if ($bln && $thn){
			$bulan=$bln;
			$tahun=$thn;
			$periode=$tahun.$bulan;
		}else{
			if (isset($_POST['bulan'])){
				$noid=$this->input->post('noid');	
				$page=$this->input->post('page');
				$urlbef=$this->input->post('urlbef');
				$bulan=$this->input->post('bulan');
				$tahun=$this->input->post('tahun');
				$periode=$tahun.$bulan;
			}else{
				$bulan=date("m");
				$tahun=date("y");
				$periode=$tahun.$bulan;
			}
		}
		$data['noid'] = $noid;
		if($query = $this->mmember_model->cek_upline($noid)){
			$data['namamembers'] = $query->namamembers;
		}
		$data['bulan'] = $bulan;
		$data['tahun'] = $tahun;
		// $data['control'] = $control;
		// $data['page'] = $page;
		// $data['urlbef'] = $urlbef;
		if ($periode < date('ym')){$passup=1;}else{$passup=0;}
		if($row = $this->mmember_model->get_royalty($noid,$periode,$passup)){
			$data['personal'] = $row->personal;
			$data['personalval'] = $row->personalval;
			$data['salelevel1'] = $row->salelevel1;
			$data['salelevel1val'] = $row->salelevel1val;
			$data['salelevel2'] = $row->salelevel2;
			$data['salelevel2val'] = $row->salelevel2val;
			$data['salelevel3'] = $row->salelevel3;
			$data['salelevel3val'] = $row->salelevel3val;
			$data['salelevel4'] = $row->salelevel4;
			$data['salelevel4val'] = $row->salelevel4val;
			$data['salelevel5'] = $row->salelevel5;
			$data['salelevel5val'] = $row->salelevel5val;
			$data['salelevel6'] = $row->salelevel6;
			$data['salelevel6val'] = $row->salelevel6val;
			$data['salelevel7'] = $row->salelevel7;
			$data['salelevel7val'] = $row->salelevel7val;
			$data['salelevel8'] = $row->salelevel8;
			$data['salelevel8val'] = $row->salelevel8val;
			$data['salelevel9'] = $row->salelevel9;
			$data['salelevel9val'] = $row->salelevel9val;
			$data['salelevel10'] = $row->salelevel10;
			$data['salelevel10val'] = $row->salelevel10val;
			if ($passup){
			$data['normal1'] = $row->normal1;
			$data['normal2'] = $row->normal2;
			$data['normal3'] = $row->normal3;
			$data['normal4'] = $row->normal4;
			$data['normal5'] = $row->normal5;
			$data['normal6'] = $row->normal6;
			$data['normal7'] = $row->normal7;
			$data['normal8'] = $row->normal8;
			$data['normal9'] = $row->normal9;
			$data['normal10'] = $row->normal10;
			}	
			
			if (strval($data['personal'])>=30){
				$data['totalkomisi'] = $data['personalval']+$data['salelevel1val']+$data['salelevel2val']+$data['salelevel3val']+$data['salelevel4val']+$data['salelevel5val']+
										$data['salelevel6val']+$data['salelevel7val']+$data['salelevel8val']+$data['salelevel9val']+$data['salelevel10val'];
			}else{
				$data['totalkomisi'] = 0;
			}		
		}else{
			$data['personal'] = 0;
			$data['personalval'] = 0;
			$data['salelevel1'] = 0;
			$data['salelevel1val'] = 0;
			$data['salelevel2'] = 0;
			$data['salelevel2val'] = 0;
			$data['salelevel3'] = 0;
			$data['salelevel3val'] = 0;
			$data['salelevel4'] = 0;
			$data['salelevel4val'] = 0;
			$data['salelevel5'] = 0;
			$data['salelevel5val'] = 0;
			$data['salelevel6'] = 0;
			$data['salelevel6val'] = 0;
			$data['salelevel7'] = 0;
			$data['salelevel7val'] = 0;
			$data['salelevel8'] = 0;
			$data['salelevel8val'] = 0;
			$data['salelevel9'] = 0;
			$data['salelevel9val'] = 0;
			$data['salelevel10'] = 0;
			$data['salelevel10val'] = 0;
			$data['totalkomisi'] = 0;
			//if ($passup){
			$data['normal1'] = 0;
			$data['normal2'] = 0;
			$data['normal3'] = 0;
			$data['normal4'] = 0;
			$data['normal5'] = 0;
			$data['normal6'] = 0;
			$data['normal7'] = 0;
			$data['normal8'] = 0;
			$data['normal9'] = 0;
			$data['normal10'] = 0;
			//}	
			
		}	
		//print_r($row);exit();		
		if ($periode>1302){
			if ($passup){
				$data['template']='mmember/royaltypp_view';	
			}else{
				$data['template']='mmember/royalty_view';
			}
		}else{
			// $data['template']='mmember/royalty_last_view';
			$data['template']='mmember/royalty_view';
		}
		$data['title'] ="Bonus Belanja";
		$data['breadcrum'] = array(array("Master","#","frames"),array("Members",'members',"point_right"),array("View Bonus Belanja",'#',"active"));		
		$data['active'] =225;
		/* $data = array_merge($data,$this->infobar);
		$this->parser->parse('index',$data); */
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function viewkomisinet($noid=0,$page=0,$urlbef=0,$control=0,$bln=0,$thn=0)
	{	
		$this->session->set_userdata('page', 'Members - Bonus Leveling');
		if ($bln && $thn){
			$bulan=$bln;
			$tahun=$thn;
			$periode=$tahun.$bulan;
		}else{
			if (isset($_POST['bulan'])){
				$noid=$this->input->post('noid');	
				$page=$this->input->post('page');
				$urlbef=$this->input->post('urlbef');
				$bulan=$this->input->post('bulan');
				$tahun=$this->input->post('tahun');
				$periode=$tahun.$bulan;
			}else{
				$bulan=date("m");
				$tahun=date("y");
				$periode=$tahun.$bulan;
			}
		}
		$data['noid'] = $noid;
		if($query = $this->mmember_model->cek_upline($noid)){
			$data['namamembers'] = $query->namamembers;
		}
		$data['bulan'] = $bulan;
		$data['tahun'] = $tahun;
		$data['control'] = $control;
		$data['page'] = $page;
		$data['urlbef'] = $urlbef;
		//print_r($periode);exit();
		if($row = $this->mmember_model->get_royaltynet($noid,$periode)){
			$data['personal'] = $row->personal;
			$data['sponsor'] = $row->sponsor;
			$data['sponsorval'] = $row->sponsorval;
			$data['netlevel1'] = $row->netlevel1;
			$data['netlevel1val'] = $row->netlevel1val;
			$data['netlevel2'] = $row->netlevel2;
			$data['netlevel2val'] = $row->netlevel2val;
			$data['netlevel3'] = $row->netlevel3;
			$data['netlevel3val'] = $row->netlevel3val;
			$data['netlevel4'] = $row->netlevel4;
			$data['netlevel4val'] = $row->netlevel4val;
			$data['netlevel5'] = $row->netlevel5;
			$data['netlevel5val'] = $row->netlevel5val;
			$data['netlevel6'] = $row->netlevel6;
			$data['netlevel6val'] = $row->netlevel6val;
			$data['netlevel7'] = $row->netlevel7;
			$data['netlevel7val'] = $row->netlevel7val;
			$data['netlevel8'] = $row->netlevel8;
			$data['netlevel8val'] = $row->netlevel8val;
			$data['netlevel9'] = $row->netlevel9;
			$data['netlevel9val'] = $row->netlevel9val;
			$data['netlevel10'] = $row->netlevel10;
			$data['netlevel10val'] = $row->netlevel10val;
			$data['totalkomisi'] = $data['sponsorval']+$data['netlevel1val']+$data['netlevel2val']+$data['netlevel3val']+$data['netlevel4val']+$data['netlevel5val']+$data['netlevel6val']+$data['netlevel7val']+$data['netlevel8val']+$data['netlevel9val']+$data['netlevel10val'];
		}else{
			$data['personal'] = 0;
			$data['sponsor'] = 0;
			$data['sponsorval'] = 0;
			$data['netlevel1'] = 0;
			$data['netlevel1val'] = 0;
			$data['netlevel2'] = 0;
			$data['netlevel2val'] = 0;
			$data['netlevel3'] = 0;
			$data['netlevel3val'] = 0;
			$data['netlevel4'] = 0;
			$data['netlevel4val'] = 0;
			$data['netlevel5'] = 0;
			$data['netlevel5val'] = 0;
			$data['netlevel6'] = 0;
			$data['netlevel6val'] = 0;
			$data['netlevel7'] = 0;
			$data['netlevel7val'] = 0;
			$data['netlevel8'] = 0;
			$data['netlevel8val'] = 0;
			$data['netlevel9'] = 0;
			$data['netlevel9val'] = 0;
			$data['netlevel10'] = 0;
			$data['netlevel10val'] = 0;			
			$data['totalkomisi'] = 0;
		}		
		
		if ($periode>1302){
			$data['template']='mmember/royalty_viewnet';
		}else{
			$data['template']='mmember/royalty_last_viewnet';
		}
		$data['title'] ="Bonus Leveling Members";
		$data['breadcrum'] = array(array("Master","#","frames"),array("Members",'members',"point_right"),array("View Bonus Leveling",'#',"active"));		
		$data['active'] =226;
		//print_r($periode);exit();
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function viewkomisiother($noid=0,$page=0,$urlbef=0,$control=0,$bln=0,$thn=0)
	{	$this->session->set_userdata('page', 'Members - Bonus Lainnya');
		if ($bln && $thn){
			$bulan=$bln;
			$tahun=$thn;
			$periode=$tahun.$bulan;
		}else{
			if (isset($_POST['bulan'])){
				$noid=$this->input->post('noid');	
				$page=$this->input->post('page');
				$urlbef=$this->input->post('urlbef');
				$bulan=$this->input->post('bulan');
				$tahun=$this->input->post('tahun');
				$periode=$tahun.$bulan;
			}else{
				$bulan=date("m");
				$tahun=date("y");
				$periode=$tahun.$bulan;
			}
		}
		$data['noid'] = $noid;
		if($query = $this->mmember_model->cek_upline($noid)){
			$data['namamembers'] = $query->namamembers;
		}
		$data['bulan'] = $bulan;
		$data['tahun'] = $tahun;
		$data['control'] = $control;
		$data['page'] = $page;
		$data['urlbef'] = $urlbef;
/*		if($query = $this->mmember_model->cek_omzet($periode)){
			$data['omzetp'] = $query->total;
		}else{
			$data['omzetp'] = 0;
		}
*/		
		$data['omzetp'] = $this->mmember_model->get_omzet($periode);
		$data['omzeta'] = $this->mmember_model->get_omzeta($periode);	
		$data['passup'] = $this->mmember_model->check_passup($periode);
		if ($data['passup']){
			$data['omzeta_pu'] = $this->mmember_model->get_omzeta_pu($periode);	
		}else{
			$data['omzeta_pu'] = 0;
		}
		if($row = $this->mmember_model->get_komisiother($noid,$periode)){
			$data['personal'] = $row->personal;
			$data['salejalur1'] = $row->salejalur1;
			$data['salejalur2'] = $row->salejalur2;
			$data['salejalur3'] = $row->salejalur3;
			$data['salejalur4'] = $row->salejalur4;
			$data['salejalur5'] = $row->salejalur5;
			if ($data['passup']){
				if($row = $this->mmember_model->get_recordjaminan_pu($periode,$noid))
				{
					$data['salejalur1_pu'] = $row->salejalur1;
					$data['salejalur2_pu'] = $row->salejalur2;
					$data['salejalur3_pu'] = $row->salejalur3;
					$data['salejalur4_pu'] = $row->salejalur4;
					$data['salejalur5_pu'] = $row->salejalur5;
					$data['omzetg_pu'] = $row->omzetg;
				}else{
					$data['salejalur1_pu'] = $data['salejalur2_pu'] = $data['salejalur3_pu'] = $data['salejalur4_pu'] = $data['salejalur5_pu'] = $data['omzetg_pu'] = 0;
				}
			}else{
				$data['salejalur1_pu'] = $data['salejalur2_pu'] = $data['salejalur3_pu'] = $data['salejalur4_pu'] = $data['salejalur5_pu'] = $data['omzetg_pu'] = 0;
			}
		}else{
			$data['personal'] = $data['salejalur1'] = $data['salejalur2'] = $data['salejalur3'] = $data['salejalur4'] = $data['salejalur5'] = $data['omzetg'] = 0;
			$data['salejalur1_pu'] = $data['salejalur2_pu'] = $data['salejalur3_pu'] = $data['salejalur4_pu'] = $data['salejalur5_pu'] = $data['omzetg_pu'] = 0;
		}		
		
		$data['omzetg']=$data['personal']+$data['salejalur1']+$data['salejalur2']+$data['salejalur3']+$data['salejalur4']+$data['salejalur5'];
		if ($periode>1302){
			if ($periode<1401){
				$data['pengali']=150;
			}else{
				$data['pengali']=175;
			}
			if ($data['omzetg'] >= 10000 && $data['personal']>=30 && $data['salejalur1'] >=1000 && $data['salejalur2'] >=1000 && $data['salejalur3'] >=1000 && $data['salejalur4'] >=1000 && $data['salejalur5'] >=1000 ){
				$data['hasilroy']="Rp ".number_format(($data['pengali']*$data['omzetp'])*($data['omzetg']/$data['omzeta']),0,",",".").",-";
				$data['msg3']= $data['hasilroy']. "<br>( Sementara Sebelum Pass-Up )";
				if ($data['passup'] && $data['omzetg_pu'] >= 10000 && $data['personal']>=30 && $data['salejalur1_pu'] >=1000 && $data['salejalur2_pu'] >=1000 && $data['salejalur3_pu'] >=1000 && $data['salejalur4_pu'] >=1000 && $data['salejalur5_pu'] >=1000 ){
					$data['hasilroy_pu']="Rp ".number_format(($data['pengali']*$data['omzetp'])*($data['omzetg_pu']/$data['omzeta_pu']),0,",",".").",-";
					$data['msg3']=$data['hasilroy_pu']. "<br>( Hasil Setelah Pass-Up )";
				}else{
					$data['hasilroy_pu']="Rp 0,-";
				}
			}else{
				$data['hasilroy']="Rp 0,-";
				$data['hasilroy_pu']="Rp 0,-";
			}
			$data['template']='mmember/royalty_viewother';
		}else{
			if ($data['personal']>=30 && $data['omzetg']>=10000 && $data['salejalur1'] >=300 && $data['salejalur2'] >=300 && $data['salejalur3'] >=300 && $data['salejalur4'] >=300 && $data['salejalur5'] >=300 ){
				$data['msg1']="Rp 1.000.000,-";
			}
			if ($data['personal']>=30 && $data['omzetg']>=200000 && $data['salejalur1'] >=1000 && $data['salejalur2'] >=1000 && $data['salejalur3'] >=1000 && $data['salejalur4'] >=1000 && $data['salejalur5'] >=1000 ){
				$data['msg2']="Umroh Atau Rp 30.000.000,-";
			}elseif ($data['personal']>=30 && $data['omzetg']>=100000 && $data['salejalur1'] >=750 && $data['salejalur2'] >=750 && $data['salejalur3'] >=750 && $data['salejalur4'] >=750 && $data['salejalur5'] >=750 ){
				$data['msg2']="Motor Atau Rp 15.000.000,-";
			}elseif ($data['personal']>=30 && $data['omzetg']>=50000 && $data['salejalur1'] >=600 && $data['salejalur2'] >=600 && $data['salejalur3'] >=600 && $data['salejalur4'] >=600 && $data['salejalur5'] >=600 ){
				$data['msg2']="IPad Atau Rp 7.500.000,-";
			}elseif ($data['personal']>=30 && $data['omzetg']>=25000 && $data['salejalur1'] >=450 && $data['salejalur2'] >=450 && $data['salejalur3'] >=450 && $data['salejalur4'] >=450 && $data['salejalur5'] >=450 ){
				$data['msg2']="HP Atau Rp 3.750.000,-";
			}
			if ($data['personal']>=30 && $data['salejalur1'] >=2000 && $data['salejalur2'] >=2000 && $data['salejalur3'] >=2000 && $data['salejalur4'] >=2000 && $data['salejalur5'] >=2000 ){
				$data['msg3']=$data['hasilroy']="Rp ".number_format((150*$data['omzetp'])*($data['omzetg']/$data['omzeta']),0,",",".").",-";
			}else{
				$data['hasilroy']="Rp 0,-";
			}
			$data['template']='mmember/royalty_last_viewother';
		}	
		$data['breadcrum'] = array(array("Master","#","frames"),array("Members",'members',"point_right"),array("View Bonus Lain-lain",'#',"active"));		
		$data['title'] ="Komisi Lainnya Members";
		$data['active'] =220;
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function belanja($noid=0,$bln=0,$thn=0)
	{	
		if (isset($_POST['noid'])){
			
		}
		$this->session->set_userdata('page', 'Members - View Belanja');
		if($query = $this->mmember_model->cek_upline($noid)){
			$data['noid'] = $noid;
			$data['namamembers'] = $query->namamembers;
		}
		if ($bln && $thn){
			$bulan=$bln;
			$tahun=$thn;
			$periode=$tahun.$bulan;
		}else{
			if (isset($_POST['bulan'])){
				$bulan=$this->input->post('bulan');
				$tahun=$this->input->post('tahun');
				$periode=$tahun.$bulan;
			}else{
				$bulan=date("m");
				$tahun=date("y");
				$periode=$tahun.$bulan;
			}
		}
		$data['bulan'] = $bulan;
		$data['tahun'] = $tahun;
		$data['ajax_url'] = 'mmember/get_data_belanja/'.$noid.'/'.$periode; 
	
		$data['title'] ="List Belanja ".$data['namamembers']. " Periode : ";
		$data['active'] =227;
		$data['breadcrum'] = array(array("Master","#","frames"),array("Members",'members',"point_right"),array("View Belanja",'#',"active"));		
		$data['template'] = 'mmember/belanja_view';	
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function detail_jual($nojual=0)
	{	
		$data=array();
		$this->session->set_userdata('page', 'Members - Detail Belanja');
		$query = $this->all_model->header_jual_member($nojual);
		if ($query){
			$data['namacabang']=$query->namacabang;
			$data['noid']=$query->noid;
			$data['namamembers']=$query->namamembers;
			$data['noidstokies']=$query->noidstokies;
			$data['namastokies']=$query->namastokies;
			$data['nojual']=$query->nojual;
			$data['tgljual']=$query->tgljual;
			$data['query']=$this->all_model->detail_jual_member($nojual);
		}
		
		$data['title'] ="List Belanja Detail";
		// $data['active'] =227;
		$data['breadcrum'] = array(array("Master","#","frames"),array("Members",'members',"point_right"),array("View Belanja Detail",'#',"active"));		
		$data['template'] = 'mmember/belanja_view_detail';	
		$data = array_merge($data, backend_info());
		$this->parser->parse('module_template', $data);
	}
	function get_data_belanja($noid,$periode)
    {
        $query = $this->mmember_model->data_belanja($noid,$periode);
        $this->output->set_output($query);
    }	
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class All extends CI_Controller {

	/**
	 * Member controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
	  {
			parent::__construct();
			
	  }

	function s2_stokis() {
		$cari 	= $this->input->post('search');
		$data = $this->all_model->s2_stokis($cari);
		$this->output->set_output(json_encode($data));
	}
	function s2_kota() {
		$cari 	= $this->input->post('search');
		$data = $this->all_model->s2_kota($cari);
		$this->output->set_output(json_encode($data));
	}
	function s2_member() {
		$cari 	= $this->input->post('search');
		$data = $this->all_model->s2_member($cari);
		$this->output->set_output(json_encode($data));
	}
}

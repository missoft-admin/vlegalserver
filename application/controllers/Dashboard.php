<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	
	function index()
    {
		// print_r('Sini');exit();
        $data = array();
        $data['title']      = 'Dashboard';
        $data['template']    = 'dashboard';
        $data['breadcrum']  = array(
                                array("Dashboard",'#')
                              );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }
}

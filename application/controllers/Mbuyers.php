<?php
defined('BASEPATH') OR exit('Hacking attempt: Out of System!');

  /**
   * Develop by Deni Purnama
   * denipurnama371@gmail.com
   */
class Mbuyers extends CI_Controller {
private $table      = 'kit_buyer'; 
private $tbljson    = 'kit_negara';
private $field      = 'buyer';
private $label      = 'Buyer';
    public function __construct()
    {
        parent::__construct();
        // PermissionUserLoggedIn($this->session);
            $this->load->model('Mbuyers_model','model');
            // $_SESSION['logged_in']=false;
            // $this->form_validation->set_error_delimiters('<label>', '</label>');
    }
	
    function index()
    {
exit('Hacking attempt: Out of System!');
    }

	function showingData()
    {
        $data = array();
        $data['title']      = 'Setting - Buyers';
        $data['template']   = 'Mbuyers/index';
        $data['tJudul']      = $this->label;
        $data['dJudul']      = $this->field;
        $data['url_ajax']   = site_url().'ajax/buyers';
        $data['breadcrum']  = array(
                                array("Area Admin",'#'),
                                array($this->label,'#'),
                                array("List",'setting/buyers')
                              );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    function insertBaru()
    {
        $data = array();
        $data['title']      = 'Setting - New Buyer';
        $data['template']   = 'Mbuyers/manage';
        $data['tJudul']      = $this->label;
        $data['dJudul']      = $this->field;
        $data['url_ajax']   = site_url().'ajax/buyers';
        $data['url_proses'] = site_url().'setting/new-buyer/proses';
        $data['breadcrum']  = array(
                                array("Area Admin",'#'),
                                array($this->label,'#'),
                                array("New",'setting/buyers')
                              );
$data['newbuyer']='';
$data['newalamat']='';
$data['newstatus']='';
$data['newnegara']='';
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    function indexUpdate()
    {
$id=decryptURL($this->uri->segment(3));
$get=rowWhere('id'.$this->field,$id,$this->table);
$getN=rowWhere('idnegara',$get->idnegara,$this->tbljson);
        $data = array();
        $data['title']      = 'Setting - Edit Buyer';
        $data['template']   = 'Mbuyers/manage';
        $data['tJudul']      = $this->label;
        $data['dJudul']      = $this->field;
        $data['url_ajax']   = site_url().'ajax/users';
        $data['url_proses']   = site_url().'setting/update-buyers/proses';
        $data['breadcrum']  = array(
                                array("Area Admin",'#'),
                                array($this->label,'#'),
                                array("Edit",'setting/buyers')
                              );
$data['newbuyer']=$get->buyer;
$data['newalamat']=$get->alamat;
$data['newstatus']=$get->xstatus;
$data['newnegara']=$getN->idnegara.','.$getN->negara;
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

function saveNew(){
    // print_r('oke');exit();
        $data['idnegara'] = $this->input->post('new-negara');
        $data['buyer']    = $this->input->post('new-buyer');
        $data['alamat']   = $this->input->post('new-alamat');
        $data['ket']      = ($data['idnegara']=="ID")?'D':'L';
        $data['xstatus']  = $this->input->post('new-status');
        $data['buyerstdelete'] = 1;
        if(saveData($this->table,$data)){
$_SESSION['msg']='ToastrSukses("'.$this->label.' baru telah ditambahkan","Info")';
        redirect(site_url().'setting/buyers');
        }else{
$_SESSION['msg']='Toastr("Maaf, '.$this->label.' gagal ditambahkan","Info")';
        redirect(site_url().'setting/new-buyer');
        }
}

function FupdateData(){
$where =array('id'.$this->field =>decryptURL($this->input->post('hide-ID')));
$idN=explode(",", $this->input->post('hide-negara'));
        $data['idnegara'] = $idN[0];
        $data['buyer']    = $this->input->post('new-buyer');
        $data['alamat']   = $this->input->post('new-alamat');
        $data['ket']      = ($data['idnegara']=="ID")?'D':'L';
        $data['xstatus']  = $this->input->post('new-status');
        $data['buyerstdelete'] = 1;

        if(updateData($where,$data,$this->table)){
$_SESSION['msg']='ToastrSukses("Buyer berhasil diubah","Info")';
        redirect(site_url().'setting/buyers');
        }else{
$_SESSION['msg']='Toastr("Maaf, data '.$this->label.' gagal diubah","Info")';
        redirect(site_url().'setting/new-buyer');
        }
}

        public function getListDT(){
            $table      = $this->table; 
            $field      = $this->field;
            $primaryKey = 'id'.$field;
            $sql_details = sql_connect();

                $columns = array(
                    array('db' => $primaryKey, 'dt' => 0, 'field' => $primaryKey),
                    array('db' => 'idnegara', 'dt' => 1, 'field' => 'idnegara'),
                    array('db' => $field, 'dt' => 2, 'field' => $field),
                    array('db' => 'alamat', 'dt' => 3, 'field' => 'alamat','formatter'=>function($d,$row){
                        return (strlen($d)<=200)?$d:substr($d, 0, 100).' ...';
                    }),
                    array('db' => 'xstatus', 'dt' => 4, 'field' => 'xstatus','formatter'=>function($d,$row){
                        return '<a href="javascript:void(0)" data-id="'.encryptURL($row['idbuyer']).'" class="statusAlamat">'.stUser($row['xstatus']).'</a>';
                    }),
                    array('db' => $primaryKey, 'dt' => 5, 'field' => $primaryKey, 'formatter' => function( $d, $row ) {
                return '<a href="'.site_url('setting/buyers/').encryptURL($d).'" class="btn btn-xs btn-info" title="Edit '.$this->label.'">
                <i class="far fa-edit"></i>
                </a><a href="javascript:void(0)" class="btn btn-xs btn-danger delete-user" data-id="'.encryptURL($d).'" title="Delete '.$this->label.'"><i class="far fa-trash-alt"></i></a>';
                                   }),
                );
            $joinQuery  = "";
            $extraWhere = $field."stdelete=1";
            $groupBy    = "";
            $ordercus   = "ORDER BY idnegara ASC,$field ASC";
            $having     = "";
            echo json_encode(
                SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having, $ordercus )
            );
        }



function delAkun() {
$id= decryptURL($this->input->post("id")); 
$aa=$this->model->delAkun($id);
echo rowWhere('id'.$this->field,$id,$this->table)->buyer;
}
function gantiStatus() {
$id= decryptURL($this->input->post("id")); 
$aa=$this->model->gantiStatus($id);
$bb = $this->model->dataStatus($id);
// $dataStatus_json = $bb;
echo stUser($bb);
}

}

<?php
defined('BASEPATH') OR exit('Hacking attempt: Out of System!');

  /**
   * Develop by Deni Purnama
   * denipurnama371@gmail.com
   */
class MgroupUsers extends CI_Controller {
private $table      = 'users_group'; 
private $field      = 'ugroup_';
    public function __construct()
    {
        parent::__construct();
        // PermissionUserLoggedIn($this->session);
            $this->load->model('MgroupUsers_model','model');
            // $_SESSION['logged_in']=false;
            // $this->form_validation->set_error_delimiters('<label>', '</label>');
    }
	
    function index()
    {
exit('Hacking attempt: Out of System!');
    }

	function showingData()
    {
        $data = array();
        $data['title']      = 'Setting - Group Users';
        $data['template']   = 'Mgroupuser/index';
        $data['url_ajax']   = site_url().'ajax/group-users';
        $data['breadcrum']  = array(
                                array("Area Admin",'#'),
                                array("Group Users",'#'),
                                array("List",'setting/group-users')
                              );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    function insertBaru()
    {
        $data = array();
        $data['title']      = 'Setting - Group Users';
        $data['template']   = 'Mgroupuser/manage';
        $data['url_ajax']   = site_url().'ajax/group-users';
        $data['url_proses'] = site_url().'setting/new-group/proses';
        $data['breadcrum']  = array(
                                array("Area Admin",'#'),
                                array("Group Users",'#'),
                                array("New",'setting/group-users')
                              );
$data['newtitle']='';
$data['newstatus']='';
$data['newmodules']='';
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    function indexUpdate()
    {
$id=decryptURL($this->uri->segment(3));
$get=rowWhere($this->field.'id',$id,$this->table);
        $data = array();
        $data['title']      = 'Setting - Group Users';
        $data['template']   = 'Mgroupuser/manage';
        $data['url_ajax']   = site_url().'ajax/users';
        $data['url_proses']   = site_url().'setting/update-group/proses';
        $data['breadcrum']  = array(
                                array("Area Admin",'#'),
                                array("Group Users",'#'),
                                array("Edit",'setting/group-users')
                              );
$data['newtitle']=$get->ugroup_title;
$data['newstatus']=$get->ugroup_status;
$data['newmodules']=$get->ugroup_modules;
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

function saveNew(){
date_default_timezone_set('Asia/Jakarta');
        $data[$this->field.'title'] = $this->input->post('new-title');
        $data[$this->field.'modules'] = join(",",$this->input->post('new-modules'));
        $data[$this->field.'status'] = $this->input->post('new-status');
        $data[$this->field.'create_date'] = date('Y-m-d H:i:s');
        $data[$this->field.'stdelete'] = 1;

        if(saveData($this->table,$data)){
$_SESSION['msg']='ToastrSukses("Group baru telah ditambahkan","Info")';
        redirect(site_url().'setting/group-users');
        }else{
$_SESSION['msg']='Toastr("Maaf, Group gagal ditambahkan","Info")';
        redirect(site_url().'setting/new-group');
        }
}

function FupdateData(){
$where =array($this->field.'id'         =>decryptURL($this->input->post('hide-ID')));
        $data[$this->field.'title']     = $this->input->post('new-title');
        $data[$this->field.'modules']   = join(",",$this->input->post('new-modules'));
        $data[$this->field.'status']    = $this->input->post('new-status');

        if(updateData($where,$data,$this->table)){
$_SESSION['msg']='ToastrSukses("User berhasil diubah","Info")';
        redirect(site_url().'setting/group-users');
        }else{
$_SESSION['msg']='Toastr("Maaf, data user gagal diubah","Info")';
        redirect(site_url().'setting/new-group');
        }
}

public function getGroupUsers()
    {
        echo json_encode(getData('users_group'));
    }

        public function getListDT(){
            $table      = $this->table; 
            $field      = $this->field;
            $primaryKey = $field.'id';
            $sql_details = sql_connect();

                $columns = array(
                    array('db' => $field.'title', 'dt' => 0, 'field' => $field.'title'),
                    array('db' => $field.'modules', 'dt' => 1, 'field' => $field.'modules','formatter'=>function($d,$row){
                        return (strlen($d)<=100)?$d:substr($d, 0, 100).' ...';
                    }),
                    array('db' => $field.'create_date', 'dt' => 2, 'field' => $field.'create_date'),
                    array('db' => $field.'status', 'dt' => 3, 'field' => $field.'id','formatter'=>function($d,$row){
                        return '<a href="javascript:void(0)" data-id="'.encryptURL($row[4]).'" class="statusAlamat">'.stUser($row['ugroup_status']).'</a>';
                    }),
                    array('db' => $field.'id', 'dt' => 4, 'field' => $field.'id', 'formatter' => function( $d, $row ) {
                return '<a href="'.site_url('setting/group-users/').encryptURL($d).'" class="btn btn-xs btn-info" title="Edit User">
                <i class="far fa-edit"></i>
                </a><a href="javascript:void(0)" class="btn btn-xs btn-danger delete-user" data-id="'.encryptURL($d).'" title="Delete User"><i class="far fa-trash-alt"></i></a>';
                                   }),
                );
            $joinQuery  = "";
            $extraWhere = $field."stdelete=1";
            $groupBy    = "";
            $ordercus   = "ORDER BY ".$field."title ASC";
            $having     = "";
            echo json_encode(
                SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having, $ordercus )
            );
        }



function delAkun() {
$id= decryptURL($this->input->post("id")); 
$aa=$this->model->delAkun($id);
echo rowWhere($this->field.'id',$id,$this->table)->ugroup_title;
}
function gantiStatus() {
$id= decryptURL($this->input->post("id")); 
$aa=$this->model->gantiStatus($id);
$bb = $this->model->dataStatus($id);
$dataStatus_json = $bb;
echo stUser($dataStatus_json);
}

}

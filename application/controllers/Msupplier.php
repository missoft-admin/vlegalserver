<?php
defined('BASEPATH') OR exit('Hacking attempt: Out of System!');

  /**
   * Develop by Deni Purnama
   * denipurnama371@gmail.com
   */
class Msupplier extends CI_Controller {
private $table      = 'kit_supplier'; 
private $tbljson    = 'kit_negara';
private $field      = 'supplier';
private $label      = 'Supplier';
    public function __construct()
    {
        parent::__construct();
        // PermissionUserLoggedIn($this->session);
            $this->load->model('Msupplier_model','model');
            // $_SESSION['logged_in']=false;
            // $this->form_validation->set_error_delimiters('<label>', '</label>');
    }
    
    function index()
    {
exit('Hacking attempt: Out of System!');
    }

    function showingData()
    {
        $data = array();
        $data['title']      = 'Setting - '.$this->label;
        $data['template']   = 'Msupplier/index';
        $data['tJudul']      = $this->label;
        $data['dJudul']      = 'supplier';
        $data['url_ajax']   = site_url().'ajax/supplier';
        $data['breadcrum']  = array(
                                array("Area Admin",'#'),
                                array($this->label,'#'),
                                array("List",'setting/supplier')
                              );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    function insertBaru()
    {
        $data = array();
        $data['title']      = 'Setting - New '.$this->label;
        $data['template']   = 'Msupplier/manage';
        $data['tJudul']      = $this->label;
        $data['dJudul']      = 'supplier';
        $data['url_ajax']   = site_url().'ajax/supplier';
        $data['url_proses'] = site_url().'setting/new-supplier/proses';
        $data['breadcrum']  = array(
                                array("Area Admin",'#'),
                                array($this->label,'#'),
                                array("New",'setting/supplier')
                              );
$data['newsupplier']='';
$data['newalamat']='';
$data['newstatus']='';
$data['newnegara']='';
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    function indexUpdate()
    {
$id=decryptURL($this->uri->segment(3));
$get=rowWhere('id'.$this->field,$id,$this->table);
$getN=rowWhere('idnegara',$get->idnegara,$this->tbljson);
        $data = array();
        $data['title']      = 'Setting - Edit '.$this->label;
        $data['template']   = 'Msupplier/manage';
        $data['tJudul']      = $this->label;
        $data['dJudul']      = 'supplier';
        $data['url_ajax']   = site_url().'ajax/users';
        $data['url_proses']   = site_url().'setting/update-supplier/proses';
        $data['breadcrum']  = array(
                                array("Area Admin",'#'),
                                array($this->label,'#'),
                                array("Edit",'setting/supplier')
                              );
$data['newsupplier']=$get->supplier;
$data['newalamat']=$get->noser;
$data['newstatus']=$get->xstatus;
$data['newnegara']=$get->idnegara.','.$getN->negara;
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

function saveNew(){
    // print_r('oke');exit();
        $data['idnegara'] = $this->input->post('new-negara');
        $data['supplier']    = $this->input->post('new-supplier');
        $data['noser']   = $this->input->post('new-alamat');
        // $data['ket']      = ($data['idnegara']=="ID")?'D':'L';
        $data['xstatus']  = $this->input->post('new-status');
        $data['supplierstdelete'] = 1;
        if(saveData($this->table,$data)){
$_SESSION['msg']='ToastrSukses("'.$this->label.' baru telah ditambahkan","Info")';
        redirect(site_url().'setting/supplier');
        }else{
$_SESSION['msg']='Toastr("Maaf, '.$this->label.' gagal ditambahkan","Info")';
        redirect(site_url().'setting/new-supplier');
        }
}

function FupdateData(){
$where =array('id'.$this->field =>decryptURL($this->input->post('hide-ID')));
$idN=explode(",", $this->input->post('hide-negara'));
        $data['idnegara'] = $idN[0];
        $data['supplier']    = $this->input->post('new-supplier');
        $data['noser']   = $this->input->post('new-alamat');
        // $data['ket']      = ($data['idnegara']=="ID")?'D':'L';
        $data['xstatus']  = $this->input->post('new-status');
        $data['supplierstdelete'] = 1;

        if(updateData($where,$data,$this->table)){
$_SESSION['msg']='ToastrSukses("'.$this->label.' berhasil diubah","Info")';
        redirect(site_url().'setting/supplier');
        }else{
$_SESSION['msg']='Toastr("Maaf, data '.$this->label.' gagal diubah","Info")';
        redirect(site_url().'setting/new-supplier');
        }
}

public function getNegara()
    {
$like=array('negara'=>(!empty($_GET['search'])?$_GET['search']:''));
$q=getLike($like,$this->tbljson);
    echo json_encode($q);
    }

        public function getListDT(){
            $table      = $this->table; 
            $field      = $this->field;
            $primaryKey = 'id'.$field;
            $sql_details = sql_connect();

                $columns = array(
                    array('db' => $primaryKey, 'dt' => 0, 'field' => $primaryKey),
                    array('db' => 'idnegara', 'dt' => 1, 'field' => 'idnegara'),
                    array('db' => $field, 'dt' => 2, 'field' => $field),
                    array('db' => 'noser', 'dt' => 3, 'field' => 'noser','formatter'=>function($d,$row){
                        return (strlen($d)<=200)?$d:substr($d, 0, 100).' ...';
                    }),
                    array('db' => 'xstatus', 'dt' => 4, 'field' => 'xstatus','formatter'=>function($d,$row){
                        return '<a href="javascript:void(0)" data-id="'.encryptURL($row['idsupplier']).'" class="statusAlamat">'.stUser($row['xstatus']).'</a>';
                    }),
                    array('db' => $primaryKey, 'dt' => 5, 'field' => $primaryKey, 'formatter' => function( $d, $row ) {
                return '<a href="'.site_url('setting/supplier/').encryptURL($d).'" class="btn btn-xs btn-info" title="Edit '.$this->label.'">
                <i class="far fa-edit"></i>
                </a><a href="javascript:void(0)" class="btn btn-xs btn-danger delete-user" data-id="'.encryptURL($d).'" title="Delete '.$this->label.'"><i class="far fa-trash-alt"></i></a>';
                                   }),
                );
            $joinQuery  = "";
            $extraWhere = $field."stdelete=1";
            $groupBy    = "";
            $ordercus   = "ORDER BY idnegara ASC,$field ASC";
            $having     = "";
            echo json_encode(
                SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having, $ordercus )
            );
        }



function delAkun() {
$id= decryptURL($this->input->post("id")); 
$aa=$this->model->delAkun($id);
echo rowWhere('id'.$this->field,$id,$this->table)->supplier;
}
function gantiStatus() {
$id= decryptURL($this->input->post("id")); 
$aa=$this->model->gantiStatus($id);
$bb = $this->model->dataStatus($id);
// $dataStatus_json = $bb;
echo stUser($bb);
}

}

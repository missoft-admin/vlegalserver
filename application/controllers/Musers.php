<?php
defined('BASEPATH') OR exit('Hacking attempt: Out of System!');

  /**
   * Develop by Deni Purnama
   * denipurnama371@gmail.com
   */
class Musers extends CI_Controller {
private $table='users';
    public function __construct()
    {
        parent::__construct();
        // PermissionUserLoggedIn($this->session);
            $this->load->model('Musers_model','model');
            // $_SESSION['logged_in']=false;
            // $this->form_validation->set_error_delimiters('<label>', '</label>');
    }
	
    function index()
    {
exit('Hacking attempt: Out of System!');
    }

	function showingData()
    {
        $data = array();
        $data['title']      = 'Setting - Users';
        $data['template']   = 'Muser/index';
        $data['url_ajax']   = site_url().'ajax/users';
        $data['breadcrum']  = array(
                                array("Area Admin",'#'),
                                array("Users",'#'),
                                array("List",'setting/users')
                              );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    function insertBaru()
    {
        $data = array();
        $data['title']      = 'Setting - Users';
        $data['template']   = 'Muser/manage';
        $data['url_ajax']   = site_url().'ajax/users';
        $data['url_proses']   = site_url().'setting/new-users/proses';
        $data['breadcrum']  = array(
                                array("Area Admin",'#'),
                                array("Users",'#'),
                                array("New",'setting/users')
                              );
$data['newusername']='';
$data['newpassword']='';
$data['newname']='';
$data['newemail']='';
$data['newusergroup']='';
$data['newstatus']='';
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    function indexUpdate()
    {
$id=decryptURL($this->uri->segment(3));
$get=rowWhere('user_id',$id,$this->table);
        $data = array();
        $data['title']      = 'Setting - Users';
        $data['template']   = 'Muser/manage';
        $data['url_ajax']   = site_url().'ajax/users';
        $data['url_proses']   = site_url().'setting/update-users/proses';
        $data['breadcrum']  = array(
                                array("Area Admin",'#'),
                                array("Users",'#'),
                                array("Edit",'setting/users')
                              );
        $data['newusername']=$get->user_name;
        $data['newpassword']=$get->user_password;
        $data['newname']=$get->name;
        $data['newemail']=$get->email;
        $data['newusergroup']=$get->group;
        $data['newstatus']=$get->status;
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

function saveNew(){
$saltword = $this->sessionadmin->SaltWord();
$passwd = sha1($saltword.md5($_POST['new-password']));
        $data['user_name'] = $_POST['new-username'];
        $data['user_password'] = $passwd;
        $data['name'] = $_POST['new-name'];
        $data['email'] = $_POST['new-email'];
        $data['group'] = $_POST['new-usergroup'];
        $data['status'] = $_POST['new-status'];

        if(saveData($this->table,$data)){
$_SESSION['msg']='ToastrSukses("User baru telah ditambahkan","Info")';
        redirect(site_url().'setting/users');
        }else{
$_SESSION['msg']='Toastr("Maaf, user gagal ditambahkan","Info")';
        redirect(site_url().'setting/new-users');
        }
}

function FupdateData(){
if(!empty($_POST['new-password'])){
$saltword = $this->sessionadmin->SaltWord();
$passwd = sha1($saltword.md5($_POST['new-password']));
        $data['user_password'] = $passwd;
}
$where=array('user_id'=>decryptURL($_POST['hide-usrID']));
// $where['user_id']=decryptURL($_POST['hide-usrID']);
        $data['user_name'] = $_POST['new-username'];
        $data['name'] = $_POST['new-name'];
        $data['email'] = $_POST['new-email'];
        $data['group'] = $_POST['new-usergroup'];
        $data['status'] = $_POST['new-status'];

        if(updateData($where,$data,$this->table)){
$_SESSION['msg']='ToastrSukses("User berhasil diubah","Info")';
        redirect(site_url().'setting/users');
        }else{
$_SESSION['msg']='Toastr("Maaf, data user gagal diubah","Info")';
        redirect(site_url().'setting/new-users');
        }
}


        public function getListDT(){
            $table = $this->table; 
            $primaryKey = 'user_id';
            $sql_details = sql_connect();

                $columns = array(
                    array('db' => '`t1`.`user_name`', 'dt' => 0, 'field' => 'user_name'),
                    array('db' => '`t1`.`name`', 'dt' => 1, 'field' => 'name' ),
                    array('db' => '`t1`.`email`', 'dt' => 2, 'field' => 'email'),
                    array('db' => '`t2`.`ugroup_title`', 'dt' => 3, 'field' => 'ugroup_title'),
                    array('db' => '`t1`.`status`', 'dt' => 4, 'field' => 'user_id','formatter'=>function($d,$row){
                        return '<a href="javascript:void(0)" data-id="'.encryptURL($d).'" class="statusAlamat">'.stUser($row['status']).'</a>';
                    }),
                    array('db' => '`t1`.`user_id`', 'dt' => 5, 'field' => 'user_id', 'formatter' => function( $d, $row ) {
                return '<a href="'.site_url('setting/users/').encryptURL($d).'" class="btn btn-xs btn-info" title="Edit User">
                <i class="far fa-edit"></i>
                </a><a href="javascript:void(0)" class="btn btn-xs btn-danger delete-user" data-id="'.encryptURL($d).'" title="Delete User"><i class="far fa-trash-alt"></i></a>';
                                   }),
                );
            $joinQuery  = "FROM `$table` as `t1` LEFT JOIN `users_group` as `t2` ON (`t1`.`group`=`t2`.`ugroup_id`)";
            $extraWhere = "`t1`.`active_status`=1";
            $groupBy    = "";
            $ordercus   = "ORDER BY `t1`.`user_name` ASC";
            $having     = "";
            echo json_encode(
                SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having, $ordercus )
            );
        }



function delAkun() {
        $id= decryptURL($_POST["id"]); 
    $aa=$this->model->delAkun($id);
        // $bb = $this->model->dataStatus($id);
        // $dataStatus_json = $bb;
    echo rowWhere('user_id',$id,$this->table)->name;
    }
function gantiStatus() {
        $id= decryptURL($_POST["id"]); 
    $aa=$this->model->gantiStatus($id);
        $bb = $this->model->dataStatus($id);
        $dataStatus_json = $bb;
    echo stUser($dataStatus_json);
    }



function books_page()
     {

          // Datatables Variables
          $draw = intval($this->input->get("draw"));
          $start = intval($this->input->get("start"));
          $length = intval($this->input->get("length"));

      $this->select = array('*');
      $this->from   = $this->table;
      $this->join   =array(
            array('users_group', 'users_group.ugroup_id = '.$this->table.'.group', '')
      );
      $this->where  = array();
      $this->order  = array(
        $this->table.'.user_name' => 'ASC'
      );
      $this->group  = array();

    $this->column_search   = array($this->table.'.user_name',$this->table.'.email','users_group.ugroup_title');
    $this->column_order    = array($this->table.'.user_name',$this->table.'.email','users_group.ugroup_title');

    $list = $this->datatable->get_datatables();
    $data = array();
    $no = $_POST['start'];
    // print_r($list);exit();
    foreach ($list as $r) {
$no++;
        $row = array();
               $row[] = $r->user_id;
               $row[] = $r->user_id;
                   $row[]= $r->user_name;
                   $row[]= $r->email;
                   $row[]= $r->group;
                   $row[]= stUser($r->status);

        $data[] = $row;
               
          }

          $output = array(
      "draw" => $_POST['draw'],
      "recordsTotal" => $this->datatable->count_all(),
      "recordsFiltered" => $this->datatable->count_all(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
     }


}

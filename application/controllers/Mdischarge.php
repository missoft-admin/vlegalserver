<?php
defined('BASEPATH') OR exit('Hacking attempt: Out of System!');

  /**
   * Develop by Deni Purnama
   * denipurnama371@gmail.com
   */
class Mdischarge extends CI_Controller {
private $table      = 'kit_discharge'; 
private $tbljson    = 'kit_negara';
public $link        = 'discharge';
public $field1      = 'iddischarge';
public $field2      = 'kode';
public $field3      = 'uraian';
public $folder      = 'Mdischarge';
public $label      = 'Discharge';
    public function __construct()
    {
        parent::__construct();
        // PermissionUserLoggedIn($this->session);
            $this->load->model($this->folder.'_model','model');
            // $_SESSION['logged_in']=false;
            // $this->form_validation->set_error_delimiters('<label>', '</label>');
    }
    
    function index()
    {
exit('Hacking attempt: Out of System!');
    }

    function showingData()
    {
// $data array() for basic HTML
        $data = array();
        $data['title']        = 'Setting - '.$this->label;
        $data['template']     = $this->folder.'/index';
        $data['tJudul']       = $this->label;
        $data['dJudul']       = $this->field1;
        $data['url_index']    = site_url().'setting/'.$this->link;
        $data['url_addnew']   = site_url().'setting/new-'.$this->link;
        $data['url_kedua']    = 'new-'.$this->link;
        $data['url_ajax']     = site_url().'ajax/'.$this->link;
        $data['url_delete']   = site_url().'ajax/del'.$this->label;
        $data['url_uStatus']  = site_url().'ajax/upStatus'.$this->label;
        $data['breadcrum']    = array(
                                array("Area Admin",'#'),
                                array($this->label,'#'),
                                array("List",'setting/'.$this->link)
                              );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    function insertBaru()
    {
// $data array() for basic HTML
        $data = array();
        $data['title']      = 'Setting - New '.$this->label;
        $data['template']   = $this->folder.'/manage';
        $data['tJudul']     = $this->label;
        $data['dJudul']     = $this->link;
        $data['url_index']  = site_url().'setting/'.$this->link;
        $data['url_addnew'] = site_url().'setting/new-'.$this->link;
        $data['url_kedua']  = 'new-'.$this->link;
        $data['url_ajax']   = site_url().'ajax/'.$this->link;
        $data['url_proses'] = site_url().'setting/new-'.$this->link.'/proses';
        $data['breadcrum']  = array(
                                array("Area Admin",'#'),
                                array($this->label,'#'),
                                array("New",'setting/new-'.$this->link)
                              );

// $data array() for value database
$data['new'.$this->field2]='';
$data['new'.$this->field3]='';
$data['newstatus']='';
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    function indexUpdate()
    {
$id=decryptURL($this->uri->segment(3));
$get=rowWhere($this->field1,$id,$this->table);
// $data array() for basic HTML
        $data = array();
        $data['title']      = 'Setting - Edit '.$this->label;
        $data['template']   = $this->folder.'/manage';
        $data['tJudul']     = $this->label;
        $data['dJudul']     = $this->link;
        $data['url_index']  = site_url().'setting/'.$this->link;
        $data['url_addnew'] = site_url().'setting/new-'.$this->link;
        $data['url_kedua']  = 'new-'.$this->link;
        $data['url_ajax']   = site_url().'ajax/'.$this->link;
        $data['url_proses'] = site_url().'setting/update-'.$this->link.'/proses';
        $data['breadcrum']  = array(
                                array("Area Admin",'#'),
                                array($this->label,'#'),
                                array("Edit",'setting/'.$this->link.'/'.$this->uri->segment(3))
                              );

// $data array() for value database
$data['new'.$this->field2]=$get->kode;
$data['new'.$this->field3]=$get->uraian;
$data['newstatus']=$get->xstatus;
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

function saveNew(){
        $data[$this->field2]    = $this->input->post('new-'.$this->field2);
        $data[$this->field3]    = $this->input->post('new-'.$this->field3);
        $data['xstatus']  = $this->input->post('new-status');
        $data[$this->link.'stdelete'] = 1;
        if(saveData($this->table,$data)){
$_SESSION['msg']='ToastrSukses("'.$this->label.' baru telah ditambahkan","Info")';
        redirect(site_url().'setting/'.$this->link);
        }else{
$_SESSION['msg']='Toastr("Maaf, '.$this->label.' gagal ditambahkan","Info")';
        redirect(site_url().'setting/new-'.$this->link);
        }
}

function FupdateData(){
$where =array($this->field1 =>decryptURL($this->input->post('hide-ID')));
        $data[$this->field2]    = $this->input->post('new-'.$this->field2);
        $data[$this->field3]    = $this->input->post('new-'.$this->field3);
        $data['xstatus']    = $this->input->post('new-status');

        if(updateData($where,$data,$this->table)){
$_SESSION['msg']='ToastrSukses("'.$this->label.' berhasil diubah","Info")';
        redirect(site_url().'setting/'.$this->link);
        }else{
$_SESSION['msg']='Toastr("Maaf, data '.$this->label.' gagal diubah","Info")';
        redirect(site_url().'setting/new-'.$this->link);
        }
}

        public function getListDT(){
            $table      = $this->table; 
            // $field      = $this->field;
            $primaryKey = $this->field1;
            $sql_details = sql_connect();

                $columns = array(
                    array('db' => $primaryKey, 'dt' => 0, 'field' => $primaryKey),
                    array('db' => $this->field2, 'dt' => 1, 'field' => $this->field2),
                    array('db' => $this->field3, 'dt' => 2, 'field' => $this->field3),
                    array('db' => 'xstatus', 'dt' => 3, 'field' => 'xstatus','formatter'=>function($d,$row){
                        return '<a href="javascript:void(0)" data-id="'.encryptURL($row[$this->field1]).'" class="statusAlamat">'.stUser($row['xstatus']).'</a>';
                    }),
                    array('db' => $primaryKey, 'dt' => 4, 'field' => $primaryKey, 'formatter' => function( $d, $row ) {
                return '<a href="'.site_url('setting/'.$this->link.'/').encryptURL($d).'" class="btn btn-xs btn-info" title="Edit '.$this->label.'">
                <i class="far fa-edit"></i>
                </a><a href="javascript:void(0)" class="btn btn-xs btn-danger delete-user" data-id="'.encryptURL($d).'" title="Delete '.$this->label.'"><i class="far fa-trash-alt"></i></a>';
                                   }),
                );
            $joinQuery  = "";
            $extraWhere = $this->link."stdelete=1";
            $groupBy    = "";
            $ordercus   = "ORDER BY ".$this->field3." ASC";
            $having     = "";
            echo json_encode(
                SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having, $ordercus )
            );
        }



function delAkun() {
$id= decryptURL($this->input->post("id")); 
$aa=$this->model->delAkun($id);
$get=rowWhere($this->field1,$id,$this->table);
echo $get->kode.' - '.$get->uraian;
}
function gantiStatus() {
$id= decryptURL($this->input->post("id")); 
$aa=$this->model->gantiStatus($id);
$bb = $this->model->dataStatus($id);
// $dataStatus_json = $bb;
echo stUser($bb);
}

}

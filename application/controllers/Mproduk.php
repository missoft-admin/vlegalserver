<?php
defined('BASEPATH') OR exit('Hacking attempt: Out of System!');

  /**
   * Develop by Deni Purnama
   * denipurnama371@gmail.com
   */
class Mproduk extends CI_Controller {
private $table      = 'kit_produk'; 
private $tbljson    = 'kit_negara';
private $field      = 'produk';
private $label      = 'Produk';
    public function __construct()
    {
        parent::__construct();
        // PermissionUserLoggedIn($this->session);
            $this->load->model('Mproduk_model','model');
            // $_SESSION['logged_in']=false;
            // $this->form_validation->set_error_delimiters('<label>', '</label>');
    }
    
    function index()
    {
exit('Hacking attempt: Out of System!');
    }

    function showingData()
    {
        $data = array();
        $data['title']        = 'Setting - '.$this->label;
        $data['template']     = 'Mproduk/index';
        $data['tJudul']       = $this->label;
        $data['dJudul']       = $this->field;
        $data['url_ajax']     = site_url().'ajax/produk';
        $data['url_delete']   = site_url().'ajax/delProduk';
        $data['url_uStatus']  = site_url().'ajax/upStatusProduk';
        $data['breadcrum']  = array(
                                array("Area Admin",'#'),
                                array($this->label,'#'),
                                array("List",'setting/produk')
                              );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    function insertBaru()
    {
        $data = array();
        $data['title']      = 'Setting - New '.$this->label;
        $data['template']   = 'Mproduk/manage';
        $data['tJudul']      = $this->label;
        $data['dJudul']      = $this->field;
        $data['url_ajax']   = site_url().'ajax/produk';
        $data['url_proses'] = site_url().'setting/new-produk/proses';
        $data['breadcrum']  = array(
                                array("Area Admin",'#'),
                                array($this->label,'#'),
                                array("New",'setting/produk')
                              );
$data['kodehs']='';
$data['newproduk']='';
$data['newstatus']='';
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    function indexUpdate()
    {
$id=decryptURL($this->uri->segment(3));
$get=rowWhere('id'.$this->field,$id,$this->table);
        $data = array();
        $data['title']      = 'Setting - Edit '.$this->label;
        $data['template']   = 'Mproduk/manage';
        $data['tJudul']      = $this->label;
        $data['dJudul']      = $this->field;
        $data['url_ajax']   = site_url().'ajax/produk';
        $data['url_proses']   = site_url().'setting/update-produk/proses';
        $data['breadcrum']  = array(
                                array("Area Admin",'#'),
                                array($this->label,'#'),
                                array("Edit",'setting/produk')
                              );
$data['kodehs']=$get->kodehs;
$data['newproduk']=$get->produk;
$data['newstatus']=$get->xstatus;
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

function saveNew(){
        $data['kodehs']    = $this->input->post('new-kodehs');
        $data[$this->field]    = $this->input->post('new-produk');
        $data['xstatus']  = $this->input->post('new-status');
        $data[$this->field.'stdelete'] = 1;
        if(saveData($this->table,$data)){
$_SESSION['msg']='ToastrSukses("'.$this->label.' baru telah ditambahkan","Info")';
        redirect(site_url().'setting/produk');
        }else{
$_SESSION['msg']='Toastr("Maaf, '.$this->label.' gagal ditambahkan","Info")';
        redirect(site_url().'setting/new-produk');
        }
}

function FupdateData(){
$where =array('id'.$this->field =>decryptURL($this->input->post('hide-ID')));
        $data['kodehs']    = $this->input->post('new-kodehs');
        $data[$this->field]    = $this->input->post('new-produk');
        $data['xstatus']  = $this->input->post('new-status');

        if(updateData($where,$data,$this->table)){
$_SESSION['msg']='ToastrSukses("'.$this->label.' berhasil diubah","Info")';
        redirect(site_url().'setting/produk');
        }else{
$_SESSION['msg']='Toastr("Maaf, data '.$this->label.' gagal diubah","Info")';
        redirect(site_url().'setting/new-produk');
        }
}

        public function getListDT(){
            $table      = $this->table; 
            $field      = $this->field;
            $primaryKey = 'id'.$field;
            $sql_details = sql_connect();

                $columns = array(
                    array('db' => $primaryKey, 'dt' => 0, 'field' => $primaryKey),
                    array('db' => 'kodehs', 'dt' => 1, 'field' => 'kodehs'),
                    array('db' => 'produk', 'dt' => 2, 'field' => 'produk'),
                    array('db' => 'xstatus', 'dt' => 3, 'field' => 'xstatus','formatter'=>function($d,$row){
                        return '<a href="javascript:void(0)" data-id="'.encryptURL($row['idproduk']).'" class="statusAlamat">'.stUser($row['xstatus']).'</a>';
                    }),
                    array('db' => $primaryKey, 'dt' => 4, 'field' => $primaryKey, 'formatter' => function( $d, $row ) {
                return '<a href="'.site_url('setting/produk/').encryptURL($d).'" class="btn btn-xs btn-info" title="Edit '.$this->label.'">
                <i class="far fa-edit"></i>
                </a><a href="javascript:void(0)" class="btn btn-xs btn-danger delete-user" data-id="'.encryptURL($d).'" title="Delete '.$this->label.'"><i class="far fa-trash-alt"></i></a>';
                                   }),
                );
            $joinQuery  = "";
            $extraWhere = $field."stdelete=1";
            $groupBy    = "";
            $ordercus   = "ORDER BY produk ASC";
            $having     = "";
            echo json_encode(
                SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having, $ordercus )
            );
        }



function delAkun() {
$id= decryptURL($this->input->post("id")); 
$aa=$this->model->delAkun($id);
echo rowWhere('id'.$this->field,$id,$this->table)->produk;
}
function gantiStatus() {
$id= decryptURL($this->input->post("id")); 
$aa=$this->model->gantiStatus($id);
$bb = $this->model->dataStatus($id);
// $dataStatus_json = $bb;
echo stUser($bb);
}

}

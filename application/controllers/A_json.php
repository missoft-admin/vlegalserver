<?php
defined('BASEPATH') OR exit('Hacking attempt: Out of System!');

  /**
   * Develop by Deni Purnama
   * denipurnama371@gmail.com
   */
class A_json extends CI_Controller {
private $tblNegara       = 'kit_negara';
private $tblClients      = 'clients';
private $tblSupplier     = 'kit_supplier';
private $tblLoading      = 'kit_loading';
private $tblGroupUser    = 'users_group';
private $tblPropinsi     = 'kit_propinsi';
private $tblKabupaten    = 'kit_kabupaten';
private $tblUsers        = 'users';
private $tblSetting      = 'kit_setting';
private $tblBuyer        = 'kit_buyer';
private $tblSortimen     = 'kit_sortimen';
private $tblWip          = 'kit_wip';
private $tblProduk       = 'kit_produk';

    public function __construct()
    {
        parent::__construct();

    }
    
    function index()
    {
exit('Hacking attempt: Out of System!');
    }

public function getNegara()
    {
$select='idnegara,negara';
$where['xstatus']=1;
    if(!empty($_GET['id'])){
    if (!is_numeric($_GET['id'])) {
$id     = decryptURL($_GET['id']);
    } else {
$id     = $_GET['id'];
    }
    $where['idnegara']=$id;
    }
$like=array('negara'=>(!empty($_GET['search'])?$_GET['search']:''));
$q=getLike($like,$this->tblNegara,$select,$where);
    echo json_encode($q);
    }

public function getClients()
    {
$select='client_nama,client_id';
$where['client_stdelete']=1;
    if(!empty($_GET['id'])){
    if (!is_numeric($_GET['id'])) {
$id     = decryptURL($_GET['id']);
    } else {
$id     = $_GET['id'];
    }
    $where['client_id']=$id;
    }else{
    $where['client_id']=0;
    }
$like=array('client_nama'=>(!empty($_GET['search'])?$_GET['search']:''));
$q=getLike($like,$this->tblClients,$select,$where);
// print_r($this->db->last_query());exit();
    echo json_encode($q);
    } 

    public function getIui()
    {   
$select='client_id';
        for($i=1;$i<=5;$i++){   
        $select.=',client_iui_jenis_ijin'.$i.',
         client_primer_jenis_ijin'.$i.',
         client_nomor_ijin'.$i.',
         client_instansi_ijin'.$i.',
         client_tglterbit_ijin'.$i.',
         client_jenisproduk_ijin'.$i;
        }
$where['client_stdelete']=1;
    if(!empty($_GET['id'])){
    if (!is_numeric($_GET['id'])) {
$id     = decryptURL($_GET['id']);
    } else {
$id     = $_GET['id'];
    }
    $where['client_id']=$id;
    }else{
    $where['client_id']=0;
    }
        $query = getSelect($this->tblClients,$select,$where);
        $no=1;
        $dataclient_json = '{"client":[';
        foreach($query as $pecah){
            for($i=1;$i<=5;$i++){
                $a=($pecah['client_tglterbit_ijin'.$i]=='0000-00-00')?'':encode_date($pecah['client_tglterbit_ijin'.$i]);
                    $dataclient_json .= '{"id":"'.$pecah['client_id'].'",
                    "iui_jenis_ijin":"'.strtoupper($pecah['client_iui_jenis_ijin'.$i]).'",
                    "primer_jenis_ijin":"'.ucfirst($pecah['client_primer_jenis_ijin'.$i]).'",
                    "nomor_ijin":"'.$pecah['client_nomor_ijin'.$i].'",
                    "instansi_ijin":"'.$pecah['client_instansi_ijin'.$i].'",
                    "tglterbit_ijin":"'.$a.'",
                    "jenisproduk_ijin":"'.$pecah['client_jenisproduk_ijin'.$i].'"},';     
            }
        }
        $dataclient_json = substr($dataclient_json, 0, strlen($dataclient_json) - 1);
        $dataclient_json .= ']}';
        echo $dataclient_json;
}

public function getSupplier()
    {
$where['supplierstdelete']=1;
    if(!empty($_GET['id'])){
    if (!is_numeric($_GET['id'])) {
$id     = decryptURL($_GET['id']);
    } else {
$id     = $_GET['id'];
    }
    $where['idsupplier']=$id;
    }
$limit=(!empty($_GET['page']))?$_GET['page']:'';
$like=array('supplier'=>(!empty($_GET['search'])?$_GET['search']:''));
$q=getLike($like,$this->tblSupplier,'',$where,$limit);
    echo json_encode($q);
    }

public function getLoading()
    {
$select='idloading,uraian';
$where['loadingstdelete']=1;
    if(!empty($_GET['id'])){
    if (!is_numeric($_GET['id'])) {
$id     = decryptURL($_GET['id']);
    } else {
$id     = $_GET['id'];
    }
    $where['idloading']=$id;
    }
$like=array('uraian'=>(!empty($_GET['search'])?$_GET['search']:''));
$q=getLike($like,$this->tblLoading,$select,$where);
    echo json_encode($q);
    }

public function getProvinsi()
    {
$select='propid,nama,SUBSTRING(kode, 1,2) as kode';
$where=(!empty($_GET['kode']))?array('SUBSTRING(kode, 1,2)'=>$_GET['kode']):'';
    if(!empty($_GET['id'])){
    if (!is_numeric($_GET['id'])) {
$id     = decryptURL($_GET['id']);
    } else {
$id     = $_GET['id'];
    }
    $where['propid']=$id;
    }
$like=array('nama'=>(!empty($_GET['search'])?$_GET['search']:''));
$q=getLike($like,$this->tblPropinsi,$select,$where);
    echo json_encode($q);
    }
public function getKabupaten()
    {
$select='kabid,nama,SUBSTRING(kode, 1,2) as kode';
    if(!empty($_GET['id'])){
    if (!is_numeric($_GET['id'])) {
$id     = decryptURL($_GET['id']);
    } else {
$id     = $_GET['id'];
    }
    $where['kabid']=$id;
    }
$where=(!empty($_GET['kode']))?array('SUBSTRING(kode, 1,2)='=>$_GET['kode']):'';
$like=array('nama'=>(!empty($_GET['search'])?$_GET['search']:''));
$q=getLike($like,$this->tblKabupaten,$select,$where);
    echo json_encode($q);
    }

public function getGroupUsers()
    {
        echo json_encode(getData($this->tblGroupUser));
    }
 
public function getUsers()
    {
$select='user_id,user_name,name';
$where['active_status']=1;
    if(!empty($_GET['id'])){
    if (!is_numeric($_GET['id'])) {
$id     = decryptURL($_GET['id']);
    } else {
$id     = $_GET['id'];
    }
    $where['user_id']=$id;
    }
$like=array('name'=>(!empty($_GET['search'])?$_GET['search']:''));
$q=getLike($like,$this->tblUsers,$select,$where);
    echo json_encode($q);
    }
 
public function getBuyer()
    {
$select='idbuyer,buyer,idnegara';
$where['xstatus']=1;
$where['buyerstdelete']=1;
    if(!empty($_GET['id'])){
    if (!is_numeric($_GET['id'])) {
$id     = decryptURL($_GET['id']);
    } else {
$id     = $_GET['id'];
    }
    $where['idbuyer']=$id;
    }
$limit=(!empty($_GET['page']))?$_GET['page']:'';
    // print_r($_GET['page']);exit();
$like=array('buyer'=>(!empty($_GET['search'])?$_GET['search']:''));
$q=getLike($like,$this->tblBuyer,$select,$where,$limit);
    echo json_encode($q);
    }

public function getSortimen()
    {
$select='idsortimen,sortimen';
$where['xstatus']=1;
$where['sortimenstdelete']=1;
    if(!empty($_GET['id'])){
    if (!is_numeric($_GET['id'])) {
$id     = decryptURL($_GET['id']);
    } else {
$id     = $_GET['id'];
    }
    $where['idsortimen']=$id;
    }
$limit=(!empty($_GET['page']))?$_GET['page']:'';
    // print_r($_GET['page']);exit();
$like=array('sortimen'=>(!empty($_GET['search'])?$_GET['search']:''));
$q=getLike($like,$this->tblSortimen,$select,$where,$limit);
    echo json_encode($q);
    }

public function getWip()
    {
$select='idwip,wip';
$where['xstatus']=1;
$where['wipstdelete']=1;
    if(!empty($_GET['id'])){
    if (!is_numeric($_GET['id'])) {
$id     = decryptURL($_GET['id']);
    } else {
$id     = $_GET['id'];
    }
    $where['idwip']=$id;
    }
$limit=(!empty($_GET['page']))?$_GET['page']:'';
    // print_r($_GET['page']);exit();
$like=array('wip'=>(!empty($_GET['search'])?$_GET['search']:''));
$q=getLike($like,$this->tblWip,$select,$where,$limit);
    echo json_encode($q);
    }

public function getProduk()
    {
$select='idproduk,produk,kodehs';
$where['xstatus']=1;
$where['produkstdelete']=1;
    if(!empty($_GET['id'])){
    if (!is_numeric($_GET['id'])) {
$id     = decryptURL($_GET['id']);
    } else {
$id     = $_GET['id'];
    }
    $where['idproduk']=$id;
    }
$limit=(!empty($_GET['page']))?$_GET['page']:'';
    // print_r($_GET['page']);exit();
$like=array('produk'=>(!empty($_GET['search'])?$_GET['search']:''));
$q=getLike($like,$this->tblProduk,$select,$where,$limit);
    echo json_encode($q);
    }

    public function detailclient1($id = null)
    {
        $data = array();
        if ($id !== null) {
            $cek = rowArray($this->tblClients,array('client_id'=> decryptURL($id)));
            if ($cek) {
                $this->parser->parse('Mclient/detailsatu', array('client_id' => decryptURL($id)));
            } else {
                echo "";
            }
        } else {
            echo "";
        }
    }

    public function detailclient2($id = null)
    {
        $data = array();
        if ($id !== null) {
            $cek = rowArray($this->tblSetting,array('idclient'=> decryptURL($id)));
            if ($cek) {
                $this->parser->parse('Mclient/detaildua', array('idclient' => decryptURL($id)));
            } else {
                echo "";
            }
        } else {
            echo "";
        }
    }


}

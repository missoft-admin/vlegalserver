<?php
defined('BASEPATH') OR exit('Hacking attempt: Out of System!');

  /**
   * Develop by Deni Purnama
   * denipurnama371@gmail.com
   */
class Msortimen extends CI_Controller {
private $table      = 'kit_sortimen'; 
private $tbljson    = 'kit_negara';
private $field      = 'sortimen';
private $label      = 'Sortimen';
    public function __construct()
    {
        parent::__construct();
        // PermissionUserLoggedIn($this->session);
            $this->load->model('Msortimen_model','model');
            // $_SESSION['logged_in']=false;
            // $this->form_validation->set_error_delimiters('<label>', '</label>');
    }
    
    function index()
    {
exit('Hacking attempt: Out of System!');
    }

    function showingData()
    {
// $data array() for basic HTML
        $data = array();
        $data['title']        = 'Setting - '.$this->label;
        $data['template']     = 'M'.$this->field.'/index';
        $data['tJudul']       = $this->label;
        $data['dJudul']       = $this->field;
        $data['url_index']    = site_url().'setting/'.$this->field;
        $data['url_addnew']   = site_url().'setting/new-'.$this->field;
        $data['url_kedua']    = 'new-'.$this->field;
        $data['url_ajax']     = site_url().'ajax/'.$this->field;
        $data['url_delete']   = site_url().'ajax/del'.$this->label;
        $data['url_uStatus']  = site_url().'ajax/upStatus'.$this->label;
        $data['breadcrum']    = array(
                                array("Area Admin",'#'),
                                array($this->label,'#'),
                                array("List",'setting/'.$this->field)
                              );

        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    function insertBaru()
    {
// $data array() for basic HTML
        $data = array();
        $data['title']      = 'Setting - New '.$this->label;
        $data['template']   = 'M'.$this->field.'/manage';
        $data['tJudul']     = $this->label;
        $data['dJudul']     = $this->field;
        $data['url_index']  = site_url().'setting/'.$this->field;
        $data['url_addnew'] = site_url().'setting/new-'.$this->field;
        $data['url_kedua']  = 'new-'.$this->field;
        $data['url_ajax']   = site_url().'ajax/'.$this->field;
        $data['url_proses'] = site_url().'setting/new-'.$this->field.'/proses';
        $data['breadcrum']  = array(
                                array("Area Admin",'#'),
                                array($this->label,'#'),
                                array("New",'setting/new-'.$this->field)
                              );

// $data array() for value database
$data['new'.$this->field]='';
$data['newstatus']='';
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

    function indexUpdate()
    {
$id=decryptURL($this->uri->segment(3));
$get=rowWhere('id'.$this->field,$id,$this->table);
// $data array() for basic HTML
        $data = array();
        $data['title']      = 'Setting - Edit '.$this->label;
        $data['template']   = 'M'.$this->field.'/manage';
        $data['tJudul']     = $this->label;
        $data['dJudul']     = $this->field;
        $data['url_index']  = site_url().'setting/'.$this->field;
        $data['url_addnew'] = site_url().'setting/new-'.$this->field;
        $data['url_kedua']  = 'new-'.$this->field;
        $data['url_ajax']   = site_url().'ajax/users';
        $data['url_proses'] = site_url().'setting/update-'.$this->field.'/proses';
        $data['breadcrum']  = array(
                                array("Area Admin",'#'),
                                array($this->label,'#'),
                                array("Edit",'setting/'.$this->field.'/'.$this->uri->segment(3))
                              );

// $data array() for value database
$data['new'.$this->field]=$get->sortimen;
$data['newstatus']=$get->xstatus;
        $data = array_merge($data, backend_info());
        $this->parser->parse('module_template', $data);
    }

function saveNew(){
        $data[$this->field]    = $this->input->post('new-'.$this->field);
        $data['xstatus']  = $this->input->post('new-status');
        $data[$this->field.'stdelete'] = 1;
        if(saveData($this->table,$data)){
$_SESSION['msg']='ToastrSukses("'.$this->label.' baru telah ditambahkan","Info")';
        redirect(site_url().'setting/'.$this->field);
        }else{
$_SESSION['msg']='Toastr("Maaf, '.$this->label.' gagal ditambahkan","Info")';
        redirect(site_url().'setting/new-'.$this->field);
        }
}

function FupdateData(){
$where =array('id'.$this->field =>decryptURL($this->input->post('hide-ID')));
        $data[$this->field] = $this->input->post('new-'.$this->field);
        $data['xstatus']    = $this->input->post('new-status');

        if(updateData($where,$data,$this->table)){
$_SESSION['msg']='ToastrSukses("'.$this->label.' berhasil diubah","Info")';
        redirect(site_url().'setting/'.$this->field);
        }else{
$_SESSION['msg']='Toastr("Maaf, data '.$this->label.' gagal diubah","Info")';
        redirect(site_url().'setting/new-'.$this->field);
        }
}

        public function getListDT(){
            $table      = $this->table; 
            $field      = $this->field;
            $primaryKey = 'id'.$field;
            $sql_details = sql_connect();

                $columns = array(
                    array('db' => $primaryKey, 'dt' => 0, 'field' => $primaryKey),
                    array('db' => $field, 'dt' => 1, 'field' => $field),
                    array('db' => 'xstatus', 'dt' => 2, 'field' => 'xstatus','formatter'=>function($d,$row){
                        return '<a href="javascript:void(0)" data-id="'.encryptURL($row['id'.$this->field]).'" class="statusAlamat">'.stUser($row['xstatus']).'</a>';
                    }),
                    array('db' => $primaryKey, 'dt' => 3, 'field' => $primaryKey, 'formatter' => function( $d, $row ) {
                return '<a href="'.site_url('setting/'.$this->field.'/').encryptURL($d).'" class="btn btn-xs btn-info" title="Edit '.$this->label.'">
                <i class="far fa-edit"></i>
                </a><a href="javascript:void(0)" class="btn btn-xs btn-danger delete-user" data-id="'.encryptURL($d).'" title="Delete '.$this->label.'"><i class="far fa-trash-alt"></i></a>';
                                   }),
                );
            $joinQuery  = "";
            $extraWhere = $field."stdelete=1";
            $groupBy    = "";
            $ordercus   = "ORDER BY $field ASC";
            $having     = "";
            echo json_encode(
                SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having, $ordercus )
            );
        }



function delAkun() {
$id= decryptURL($this->input->post("id")); 
$aa=$this->model->delAkun($id);
echo rowWhere('id'.$this->field,$id,$this->table)->sortimen;
}
function gantiStatus() {
$id= decryptURL($this->input->post("id")); 
$aa=$this->model->gantiStatus($id);
$bb = $this->model->dataStatus($id);
// $dataStatus_json = $bb;
echo stUser($bb);
}

}

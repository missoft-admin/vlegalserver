<?php

class Stokies_model extends CI_Model {
	function __construct()
	{
		
   	}
	function data_list($baru=0)
    {
		if ($baru=='1'){
			$where = "mstokies.tgldaftar LIKE '".date("Y-m")."%'";
		}
        $this->datatables->select('mstokies.noidstokies,
				mstokies.tgldaftar,mstokies.namastokies,mstokies.status,
				mstokies.noid,mstokies.lat,mstokies.lng,mstokies.online,
				mstokies.email,mmembers.namamembers,mkota.kota as kota,
				SUM(mstokiesstok.stok) as stok,mstokies.noidcabang,mcabang.namacabang');
        // $this->datatables->select('mstokies.*');
		$this->datatables->from('mstokies');
		
		$this->datatables->join('mmembers', 'mstokies.noid = mmembers.noid','LEFT');
		$this->datatables->join('mkota', 'mstokies.kota = mkota.idkota','LEFT');
		$this->datatables->join('mcabang', 'mstokies.noidcabang = mcabang.noidcabang','LEFT');
		$this->datatables->join('mstokiesstok', 'mstokies.noidstokies = mstokiesstok.noidstokies','left');
		$this->datatables->group_by('mstokies.noidstokies');
		if ($baru=='1'){
			$this->datatables->where($where);
		}
		$this->datatables->add_column('action', '', 'id');
		$this->datatables->add_column('xstatus', '', 'id');
		$this->datatables->add_column('map', '', 'id');
		// print_r($this->datatables->generate());exit();
        return $this->datatables->generate();
        
    }
	function get_liststokies($limit,$offset,$cari=0,$baru=0,$curcabang=0,$deleted = false)
    {
        $this->db->select('mstokies.noidstokies,mstokies.tgldaftar,mstokies.namastokies,mstokies.status,mstokies.noid,mstokies.lat,mstokies.lng,mstokies.online,mstokies.email,mmembers.namamembers,mkota.kota as kota,SUM(mstokiesstok.stok) as stok,mstokies.noidcabang,mcabang.namacabang');
//        $this->db->where('mbarang.jenis <>',0);
        if($deleted){
        	$this->db->where('mstokies.deleted',1);
        }else{ 
        	$this->db->where('mstokies.deleted',0);
        }
    	if($cari){
	    	$where = "mstokies.noidstokies NOT LIKE 'MP%' AND (mstokies.noidstokies LIKE '%".addslashes($cari)."%' OR mstokies.namastokies LIKE '%".addslashes($cari)."%' OR mkota.kota LIKE '%".addslashes($cari)."%' OR mstokies.kecamatan LIKE '%".addslashes($cari)."%')";
	    	$this->db->where($where);
	    }elseif($baru){
	    	$where = "mstokies.tgldaftar LIKE '".$baru."%'";
	    	$this->db->where($where);
	    	if ($curcabang){
	    		$this->db->where('mstokies.noidcabang',$curcabang);
	    	}
    	}else{
        	$this->db->not_like('mstokies.noidstokies', 'MP', 'after');
        }
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		
		$this->db->join('mmembers', 'mstokies.noid = mmembers.noid');
		$this->db->join('mkota', 'mstokies.kota = mkota.idkota');
		$this->db->join('mcabang', 'mstokies.noidcabang = mcabang.noidcabang');
		$this->db->join('mstokiesstok', 'mstokies.noidstokies = mstokiesstok.noidstokies','left');
//		$this->db->join('mbarang', 'mstokiesstok.kodebarang = mbarang.kodebarang','left');
		$this->db->group_by('mstokies.noidstokies');

    	if($baru){
			$this->db->order_by('mstokies.tgldaftar','DESC');
		}else{
			$this->db->order_by('mstokies.status','DESC');
			$this->db->order_by('mstokies.namastokies','ASC');
		}
		$query = $this->db->get('mstokies');
        return $query->result();
    }

	function count($cari=0,$baru=0,$curcabang=0,$deleted = false)
	{
        if($deleted){
        	$this->db->where('deleted',1);
        }else{ 
        	$this->db->where('deleted',0);
        }
	    if($cari){
	    	$where = "noidstokies NOT LIKE 'MP%' AND (noidstokies LIKE '%".addslashes($cari)."%' OR namastokies LIKE '%".addslashes($cari)."%' OR mkota.kota LIKE '%".addslashes($cari)."%')";
	    	$this->db->where($where);
	    }elseif($baru){
	    	$where = "tgldaftar LIKE '".$baru."%'";
	    	$this->db->where($where);
	    	if ($curcabang){
	    		$this->db->where('mstokies.noidcabang',$curcabang);
	    	}	    	
	    }else{
        	$this->db->not_like('noidstokies', 'MP', 'after');
        }
		$this->db->from('mstokies');
		$this->db->join('mkota', 'mstokies.kota = mkota.idkota');
		$query = $this->db->count_all_results();
        return $query;
	}
	function get_newnoidstokies() 
	{	$init=date("y");
    	$query = $this->db->query("SELECT noidstokies FROM mstokies WHERE noidstokies like '$init%' ORDER BY noidstokies DESC LIMIT 1");    	
		if ($row = $query->row()){
			$hasil=$row->noidstokies;
			$hasil = substr($hasil,2,5);
			$hasil = $hasil + 1;
			$hasil = $init.substr("00000",0, 5 - strlen($hasil)).$hasil;			
		}else{
			$hasil=$init."00001";
		}
		return $hasil;	
	}
	function get_stokies($noidstokies)
    {	
		$this->db->select('mstokies.noidstokies,mstokies.noidcabang,mstokies.namastokies,mstokies.noid,mmembers.namamembers,mstokies.alamat,
							mstokies.desa,mstokies.kecamatan,mstokies.kota,mstokies.telepon,mstokies.hp,mstokies.tgldaftar,mstokies.lat,mstokies.lng,mstokies.group,
							mstokies.email,mstokies.online,mstokies.norekening,mstokies.atasnama,mstokies.idbank,mstokies.kodebank,mstokies.bank,mstokies.cabang,
							mstokies.createdby,userc.nama AS namac,mstokies.editedby,usere.user_level AS ide,usere.nama AS namae
							,mkota.kota as nama_kota,mkota.propinsi');
		$this->db->from('mstokies');
		$this->db->where('mstokies.noidstokies',$noidstokies);
		$this->db->join("mkota", "mkota.idkota= mstokies.kota",'LEFT');
		$this->db->join("mmembers", "mstokies.noid = mmembers.noid");
		$this->db->join("zusers AS userc", "mstokies.createdby = userc.userid", 'left');
		$this->db->join("zusers AS usere", "mstokies.editedby = usere.userid", 'left');
		$this->db->limit(1);
		$query = $this->db->get();
		// print('ada saja'.$query);exit();
		return $query->row();
    }
	function get_stokies_rekening($noidstokies)
    {	
		$this->db->select('mstokies_rekening.idbank,mstokies_rekening.kodebank,mstokies_rekening.norekening,mstokies_rekening.atasnama,mstokies_rekening.bank,mstokies_rekening.cabang,
							mstokies_rekening.edit_by,mstokies_rekening.tgl_edit,usere.user_level AS ide,usere.nama AS namae');
		$this->db->from('mstokies_rekening');
		$this->db->where('mstokies_rekening.noidstokies',$noidstokies);
		$this->db->join("zusers AS usere", "mstokies_rekening.edit_by = usere.userid", 'left');
		$this->db->order_by('id','DESC');
		$this->db->limit(1);
		$query = $this->db->get();
		
		return $query->row();
		
    }
	
	function cek_members($noid)
	{
		$this->db->select('namamembers');
		$this->db->from('mmembers');
		$this->db->where('noid', $noid);
		$this->db->where('deleted',0);
		$this->db->limit(1);
		$query = $this->db->get();
        return $query->row();		
	} 
	function cek_nama_bank($kodebank)
	{
		// print_r($kodebank);exit();
		$this->db->select('namabank');
		$this->db->from('mkodebank');
		$this->db->where('id', $kodebank);
		$this->db->limit(1);
		$query = $this->db->get();
        return $query->row('namabank');		
	} 
	function add_record($data) 
	{
		$this->db->insert('mstokies', $data);
		return ;		
	}
	function add_record_rekening($data) 
	{
		// print_r($data);exit();
		$this->db->insert('mstokies_rekening', $data);
		return ;		
	}
	function update_record($noidstokies,$data) 
	{	
		$this->db->where('noidstokies',$noidstokies);
		$this->db->update('mstokies',$data);
		return;
	}
	function deletestokies($noidstokies)
	{
		$this->db->where('noidstokies',$noidstokies);
		$this->db->update('mstokies',array('deleted' => 1,'deletedby' => $this->session->userdata('userid')));
		return;
	}
	function cek_stokies($noids)
	{
		$this->db->select('namastokies');
		$this->db->from('mstokies');
		$this->db->where('noidstokies', $noids);
		$this->db->where('deleted',0);
		$this->db->limit(1);
		$query = $this->db->get();
        return $query->row();		
	}
	function countbelanja($noids,$periode)
	{
		$this->db->select('ttransjual2stokieshead.nojual');
		$this->db->from('ttransjual2stokieshead');
//		$this->db->join("ttransjual2stokiesdetail", "ttransjual2stokieshead.nojual = ttransjual2stokiesdetail.nojual");
		$this->db->where('ttransjual2stokieshead.noidstokies',$noids);
		$this->db->where('DATE_FORMAT(ttransjual2stokieshead.tgljual,"%y%m")',$periode);
//		$this->db->group_by('ttransjual2stokieshead.nojual');
		$query = $this->db->count_all_results();
        return $query;
	}
	function get_recordpenjualan($noids,$periode)
	{
		$this->datatables->select('ttransjualhead.nojual,ttransjualhead.tgljual,Sum(ttransjualdetail.jumlah) as total,zusers.nama AS namac');
		$this->datatables->from('ttransjualhead');
		$this->datatables->join("ttransjualdetail", "ttransjualhead.nojual = ttransjualdetail.nojual");
		$this->datatables->join("zusers", "ttransjualhead.createdby = zusers.userid", 'left');
		$this->datatables->where('ttransjualhead.noidstokies',$noids);
		$this->datatables->where('DATE_FORMAT(ttransjualhead.tgljual,"%y%m")',$periode);
		$this->datatables->group_by('ttransjualhead.nojual');
		// $this->datatables->order_by('ttransjualhead.nojual','DESC');
		
		
		// $query = $this->db->get();
		// return $query->result();
	}
	function get_recordbelanja($noids,$periode)
	{
		$this->datatables->select('ttransjual2stokieshead.nojual,
		ttransjual2stokieshead.tgljual,Sum(ttransjual2stokiesdetail.jumlah) as total,
		zusers.nama AS namac');
		$this->datatables->from('ttransjual2stokieshead');
		$this->datatables->join("ttransjual2stokiesdetail", "ttransjual2stokieshead.nojual = ttransjual2stokiesdetail.nojual");
		$this->datatables->join("zusers", "ttransjual2stokieshead.createdby = zusers.userid", 'left');
		$this->datatables->where('ttransjual2stokieshead.noidstokies',$noids);
		$this->datatables->where('DATE_FORMAT(ttransjual2stokieshead.tgljual,"%y%m")',$periode);
		$this->datatables->group_by('ttransjual2stokieshead.nojual');
		
		$this->datatables->add_column('action', '', 'id');
		// $this->datatables->order_by('ttransjual2stokieshead.nojual','DESC');
		
		return $this->datatables->generate();
		// $query = $this->db->get();
		// return $query->result();
		
	}
	
	
	function get_recordpenjualanexp($noids,$periode)
	{
		$this->db->select('ttransjualhead.nojual,ttransjualhead.tgljual,ttransjualhead.noid,
			mmembers.namamembers,Sum(ttransjualdetail.jumlah) as total,zusers.nama AS namac');
		$this->db->from('ttransjualhead');
		$this->db->join("ttransjualdetail", "ttransjualhead.nojual = ttransjualdetail.nojual");
		$this->db->join("zusers", "ttransjualhead.createdby = zusers.userid", 'left');
		$this->db->join("mmembers", "ttransjualhead.noid = mmembers.noid");
		$this->db->where('ttransjualhead.noidstokies',$noids);
		$this->db->where('DATE_FORMAT(ttransjualhead.tgljual,"%y%m")',$periode);
		$this->db->group_by('ttransjualhead.nojual');
		$this->db->order_by('ttransjualhead.nojual','ASC');
		$query = $this->db->get();
		return $query->result();
	}
	function countstok($noidstokies,$deleted = false)
	{
        $this->db->from('mbarang');
		$this->db->join("mstokiesstok", "mbarang.kodebarang = mstokiesstok.kodebarang");
		if($deleted){
        	$this->db->where('deleted',1);
        }else{ 
        	$this->db->where('deleted',0);
        }
		$this->db->where('mstokiesstok.noidstokies',$noidstokies);
        $query = $this->db->count_all_results();
        return $query;
	}	
	function get_liststokbarang($noidstokies,$limit,$offset,$deleted = false)
    {
		$this->db->select('mbarang.kodebarang,mbarang.namabarang,mbarang.hargajualstokis,
			mbarang.hargajualmember,mbarang.jenis,mstokiesstok.stok');
		$this->db->join("mstokiesstok", "mbarang.kodebarang = mstokiesstok.kodebarang");
    	if($deleted){
        	$this->db->where('mbarang.deleted',1);
        }else{ 
        	$this->db->where('mbarang.deleted',0);
        }
        $this->db->where('mstokiesstok.noidstokies',$noidstokies);
		($limit == '')?	$this->db->limit($offset,0) : $this->db->limit($offset,$limit);
		
		$this->db->order_by('namabarang','ASC');
		$query = $this->db->get('mbarang');
        return $query->result();
    }
	function get_kota()
	{
		$this->db->select('idkota,kota,propinsi');
		$this->db->from('mkota');
		$this->db->order_by('idkota','ASC');
		$query = $this->db->get();
		return $query->result();
	}
	function get_bank()
	{
		$this->db->select('mkodebank.*');
		$this->db->from('mkodebank');
		$this->db->order_by('namabank','ASC');
		$this->db->where('kodebank <>','0');
		$query = $this->db->get();
		return $query->result();
	}
	function find_stokis($namastokies){
		$this->db->select('mstokies.noidstokies');
		$this->db->from('mstokies');
		$this->db->where('namastokies',$namastokies);
		$query = $this->db->get();
		$query=$query->row('noidstokies');
		if ($query){
			return 'error';
		}else{
			return 'ok';
		}		
	}
	function find_noid($noid){
		$this->db->select('mmembers.namamembers');
		$this->db->from('mmembers');
		$this->db->where('noid',$noid);
		$query = $this->db->get();
		$query=$query->row('namamembers');
		if ($query){
			return $query;
		}else{
			return 'error';
		}		
	}
	function get_cabang()
	{
		$this->db->select('noidcabang,namacabang');
		$this->db->from('mcabang');
		$this->db->order_by('namacabang', 'ASC');
		$query = $this->db->get();
        return $query->result();		
	}
	function get_mapstokies()
	{
		$this->db->select('mstokies.namastokies,mstokies.alamat,mstokies.desa,mstokies.kecamatan,mkota.kota,mstokies.telepon,mstokies.hp,mkota.propinsi,mstokies.lat,mstokies.lng');
		$this->db->from('mstokies');
		$this->db->join("mkota", "mstokies.kota = mkota.idkota");
		$this->db->where('mstokies.lat <>', '');
		$this->db->where('mstokies.lng <>', '');
		$this->db->order_by('mkota.kota','ASC');
		$this->db->order_by('mstokies.namastokies','ASC');
		$query = $this->db->get();
		return $query->result();
	}  	
	function get_namacabang($noidcabang)
	{
		$query = $this->db->query("SELECT namacabang FROM mcabang WHERE noidcabang='$noidcabang' LIMIT 1");
		if ($row = $query->row()){$nmc="Cabang ".$row->namacabang;}else{$nmc='PUSAT';}
		return $nmc;	
	}
	function cek_namas($nama)
	{	$kutip="'";
		$query = $this->db->query('SELECT namastokies FROM mstokies 
		WHERE UPPER(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TRIM(mstokies.namastokies)," ",""),".",""),"'.$kutip.'",""),"-",""),",",""))="'.$nama.'" LIMIT 1');
		if ($row = $query->row()){$nmc=$row->namastokies;}else{$nmc=0;}
		return $nmc;	
	}
	
	function get_stokiesdata($noids)
	{
		$this->db->select('namastokies,noidcabang,email');
		$this->db->from('mstokies');
		$this->db->where('noidstokies', $noids);
		$this->db->limit(1);
		$query = $this->db->get();
        return $query->row();		
	}
	function add_userrecord($data) 
	{
		$this->db->insert('zusers', $data);
		return ;		
	}
	function sendmailonline($email,$namastokies,$username,$pwd)
	{	$this->load->library('email');
//		$email='banyups@gmail.com';
	
		$this->email->from('update@tridayasinergi.biz', 'Tridaya Sinergi');
		$this->email->to($email);
		$this->email->cc("lpd.sinergi@gmail.com");
		
		$this->email->subject('Tridaya Sinergi: Permohonan Online');
		$data['namastokies']=$namastokies;
		$data['username']=$username;
		$data['password']=$pwd;
		$temail = $this->load->view('email/useronline', $data, TRUE);
		$this->email->message($temail);
//		$this->email->message(ec_useronline($namastokies,$username,$pwd));

		if ( ! $this->email->send())
		{	//return  show_error($this->email->print_debugger());
            show_error($this->email->print_debugger());
			return false;
		} else {
			return true;        
		} 		
	}
    function resetpassword($noidstokies)
	{
		$this->db->where('noidstokies',$noidstokies);
		$this->db->update('zusers',array('passkey' => '44209a6a592dea91bcf7d4dd53e47a5a'));
		return;
	}
}

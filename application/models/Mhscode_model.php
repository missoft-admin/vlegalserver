<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mhscode_model extends CI_Model
{
    private $table = 'hscode';
    private $field = 'hscode';
    public function __construct()
    {
        parent::__construct();
    }

    function dataStatus($id){
        return $this->db->select("xstatus")
                        ->where($this->field,$id)
                        ->get($this->table)
                        ->row()
                        ->xstatus;
    }
  function gantiStatus($id){ 
        $status=$this->dataStatus($id);
    
  if($status==0){$status=1;}else if($status==1){$status=0;}

        $data = array('xstatus' => $status);
        return $this->db->where($this->field,$id)
                 ->update($this->table,$data);
  }
  function delAkun($id){ 
        $data = array($this->field.'stdelete' => 0);
        return $this->db->where($this->field,$id)
                 ->update($this->table,$data);
  }

}

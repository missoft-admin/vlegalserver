<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Msettingloading_model extends CI_Model
{
    private $table = 'setting_portloading';
    private $field = 'loading';
    public function __construct()
    {
        parent::__construct();
    }

    function dataStatus($id){
        return $this->db->select("xstatus")
                        ->where("id",$id)
                        ->get($this->table)
                        ->row()
                        ->xstatus;
    }
  function gantiStatus($id){ 
        $status=$this->dataStatus($id);
    
  if($status==0){$status=1;}else if($status==1){$status=0;}

        $data = array('xstatus' => $status);
        return $this->db->where('id'.$this->field,$id)
                 ->update($this->table,$data);
  }
  function delAkun($id){ 
        $data = array('stdelete' => 0);
        return $this->db->where('id',$id)
                 ->update($this->table,$data);
  }

}

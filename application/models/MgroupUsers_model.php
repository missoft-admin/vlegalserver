<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MgroupUsers_model extends CI_Model
{
    private $table = 'users_group';
    private $field = 'ugroup_';
    public function __construct()
    {
        parent::__construct();
    }

    function dataStatus($id){
        return $this->db->query("select `".$this->field."status` from `".$this->table."` where `".$this->field."id` ='".$id."'")
                                ->row()
                                ->ugroup_status;
    }
  function gantiStatus($id){ 
        $status=$this->dataStatus($id);
    
  if($status==0){$status=1;}else if($status==1){$status=0;}

        $data = array($this->field.'status' => $status);
        return $this->db->where($this->field.'id',$id)
                 ->update($this->table,$data);
  }
  function delAkun($id){ 
        $data = array($this->field.'stdelete' => 0);
        return $this->db->where($this->field.'id',$id)
                 ->update($this->table,$data);
  }

}

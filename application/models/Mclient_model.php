<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mclient_model extends CI_Model {

	var $table 	 	= "clients";
    var $field 		= "client_";

	public function __construct()
	{

	}
	
    function dataStatus($id){
        return $this->db->select($this->field."status")
                        ->where($this->field."id",$id)
                        ->get($this->table)
                        ->row()
                        ->client_status;
    }
  function gantiStatus($id){ 
        $status=$this->dataStatus($id);
    
  if($status==0){$status=1;}else if($status==1){$status=0;}

        $data = array($this->field.'status' => $status);
        return $this->db->where($this->field.'id',$id)
                 ->update($this->table,$data);
  }
  function delAkun($id){ 
        $data = array($this->field.'stdelete' => 0);
        return $this->db->where($this->field.'id',$id)
                 ->update($this->table,$data);
  }

	function countItems($keyword='')
	{
		$this->db->select("COUNT(*) as count");
		$this->db->from($this->tableName);
        if(!empty($keyword)){
			$this->db->like('client_nama',$keyword);
		}
		$query = $this->db->get();
		$array = $query->row_array();
		$query->free_result();
		unset($query);
        return $array['count'];
	}
	function getItemById($id)
	{
		$this->db->select("c.*,p.nama as propinsi, k.nama as kabupaten, u.user_name as username");
		$this->db->from($this->tableName ." c ");
		$this->db->join("kit_propinsi p","p.propid=c.client_propinsi","LEFT");
		$this->db->join("kit_kabupaten k","k.kabid=c.client_kabupaten","LEFT");
		$this->db->join("users u","u.user_id=c.client_userid","LEFT");
		$this->db->where($this->fieldPrefix."id",$id);
		$query = $this->db->get();
		$array = $query->row_array();
		$query->free_result();
		unset($query);
        return $array;
	}

	function addNewItem()
	{
        //print_r($_POST); exit();
        // $data[$this->field.'id']                 = decryptURL($_POST['hide-ID']);
        $data= array();
        $data[$this->field.'nick']               = $this->input->post('new-'.$this->field.'nick');
        $data[$this->field.'nama']               = $this->input->post('new-'.$this->field.'nama');
        $data[$this->field.'alamat']             = $this->input->post('new-'.$this->field.'alamat');
        $data[$this->field.'propinsi']           = explode(',',$this->input->post('hide-provinsi'))[0];
        $data[$this->field.'kabupaten']          = explode(',',$this->input->post('hide-kabupaten'))[0];
for($p=1;$p<=5;$p++){
        $data[$this->field.'alamatpabrik'.$p]      = $this->input->post('new-'.$this->field.'alamatpabrik'.$p);
}
        // $data[$this->field.'alamatpabrik2']      = $this->input->post('new-'.$this->field.'alamatpabrik2');
        // $data[$this->field.'alamatpabrik3']      = $this->input->post('new-'.$this->field.'alamatpabrik3');
        // $data[$this->field.'alamatpabrik4']      = $this->input->post('new-'.$this->field.'alamatpabrik4');
        // $data[$this->field.'alamatpabrik5']      = $this->input->post('new-'.$this->field.'alamatpabrik5');
        $data[$this->field.'telp']               = $this->input->post('new-'.$this->field.'telp');
        $data[$this->field.'fax']                = $this->input->post('new-'.$this->field.'fax');
        $data[$this->field.'email']              = $this->input->post('new-'.$this->field.'email');
        $data[$this->field.'website']            = $this->input->post('new-'.$this->field.'website');
        $data[$this->field.'namacp']             = $this->input->post('new-'.$this->field.'namacp');
        $data[$this->field.'jabatancp']          = $this->input->post('new-'.$this->field.'jabatancp');
        $data[$this->field.'telpcp']             = $this->input->post('new-'.$this->field.'telpcp');
        $data[$this->field.'emailcp']            = $this->input->post('new-'.$this->field.'emailcp');
        $data[$this->field.'namadv']             = $this->input->post('new-'.$this->field.'namadv');
        $data[$this->field.'jabatandv']          = $this->input->post('new-'.$this->field.'jabatandv');
        $data[$this->field.'telpdv']             = $this->input->post('new-'.$this->field.'telpdv');
        $data[$this->field.'emaildv']            = $this->input->post('new-'.$this->field.'emaildv');
        $data[$this->field.'aktapendirian']      = $this->input->post('new-'.$this->field.'aktapendirian');
        $data[$this->field.'aktaperubahan']      = $this->input->post('new-'.$this->field.'aktaperubahan');
        $data[$this->field.'siup']               = $this->input->post('new-'.$this->field.'siup');
        $data[$this->field.'siuptgl']            = decode_date($this->input->post('new-'.$this->field.'siuptgl'));
        $data[$this->field.'siuptglkadaluarsa']  = decode_date($this->input->post('new-'.$this->field.'siuptglkadaluarsa'));
        $data[$this->field.'tdp']                = $this->input->post('new-'.$this->field.'tdp');
        $data[$this->field.'tdptgl']             = decode_date($this->input->post('new-'.$this->field.'tdptgl'));
        $data[$this->field.'tdptglkadaluarsa']   = decode_date($this->input->post('new-'.$this->field.'tdptglkadaluarsa'));
        $data[$this->field.'npwp']               = $this->input->post('new-'.$this->field.'npwp');
        $data[$this->field.'npwpkantor']         = $this->input->post('new-'.$this->field.'npwpkantor');
        $data[$this->field.'npwpsppkp']          = $this->input->post('new-'.$this->field.'npwpsppkp');
        $data[$this->field.'npwpskt']            = $this->input->post('new-'.$this->field.'npwpskt');
        $data[$this->field.'etpik']              = $this->input->post('new-'.$this->field.'etpik');
        $data[$this->field.'etpiktgl']           = decode_date($this->input->post('new-'.$this->field.'etpiktgl'));
        $data[$this->field.'etpikproduk']        = $this->input->post('new-'.$this->field.'etpikproduk');
        $data[$this->field.'status']             = $this->input->post('new-status');
        $data[$this->field.'userid']             = explode(',',$this->input->post('hide-userid'))[0];
        $data[$this->field.'sertifikat']         = $this->input->post('new-'.$this->field.'sertifikat');
        $data[$this->field.'aktif']    	         = 1;
        $data[$this->field.'stdelete']           = 1;
//tabel data json
        // print_r($_POST['hide-jsonTable']);exit();
$dataijin_list = json_decode($_POST['hide-jsonTable']);
$n=0;
// $dataf = array();
foreach($dataijin_list as $row):
             $n++;
             $dataijin=array();
             $dataijin[$this->field.'iui_jenis_ijin'.$n]      = strtolower($row[0]);
             $dataijin[$this->field.'primer_jenis_ijin'.$n]   = strtolower($row[1]);
             $dataijin[$this->field.'nomor_ijin'.$n]          = $row[2];
             $dataijin[$this->field.'instansi_ijin'.$n]       = $row[3];
             $dataijin[$this->field.'tglterbit_ijin'.$n]      = decode_date($row[4]);
             $dataijin[$this->field.'jenisproduk_ijin'.$n]    = $row[5];
             // $dataf[]= $dataijin;
endforeach;
        $sField=array_merge($data,$dataijin);
        
        // print_r($sField); exit();
        // $insert_id = saveData($this->table,$data);
	    $this->db->insert($this->table, $sField);
	    return $this->db->insert_id();
	}

	function updateItemById($where)
	{
        //print_r($_POST); exit();
        // $data[$this->field.'id']                 = decryptURL($_POST['hide-ID']);
        $data= array();
        $data[$this->field.'nick']               = $this->input->post('new-'.$this->field.'nick');
        $data[$this->field.'nama']               = $this->input->post('new-'.$this->field.'nama');
        $data[$this->field.'alamat']             = $this->input->post('new-'.$this->field.'alamat');
        $data[$this->field.'propinsi']           = explode(',',$this->input->post('hide-provinsi'))[0];
        $data[$this->field.'kabupaten']          = explode(',',$this->input->post('hide-kabupaten'))[0];
for($p=1;$p<=5;$p++){
        $data[$this->field.'alamatpabrik'.$p]      = $this->input->post('new-'.$this->field.'alamatpabrik'.$p);
}
        // $data[$this->field.'alamatpabrik2']      = $this->input->post('new-'.$this->field.'alamatpabrik2');
        // $data[$this->field.'alamatpabrik3']      = $this->input->post('new-'.$this->field.'alamatpabrik3');
        // $data[$this->field.'alamatpabrik4']      = $this->input->post('new-'.$this->field.'alamatpabrik4');
        // $data[$this->field.'alamatpabrik5']      = $this->input->post('new-'.$this->field.'alamatpabrik5');
        $data[$this->field.'telp']               = $this->input->post('new-'.$this->field.'telp');
        $data[$this->field.'fax']                = $this->input->post('new-'.$this->field.'fax');
        $data[$this->field.'email']              = $this->input->post('new-'.$this->field.'email');
        $data[$this->field.'website']            = $this->input->post('new-'.$this->field.'website');
        $data[$this->field.'namacp']             = $this->input->post('new-'.$this->field.'namacp');
        $data[$this->field.'jabatancp']          = $this->input->post('new-'.$this->field.'jabatancp');
        $data[$this->field.'telpcp']             = $this->input->post('new-'.$this->field.'telpcp');
        $data[$this->field.'emailcp']            = $this->input->post('new-'.$this->field.'emailcp');
        $data[$this->field.'namadv']             = $this->input->post('new-'.$this->field.'namadv');
        $data[$this->field.'jabatandv']          = $this->input->post('new-'.$this->field.'jabatandv');
        $data[$this->field.'telpdv']             = $this->input->post('new-'.$this->field.'telpdv');
        $data[$this->field.'emaildv']            = $this->input->post('new-'.$this->field.'emaildv');
        $data[$this->field.'aktapendirian']      = $this->input->post('new-'.$this->field.'aktapendirian');
        $data[$this->field.'aktaperubahan']      = $this->input->post('new-'.$this->field.'aktaperubahan');
        $data[$this->field.'siup']               = $this->input->post('new-'.$this->field.'siup');
        $data[$this->field.'siuptgl']            = decode_date($this->input->post('new-'.$this->field.'siuptgl'));
        $data[$this->field.'siuptglkadaluarsa']  = decode_date($this->input->post('new-'.$this->field.'siuptglkadaluarsa'));
        $data[$this->field.'tdp']                = $this->input->post('new-'.$this->field.'tdp');
        $data[$this->field.'tdptgl']             = decode_date($this->input->post('new-'.$this->field.'tdptgl'));
        $data[$this->field.'tdptglkadaluarsa']   = decode_date($this->input->post('new-'.$this->field.'tdptglkadaluarsa'));
        $data[$this->field.'npwp']               = $this->input->post('new-'.$this->field.'npwp');
        $data[$this->field.'npwpkantor']         = $this->input->post('new-'.$this->field.'npwpkantor');
        $data[$this->field.'npwpsppkp']          = $this->input->post('new-'.$this->field.'npwpsppkp');
        $data[$this->field.'npwpskt']            = $this->input->post('new-'.$this->field.'npwpskt');
        $data[$this->field.'etpik']              = $this->input->post('new-'.$this->field.'etpik');
        $data[$this->field.'etpiktgl']           = decode_date($this->input->post('new-'.$this->field.'etpiktgl'));
        $data[$this->field.'etpikproduk']        = $this->input->post('new-'.$this->field.'etpikproduk');
        $data[$this->field.'status']             = $this->input->post('new-status');
        $data[$this->field.'userid']             = explode(',',$this->input->post('hide-userid'))[0];
        $data[$this->field.'sertifikat']         = $this->input->post('new-'.$this->field.'sertifikat');
        $data[$this->field.'aktif']    	         = 1;
        $data[$this->field.'stdelete']           = 1;
//tabel data json
        // print_r($_POST['hide-jsonTable']);exit();
$dataijin_list = json_decode($_POST['hide-jsonTable']);
$n=0;
$dataf = array();
foreach($dataijin_list as $row):
             $n++;
             $dataijin=array();
             $dataijin[$this->field.'iui_jenis_ijin'.$n]      = strtolower($row[0]);
             $dataijin[$this->field.'primer_jenis_ijin'.$n]   = strtolower($row[1]);
             $dataijin[$this->field.'nomor_ijin'.$n]          = $row[2];
             $dataijin[$this->field.'instansi_ijin'.$n]       = $row[3];
             $dataijin[$this->field.'tglterbit_ijin'.$n]      = decode_date($row[4]);
             $dataijin[$this->field.'jenisproduk_ijin'.$n]    = $row[5];
             // $dataf[]= $dataijin;
endforeach;
        $sField=array_merge($data,$dataijin);
        
        // print_r($sField); exit();
        $this->db->update($this->table, $sField, $where);
	}

	function deleteItemById($id)
	{
		$this->db->where($this->fieldPrefix . 'id', $id);
		$this->db->delete($this->tableName);
		return true;
	}
	
	function updateActiveById($id){
		$data = array(
			$this->fieldPrefix . 'status' => 'Active'
		);
		$this->db->update($this->tableName, $data, array($this->fieldPrefix . 'id' => $id));
	}
	
	function getBuyer($q='',$id=''){
		$this->db->select("idbuyer as id,concat(buyer,' [', idnegara ,']') as name",FALSE);
		$this->db->from("kit_buyer");
		$this->db->where("xstatus","1");
		if(!empty($q)){
			$this->db->like("buyer",$q);
		}
		if(!empty($id)){
			$this->db->where("idbuyer IN (".$id.")");
		}
		$this->db->order_by("buyer","ASC");
		$query = $this->db->get();
		$array = $query->result_array();
		$query->free_result();
		unset($query);
        return $array;
	}
	
	function getSupplier($q='',$id=''){
		$this->db->select("idsupplier as id,concat(supplier,' [', idnegara ,']') as name",FALSE);
		$this->db->from("kit_supplier");
		$this->db->where("xstatus","1");
		if(!empty($q)){
			$this->db->like("supplier",$q);
		}
		if(!empty($id)){
			$this->db->where("idsupplier IN (".$id.")");
		}
		$this->db->order_by("supplier","ASC");
		$query = $this->db->get();
		$array = $query->result_array();
		$query->free_result();
		unset($query);
        return $array;
	}
	
	function getSortimen($q='',$id=''){
		$this->db->select("idsortimen as id,sortimen as name");
		$this->db->from("kit_sortimen");
		$this->db->where("xstatus","1");
		if(!empty($q)){
			$this->db->like("sortimen",$q);
		}
		if(!empty($id)){
			$this->db->where("idsortimen IN (".$id.")");
		}
		$this->db->order_by("sortimen","ASC");
		$query = $this->db->get();
		$array = $query->result_array();
		$query->free_result();
		unset($query);
        return $array;
	}

	function getWip($q='',$id=''){
		$this->db->select("idwip as id,wip as name");
		$this->db->from("kit_wip");
		$this->db->where("xstatus","1");
		if(!empty($q)){
			$this->db->like("wip",$q);
		}
		if(!empty($id)){
			$this->db->where("idwip IN (".$id.")");
		}
		$this->db->order_by("wip","ASC");
		$query = $this->db->get();
		$array = $query->result_array();
		$query->free_result();
		unset($query);
        return $array;
	}
	
	function getProduk($q='',$id=''){
		$this->db->select("idproduk as id,concat(produk,' [', kodehs ,']') as name",FALSE);
		$this->db->from("kit_produk");
		$this->db->where("xstatus","1");
		if(!empty($q)){
			$this->db->like("produk",$q);
		}
		if(!empty($id)){
			$this->db->where("idproduk IN (".$id.")");
		}
		$this->db->order_by("produk","ASC");
		$query = $this->db->get();
		$array = $query->result_array();
		$query->free_result();
		unset($query);
        return $array;
	}
	
	function getSettingById($id)
	{
		$this->db->select("*");
		$this->db->from("kit_setting");
		$this->db->where("idclient",$id);
		$this->db->order_by("idx","DESC");
		$query = $this->db->get();
		$array = $query->result_array();
		$query->free_result();
		unset($query);
        return $array;
	}
	
	function deleteSettingById($id)
	{
		$this->db->where('idclient', $id);
		$this->db->delete("kit_setting");
		return true;
	}
	
	function addKitSetting($rs)
	{
		$data = array(
			'idclient'	=> $rs['idclient']
		);
	    $this->db->insert("kit_setting", $data);
	    return $this->db->insert_id();
	}
	
	function updateKitSetting($rs,$table,$id){
		$data = array(
			'id'.$table => $rs['id'.$table]
		);
		#$this->db->update("kit_setting", $data, array('idclient' => $id,'id'.$table =>  and idbuyer is null limit 1));
		
		$this->db->where("idclient", $id);
		$this->db->where("id".$table." IS NULL");
		$this->db->limit(1);
		$this->db->update("kit_setting", $data);
	}
  function delRow($tbl,$where=array(),$field){ 
        $data = array($field => '');
        return $this->db->where($where)
                 ->update('kit_setting',$data);
  }
}

?>

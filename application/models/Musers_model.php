<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Musers_model extends CI_Model
{
    private $table = 'users';
    // private $tblAlamat = 'manggotaalamattujuan';
    public function __construct()
    {
        parent::__construct();
    }

    public function checkOldPass($old_password)
    {
        $id = $_SESSION['user_id'];
                $this->db->where('username', $_SESSION['username']);
                $this->db->where('idanggota', $id);
        $this->db->where('passkey', $old_password);
        $query = $this->db->get('manggota');
        if($query->num_rows() > 0)
            return 1;
        else
            return 0;
    }

    public function saveNewPass($new_pass)
    {
        $data = array(
               'passkey' => $new_pass
            );
        $this->db->where('idanggota', $_SESSION['user_id']);
        $this->db->update('manggota', $data);
        return true;
    }

    // function dataDelete($id){
    //     return $this->db->query("select `status` from `".$this->table."` where `user_id` ='".$id."'")->row()->status;
    // }
  function delAkun($id){ 
        // $status=$this->dataStatus($id);
    
  // if($status==0){$status=1;}else if($status==1){$status=0;}

        $data = array('active_status' => 0);
        return $this->db->where('user_id',$id)
                 ->update($this->table,$data);
  }

    function dataStatus($id){
        return $this->db->query("select `status` from `".$this->table."` where `user_id` ='".$id."'")->row()->status;
    }
  function gantiStatus($id){ 
        $status=$this->dataStatus($id);
    
  if($status==0){$status=1;}else if($status==1){$status=0;}

        $data = array('status' => $status);
        return $this->db->where('user_id',$id)
                 ->update($this->table,$data);
  }

}

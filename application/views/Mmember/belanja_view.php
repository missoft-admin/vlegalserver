<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>
				<a href="{base_url}mmember" class="btn"><i class="fa fa-reply"></i></a>
			</li>
           
		</ul>
		<?php $periode=get_month_name($bulan)." 20".$tahun;?>
		<h3 class="block-title">{title} <?=$periode?></h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mmember/belanja/'.$noid,'class="form-horizontal push-10-t"') ?>
				<div class="form-group">
					<label class="col-md-2 control-label" for="nama">Noid</label>
					<div class="col-md-3">
						<input  type="text" readonly class="form-control input-sm" name="noid" id="noid" placeholder="Noid" value="{noid}" />
					</div>
					<div class="col-md-7">
						<input  type="text" readonly class="form-control input-sm" name="namamembers" id="namamembers" placeholder="Nama Point Distribusi" value="{namamembers}" />
					</div>
				</div>
				<div class="form-group"> 
					<label class="col-md-2 control-label">Periode :</label>
					<div class="col-md-3">
						<select class="form-control input-sm" name="bulan" id="bulan" style="width : 100%">
							<?php  echo opt_month($bulan); ?>
						</select>
					</div>
					
					<div class="col-md-3">
						<select class="form-control input-sm" name="tahun" id="tahun" style="width : 100%">
							<?php for ($th=date('y');$th >= 10;$th--){ ?>
							<option value=<?php echo $th; ?> <?php if ($tahun==$th){echo 'selected="selected"';}?> class="ayrsingle"><?php echo '20'.$th; ?></option>
							<?php } ?>
						</select>	
					</div>
							
				</div>
				
				
				<div class="form-group">
					<label class="col-md-2 control-label"></label>
					<div class="col-md-10">
						<button class="btn btn-success" type="submit">OK</button>
					</div>
				</div>
				<?php echo form_hidden('noid', $noid); ?>
				<?php echo form_close() ?>
		</div>

	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<?= ($this->agent->is_mobile())? '<div class="table-responsive">' : '' ?>
		
			<table width="100%" class="table table-bordered table-striped table-responsive" id="datatable_index">
				<thead>
					<tr>                                    
						<th width="5%">No</th>
						<th width="15%">No Penjualan</th>
						<th width="15%">Tanggal</th>
						<th width="20">Nama Barang</th>					
						<th width="10%">Jumlah</th>					
						<th width="10%">Harga</th>					
						<th width="15%">Total</th>					
						<th width="5%">Tools</th>					
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		<?= ($this->agent->is_mobile())? '</div>' : '' ?>
	</div>
</div>
<input type="hidden" id="datatable_search" value="{datatable_search}"/>
<input type="hidden" id="datatable_page" value="{datatable_page}"/>
<input type="hidden" id="datatable_order" value="{datatable_order}"/>



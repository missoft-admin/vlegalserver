
<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>
				<a href="{base_url}mmember" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title"><i class="si si-users"></i> {title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mmember/viewkomisiother','class="form-horizontal push-10-t"') ?>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Noid</label>
				<div class="col-md-3">
					<input  type="text" readonly class="form-control input-sm" name="noid" id="noid" placeholder="Noid" value="{noid}" />
				</div>
				<div class="col-md-7">
					<input  type="text" readonly class="form-control input-sm" name="namamembers" id="namamembers" placeholder="Nama Point Distribusi" value="{namamembers}" />
				</div>
			</div>
			<div class="form-group"> 
				<label class="col-md-2 control-label">Periode :</label>
				<div class="col-md-3">
					<select class="form-control input-sm" name="bulan" id="bulan" style="width : 100%">
						<?php  echo opt_month($bulan); ?>
					</select>
				</div>
				
				<div class="col-md-3">
					<select class="form-control input-sm" name="tahun" id="tahun" style="width : 100%">
						<?php for ($th=date('y');$th >= 10;$th--){ ?>
						<option value=<?php echo $th; ?> <?php if ($tahun==$th){echo 'selected="selected"';}?> class="ayrsingle"><?php echo '20'.$th; ?></option>
						<?php } ?>
					</select>	
				</div>
						
			</div>
			
			
			<div class="form-group">
				<label class="col-md-2 control-label"></label>
				<div class="col-md-10">
					<button class="btn btn-success" type="submit">OK</button>
				</div>
			</div>
			<?php echo form_hidden('noid', $noid); ?>
			<?php echo form_close() ?>
	</div>
</div>

<div class="row">
    <div class="col-sm-6 col-lg-6">
		<div class="block block-themed">
			<div class="block-header bg-smooth-dark">
				<ul class="block-options">
					
				</ul>
				<h3 class="block-title">Info Belanja</h3>
			</div>
			
			<div class="block-content">
				<?php if ($passup):?>
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						Perhitungan Akhir Royalti Berdasarkan  <strong>PassUp</strong>
					</div>
					
				<?php endif;?>	
				<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
				<div class="table-responsive" width="100%">
					<table width="100%" class="table table-bordered table-striped table-responsive" id="datatable_index">
						<tr>
							<td><span class="icon location"></span> Jenis </td>
							<td><span style="font-weight: bold ! important; font-size: 16px ! important; margin-left: 5px;">Normal</span></td>
							<td><span style="font-weight: bold ! important; font-size: 16px ! important; margin-left: 5px;">Pass-Up</span></td>
						</tr>
						<tr>
							<td><span class="icon cart"></span> Pribadi </td>
							<td><a href="#"><span style="font-weight: bold ! important; font-size: 16px ! important; margin-left: 5px;"><?php echo number_format($personal,0,",","."); ?></span></a></td>
							<td><a href="#"><span style="font-weight: bold ! important; font-size: 16px ! important; margin-left: 5px;"><?php echo number_format($personal,0,",","."); ?></span></a></td>
						</tr>
						<tr>
							<td><span class="icon location2"></span> Jalur <a href="#"><span style="font-weight: bold ! important; font-size: 16px ! important; margin-left: 5px;"> 1</span></a></td>
							<td><a href="#"><span style="font-weight: bold ! important; font-size: 16px ! important; margin-left: 5px;"><?php echo number_format($salejalur1,0,",","."); ?></span></a></td>
							<td><a href="#"><span style="font-weight: bold ! important; font-size: 16px ! important; margin-left: 5px;"><?php if ($salejalur1_pu){echo number_format($salejalur1_pu,0,",",".");}else{echo "Belum";} ?></span></a></td>
						</tr>
						<tr>
							<td><span class="icon location2"></span> Jalur <a href="#"><span style="font-weight: bold ! important; font-size: 16px ! important; margin-left: 5px;"> 2</span></a></td>
							<td><a href="#"><span style="font-weight: bold ! important; font-size: 16px ! important; margin-left: 5px;"><?php echo number_format($salejalur2,0,",","."); ?></span></a></td>
							<td><a href="#"><span style="font-weight: bold ! important; font-size: 16px ! important; margin-left: 5px;"><?php if ($salejalur2_pu){echo number_format($salejalur2_pu,0,",",".");}else{echo "Belum";} ?></span></a></td>
						</tr>
						<tr>
							<td><span class="icon location2"></span> Jalur <a href="#"><span style="font-weight: bold ! important; font-size: 16px ! important; margin-left: 5px;"> 3</span></a></td>
							<td><a href="#"><span style="font-weight: bold ! important; font-size: 16px ! important; margin-left: 5px;"><?php echo number_format($salejalur3,0,",","."); ?></span></a></td>
							<td><a href="#"><span style="font-weight: bold ! important; font-size: 16px ! important; margin-left: 5px;"><?php if ($salejalur3_pu){echo number_format($salejalur3_pu,0,",",".");}else{echo "Belum";} ?></span></a></td>
						</tr>
						<tr>
							<td><span class="icon location2"></span> Jalur <a href="#"><span style="font-weight: bold ! important; font-size: 16px ! important; margin-left: 5px;"> 4</span></a></td>
							<td><a href="#"><span style="font-weight: bold ! important; font-size: 16px ! important; margin-left: 5px;"><?php echo number_format($salejalur4,0,",","."); ?></span></a></td>
							<td><a href="#"><span style="font-weight: bold ! important; font-size: 16px ! important; margin-left: 5px;"><?php if ($salejalur4_pu){echo number_format($salejalur4_pu,0,",",".");}else{echo "Belum";} ?></span></a></td>
						</tr>
						<tr>
							<td><span class="icon location2"></span> Jalur <a href="#"><span style="font-weight: bold ! important; font-size: 16px ! important; margin-left: 5px;"> 5</span></a></td>
							<td><a href="#"><span style="font-weight: bold ! important; font-size: 16px ! important; margin-left: 5px;"><?php echo number_format($salejalur5,0,",","."); ?></span></a></td>
							<td><a href="#"><span style="font-weight: bold ! important; font-size: 16px ! important; margin-left: 5px;"><?php if ($salejalur5_pu){echo number_format($salejalur5_pu,0,",",".");}else{echo "Belum";} ?></span></a></td>
						</tr>
						<tr>
							<td><span class="icon books"></span> &#931;Og</td>
							<td><a href="#"><span style="font-weight: bold ! important; font-size: 16px ! important; margin-left: 5px;"><?php echo number_format($omzetg,0,",","."); ?></span></a></td>
							<td><a href="#"><span style="font-weight: bold ! important; font-size: 16px ! important; margin-left: 5px;"><?php if ($omzetg_pu){echo number_format($omzetg_pu,0,",",".");}else{echo "Belum";} ?></span></a></td>
						</tr>	
						<tr>
							<td><span class="icon books"></span> &#931;Op</td>
							<td><a href="#"><span style="font-weight: bold ! important; font-size: 16px ! important; margin-left: 5px;"><?php echo number_format($omzeta,0,",","."); ?></span></a></td>
							<td><a href="#"><span style="font-weight: bold ! important; font-size: 16px ! important; margin-left: 5px;"><?php if ($omzeta_pu){echo number_format($omzeta_pu,0,",",".");}else{echo "Belum";} ?></span></a></td>
						</tr>
						<tr>
							<td><span class="icon paypal2"></span> &#931;On</td>
							<td><a href="#"><span style="font-weight: bold ! important; font-size: 16px ! important; margin-left: 5px;"><?php echo number_format($omzetp,0,",","."); ?></span></a></td>
							<td><a href="#"><span style="font-weight: bold ! important; font-size: 16px ! important; margin-left: 5px;"><?php echo number_format($omzetp,0,",","."); ?></span></a></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-6 col-lg-6">
		<div class="block block-themed">
			<div class="block-header bg-smooth-dark">
				<ul class="block-options">
					
				</ul>
				<h3 class="block-title">Royalti {noid} {namamembers}</h3>
			</div>
			<div class="block-content">
				<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
				<div class="table-responsive" width="100%">
					<?php if (isset($msg3)):?>
						
						<div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<strong>Bonus Royalti : <?php echo $msg3;?></strong>
						</div>
					<?php else:?>
						
						<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							Syarat Bonus Royalti <strong>belum terpenuhi.</strong>
						</div>
					<?php endif;?>	
					
					<p style="margin-bottom: 0px;">Syarat <strong>Bonus Royalti</strong> :</p>
					<ul style="margin-top: 0px; margin-bottom: 5px;">
						<li style="margin-bottom: 5px;"><strong>Belanja Pribadi</strong> minimal <strong>30</strong> bungkus.</li>
						<li style="margin-bottom: 5px;"><strong>Total Belanja Group</strong> minimal <strong>10.000</strong> bungkus.</li>
						<li style="margin-bottom: 5px;"><strong>5 Jalur Seimbang</strong> masing-masing <strong>1.000</strong> bungkus.</li>
					</ul>
					<p style="margin-bottom: 0px;">Formula <strong>Bonus Royalti</strong> :</p>
					<ul style="margin-top: 0px; margin-bottom: 10px;">
						<li style="margin-bottom: 10px;"><strong>( Rp <?php echo $pengali;?>,- x <span class="greenket">&#931;On</span> ) x ( <span class="greenket">&#931;Og</span> / <span class="greenket">&#931;Op</span> )</strong>.</li>
					</ul>
					<p style="margin-bottom: 0px;">Keterangan <strong>Formula</strong> :</p>
					<ul style="margin-top: 0px;margin-bottom: 5px;">
						<li><strong><span class="greenket">&#931;On</span></strong> = <strong>Total Omzet Nasional</strong>.</li>
						<li><strong><span class="greenket">&#931;Og</span></strong> = <strong>Total Omzet Group</strong>.</li>
						<li><strong><span class="greenket">&#931;Op</span></strong> = <strong>Total Omzet Group Peraih Bonus Royalti</strong>.</li>										
					</ul>
					<p style="margin-bottom: 0px;">Penerapan <strong>Formula Bonus Royalti (NonPassUp)</strong> :</p>
					<ul style="margin-top: 0px;margin-bottom: 0px;"">
						<li style="margin-bottom: 0px;"><strong>( Rp <?php echo $pengali;?>,- x <?php echo number_format($omzetp,0,",",".");?> ) x ( <?php echo number_format($omzetg,0,",",".");?> / <?php echo number_format($omzeta,0,",",".");?> )</strong> =<br><strong><?php echo $hasilroy;?></strong>.</li>
					</ul>
					<?php if ($passup):?>
					<p style="margin-bottom: 0px;">Penerapan <strong>Formula Bonus Royalti (PassUp)</strong> :</p>
					<ul style="margin-top: 0px;margin-bottom: 0px;"">
						<li style="margin-bottom: 0px;"><strong>( Rp <?php echo $pengali;?>,- x <?php echo number_format($omzetp,0,",",".");?> ) x ( <?php echo number_format($omzetg_pu,0,",",".");?> / <?php echo number_format($omzeta_pu,0,",",".");?> )</strong> =<br><strong><?php echo $hasilroy_pu;?></strong>.</li>
					</ul>
					<?php endif;?>
				</div>
			</div>
		</div>
	</div>
</div>




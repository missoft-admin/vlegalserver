
<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>
				<a href="{base_url}mmember" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title">Detail Info Jaringan : {noid}</h3>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive" width="100%">
			<table width="100%" class="table table-bordered table-striped table-responsive" id="datatable_index">
				<thead>
					<tr>                                    
						<th  align="center">Downline</th>
						<th  align="center">Jumlah</th>
						<th  align="center">Keterangan</th>
						<th  align="center">Recruitment(%)</th>
						<th  align="center">Nilai</th>					
					</tr>
				</thead>
				<tbody>
					<tr class="gradeA">
						<td>Level 1</td>
						<td  align="center"><?php echo number_format($jmllevel1,0,",","."); ?></td>
						<td  align="center"> <?php if ($mlevel1){echo "Kurang <strong><font color='red'>".$mlevel1."</font></strong> mitra";}else{echo " Matrix ";} ?> </td>
						<td  align="center"> <?php echo $ket1;?> </td>
						<td  align="center"> <?php echo $nilai1;?> </td>
					</tr>
					<tr class="gradeA">
						<td>Level 2</td>
						<td  align="center"><?php echo number_format($jmllevel2,0,",","."); ?></td>
						<td  align="center"> <?php if ($mlevel2){echo "Kurang <strong><font color='red'>".$mlevel2."</font></strong> mitra";}else{echo " Matrix ";} ?> </td>
						<td  align="center"> <?php echo $ket2;?> </td>
						<td  align="center"> <?php echo $nilai2;?> </td>
					</tr>
					<tr class="gradeA">
						<td>Level 3</td>
						<td  align="center"><?php echo number_format($jmllevel3,0,",","."); ?></td>
						<td  align="center"> <?php if ($mlevel3){echo "Kurang <strong><font color='red'>".$mlevel3."</font></strong> mitra";}else{echo " Matrix ";} ?> </td>
						<td  align="center"> <?php echo $ket3;?> </td>
						<td  align="center"> <?php echo $nilai3;?> </td>
					</tr>
					<tr class="gradeA">
						<td>Level 4</td>
						<td  align="center"><?php echo number_format($jmllevel4,0,",","."); ?></td>
						<td  align="center"> <?php if ($mlevel4){echo "Kurang <strong><font color='red'>".$mlevel4."</font></strong> mitra";}else{echo " Matrix ";} ?> </td>
						<td  align="center"> <?php echo $ket4;?> </td>
						<td  align="center"> <?php echo $nilai4;?> </td>
					</tr>
					<tr class="gradeA">
						<td>Level 5</td>
						<td  align="center"><?php echo number_format($jmllevel5,0,",","."); ?></td>
						<td  align="center"> <?php if ($mlevel5){echo "Kurang <strong><font color='red'>".number_format($mlevel5,0,",",".")."</font></strong> mitra";}else{echo " Matrix ";} ?> </td>
						<td  align="center"> <?php echo $ket5;?> </td>
						<td  align="center"> <?php echo $nilai4;?> </td>
					</tr>
					<tr class="gradeA">
						<td>Level 6</td>
						<td  align="center"><?php echo number_format($jmllevel6,0,",","."); ?></td>
						<td  align="center"> <?php if ($mlevel6){echo "Kurang <strong><font color='red'>".number_format($mlevel6,0,",",".")."</font></strong> mitra";}else{echo " Matrix ";} ?> </td>
						<td  align="center"> <?php echo $ket6;?> </td>
						<td  align="center"> <?php echo $nilai6;?> </td>
					</tr>
					<tr class="gradeA">
						<td>Level 7</td>
						<td  align="center"><?php echo number_format($jmllevel7,0,",","."); ?></td>
						<td  align="center"> <?php if ($mlevel7){echo "Kurang <strong><font color='red'>".number_format($mlevel7,0,",",".")."</font></strong> mitra";}else{echo " Matrix ";} ?> </td>
						<td  align="center"> <?php echo $ket7;?> </td>
						<td  align="center"> <?php echo $nilai7;?> </td>
					</tr>
					<tr class="gradeA">
						<td>Level 8</td>
						<td  align="center"><?php echo number_format($jmllevel8,0,",","."); ?></td>
						<td  align="center"> <?php if ($mlevel8){echo "Kurang <strong><font color='red'>".number_format($mlevel8,0,",",".")."</font></strong> mitra";}else{echo " Matrix ";} ?> </td>
						<td  align="center"> <?php echo $ket8;?> </td>
						<td  align="center"> <?php echo $nilai8;?> </td>
					</tr>
					<tr class="gradeA">
						<td>Level 9</td>
						<td  align="center"><?php echo number_format($jmllevel9,0,",","."); ?></td>
						<td  align="center"> <?php if ($mlevel9){echo "Kurang <strong><font color='red'>".number_format($mlevel9,0,",",".")."</font></strong> mitra";}else{echo " Matrix ";} ?> </td>
						<td  align="center"> <?php echo $ket9;?> </td>
						<td  align="center"> <?php echo $nilai9;?> </td>
					</tr>
					<tr class="gradeA">
						<td>Level 10</td>
						<td  align="center"><?php echo number_format($jmllevel10,0,",","."); ?></td>
						<td  align="center"> <?php if ($mlevel10){echo "Kurang <strong><font color='red'>".number_format($mlevel10,0,",",".")."</font></strong> mitra";}else{echo " Matrix ";} ?> </td>
						<td  align="center"> <?php echo $ket10;?> </td>
						<td  align="center"> <?php echo $nilai10;?> </td>
					</tr>
					<tr class="gradeA">
						<td style="padding: 2px;"></td><td style="padding: 2px;"></td><td style="padding: 2px;"></td><td style="padding: 2px;"></td><td style="padding: 2px;"></td>
					</tr>	
					<tr class="gradeA">
						<td><strong>Total </strong></td>
						<td  align="center"><strong><?php echo number_format($jmldownline,0,",","."); ?></strong></td>
						<td  align="center"> <?php if ($mtotal){echo "Kurang <strong><font color='red'>".number_format($mtotal,0,",",".")."</font></strong> mitra";}else{echo " Matrix ";} ?> </td>
						<td  align="center"><strong> <?php echo $kettot;?> </strong></td>
						<td  align="center"> <?php echo $nilaitot;?> </td>
					</tr>	
				</tbody>
			</table>
		</div>
	</div>
</div>
<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			
		</ul>
		<h3 class="block-title">Keterangan Nilai</h3>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive" width="100%">
			<table width="100%" class="table table-bordered table-striped table-responsive" id="datatable_index">
				<thead>
					<tr>                                    
						<th  align="center">Nilai</th>
						<th  align="center"></th>
						<th  align="center">Keterangan</th>					
					</tr>
				</thead>
				<tbody>
					<tr class="gradeA">
					<td class="center"><?php echo $nilainol;?></td>
					<td class="center">=</td>
					<td>&#9679; Belum Ada Recruitment pada level tersebut</td>
				</tr>
				<tr class="gradeA">
					<td class="center"><?php echo $nilaijem;?></td>
					<td class="center">=</td>
					<td>&#9679; Nilai LEVEL: Recruitment diatas <span class="greenket">0%</span> s/d <span class="greenket">2%</span> dari jumlah maksimal pada level tsb.
					<br  style="margin-bottom: 10px;">&#9679; Nilai TOTAL: Total Downline mulai <span class="greenket">1</span> s/d <span class="greenket">5</span> downline.</td>
				</tr>
				<tr class="gradeA">
					<td class="center"><?php echo str_repeat($nilaijem,2);?></td>
					<td class="center">=</td>
					<td>&#9679; Nilai LEVEL: Recruitment diatas <span class="greenket">2%</span> s/d <span class="greenket">5%</span> dari jumlah maksimal pada level tsb.
					<br  style="margin-bottom: 10px;">&#9679; Nilai TOTAL: Total Downline mulai <span class="greenket">6</span> s/d <span class="greenket">30</span> downline.</td>
				</tr>
				<tr class="gradeA">
					<td class="center"><?php echo str_repeat($nilaijem,3);?></td>
					<td class="center">=</td>
					<td>&#9679; Nilai LEVEL: Recruitment diatas <span class="greenket">5%</span> s/d <span class="greenket">8%</span> dari jumlah maksimal pada level tsb.
					<br  style="margin-bottom: 10px;">&#9679; Nilai TOTAL: Total Downline mulai <span class="greenket">31</span> s/d <span class="greenket">70</span> downline.</td>
				</tr>                                                                                                
				<tr class="gradeA">
					<td class="center"><?php echo str_repeat($nilaijem,4);?></td>
					<td class="center">=</td>
					<td>&#9679; Nilai LEVEL: Recruitment diatas <span class="greenket">8%</span> s/d <span class="greenket">12%</span> dari jumlah maksimal pada level tsb.
					<br  style="margin-bottom: 10px;">&#9679; Nilai TOTAL: Total Downline mulai <span class="greenket">71</span> s/d <span class="greenket">134</span> downline.</td>
				</tr>                                                                                                
				<tr class="gradeA">
					<td class="center"><?php echo str_repeat($nilaijem,5);?></td>
					<td class="center">=</td>
					<td>&#9679; Nilai LEVEL: Recruitment diatas <span class="greenket">12%</span> s/d <span class="greenket">17%</span> dari jumlah maksimal pada level tsb.
					<br  style="margin-bottom: 10px;">&#9679; Nilai TOTAL: Total Downline mulai <span class="greenket">135</span> s/d <span class="greenket">236</span> downline.</td>
				</tr>                                                                                                
				<tr class="gradeA">
					<td class="center"><?php echo $nilaibin;?></td>
					<td class="center">=</td>
					<td>&#9679; Nilai LEVEL: Recruitment diatas <span class="greenket">17%</span> s/d <span class="greenket">23%</span> dari jumlah maksimal pada level tsb.
					<br  style="margin-bottom: 10px;">&#9679; Nilai TOTAL: Total Downline mulai <span class="greenket">237</span> s/d <span class="greenket">402</span> downline.</td>
				</tr>                                                                                                
				<tr class="gradeA">
					<td class="center"><?php echo str_repeat($nilaibin,2);?></td>
					<td class="center">=</td>
					<td>&#9679; Nilai LEVEL: Recruitment diatas <span class="greenket">23%</span> s/d <span class="greenket">30%</span> dari jumlah maksimal pada level tsb.
					<br  style="margin-bottom: 10px;">&#9679; Nilai TOTAL: Total Downline mulai <span class="greenket">403</span> s/d <span class="greenket">669</span> downline.</td>
				</tr>                                                                                                
				<tr class="gradeA">
					<td class="center"><?php echo str_repeat($nilaibin,3);?></td>
					<td class="center">=</td>
					<td>&#9679; Nilai LEVEL: Recruitment diatas <span class="greenket">30%</span> s/d <span class="greenket">37%</span> dari jumlah maksimal pada level tsb.
					<br  style="margin-bottom: 10px;">&#9679; Nilai TOTAL: Total Downline mulai <span class="greenket">670</span> s/d <span class="greenket">1.100</span> downline.</td>
				</tr>                                                                                                
				<tr class="gradeA">
					<td class="center"><?php echo str_repeat($nilaibin,4);?></td>
					<td class="center">=</td>
					<td>&#9679; Nilai LEVEL: Recruitment diatas <span class="greenket">37%</span> s/d <span class="greenket">45%</span> dari jumlah maksimal pada level tsb.
					<br  style="margin-bottom: 10px;">&#9679; Nilai TOTAL: Total Downline mulai <span class="greenket">1.101</span> s/d <span class="greenket">1.797</span> downline.</td>
				</tr>                                                                                                
				<tr class="gradeA">
					<td class="center"><?php echo str_repeat($nilaibin,5);?></td>
					<td class="center">=</td>
					<td>&#9679; Nilai LEVEL: Recruitment diatas <span class="greenket">45%</span> s/d <span class="greenket">54%</span> dari jumlah maksimal pada level tsb.
					<br  style="margin-bottom: 10px;">&#9679; Nilai TOTAL: Total Downline mulai <span class="greenket">1.798</span> s/d <span class="greenket">2.924</span> downline.</td>
				</tr>                                                                                                
				<tr class="gradeA">
					<td class="center"><?php echo $nilaigol;?></td>
					<td class="center">=</td>
					<td>&#9679; Nilai LEVEL: Recruitment diatas <span class="greenket">54%</span> s/d <span class="greenket">64%</span> dari jumlah maksimal pada level tsb.
					<br  style="margin-bottom: 10px;">&#9679; Nilai TOTAL: Total Downline mulai <span class="greenket">2.925</span> s/d <span class="greenket">4.747</span> downline.</td>
				</tr>                                                                                                
				<tr class="gradeA">
					<td class="center"><?php echo str_repeat($nilaigol,2);?></td>
					<td class="center">=</td>
					<td>&#9679; Nilai LEVEL: Recruitment diatas <span class="greenket">64%</span> s/d <span class="greenket">75%</span> dari jumlah maksimal pada level tsb.
					<br  style="margin-bottom: 10px;">&#9679; Nilai TOTAL: Total Downline mulai <span class="greenket">4.748</span> s/d <span class="greenket">7.696</span> downline.</td>
				</tr>                                                                                                
				<tr class="gradeA">
					<td class="center"><?php echo str_repeat($nilaigol,3);?></td>
					<td class="center">=</td>
					<td>&#9679; Nilai LEVEL: Recruitment diatas <span class="greenket">75%</span> s/d <span class="greenket">87%</span> dari jumlah maksimal pada level tsb.
					<br  style="margin-bottom: 10px;">&#9679; Nilai TOTAL: Total Downline mulai <span class="greenket">7.697</span> s/d <span class="greenket">12.465</span> downline.</td>
				</tr>                                                                                                
				<tr class="gradeA">
					<td class="center"><?php echo str_repeat($nilaigol,4);?></td>
					<td class="center">=</td>
					<td>&#9679; Nilai LEVEL: Recruitment diatas <span class="greenket">87%</span> s/d <span class="greenket">99%</span> dari jumlah maksimal pada level tsb.
					<br  style="margin-bottom: 10px;">&#9679; Nilai TOTAL: Total Downline mulai <span class="greenket">12.466</span> s/d <span class="greenket">20.181</span> downline.</td>
				</tr>                                                                                                
				<tr class="gradeA">
					<td class="center"><?php echo str_repeat($nilaigol,5);?></td>
					<td class="center">=</td>
					<td>&#9679; Nilai LEVEL: Recruitment diatas <span class="greenket">99%</span> s/d <span class="greenket">100%</span> dari jumlah maksimal pada level tsb.
					<br  style="margin-bottom: 10px;">&#9679; Nilai TOTAL: Total Downline mulai <span class="greenket">20.182</span> downline.</td>
				</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>




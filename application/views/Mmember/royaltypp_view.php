
<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>
				<a href="{base_url}mmember" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title"><i class="si si-users"></i> {title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mmember/viewkomisi','class="form-horizontal push-10-t"') ?>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Noid</label>
				<div class="col-md-3">
					<input  type="text" readonly class="form-control input-sm" name="noid" id="noid" placeholder="Noid" value="{noid}" />
				</div>
				<div class="col-md-7">
					<input  type="text" readonly class="form-control input-sm" name="namamembers" id="namamembers" placeholder="Nama Point Distribusi" value="{namamembers}" />
				</div>
			</div>
			<div class="form-group"> 
				<label class="col-md-2 control-label">Periode :</label>
				<div class="col-md-3">
					<select class="form-control input-sm" name="bulan" id="bulan" style="width : 100%">
						<?php  echo opt_month($bulan); ?>
					</select>
				</div>
				
				<div class="col-md-3">
					<select class="form-control input-sm" name="tahun" id="tahun" style="width : 100%">
						<?php for ($th=date('y');$th >= 10;$th--){ ?>
						<option value=<?php echo $th; ?> <?php if ($tahun==$th){echo 'selected="selected"';}?> class="ayrsingle"><?php echo '20'.$th; ?></option>
						<?php } ?>
					</select>	
				</div>
						
			</div>
			
			
			<div class="form-group">
				<label class="col-md-2 control-label"></label>
				<div class="col-md-10">
					<button class="btn btn-success" type="submit">OK</button>
				</div>
			</div>
			<?php echo form_hidden('noid', $noid); ?>
			<?php echo form_close() ?>
	</div>
</div>
<?php if ($personal<30):?>
	<div class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<strong>INFO ! : </strong> Periode ini Syarat Tutup Point Belum Terpenuhi.
	</div>
	
<?php else :?>	
	<div class="alert alert-success">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<strong>INFO ! : </strong> Periode ini  Syarat Tutup Point Terpenuhi..
	</div>
<?php endif;?>	
<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			
		</ul>
		<h3 class="block-title">Detail Bonus Belanja : [{noid}]</h3>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive" width="100%">
			<table width="100%" class="table table-bordered table-striped table-responsive" id="datatable_index">
				<thead>
					<tr>                                    
						<th>Jenis Bonus</th>
						<th>Normal</th>
						<th>PassUp</th>
						<th></th>
						<th>Insentif</th>
						<th></th>
						<th class="right">Total</th>					
					</tr>
				</thead>
				<tbody>
					<tr>
					<td>Personal</td>
					<td <?php if ($personal<30){echo 'style="color: red; font-weight: bold;"';}else{echo 'style="color: green; font-weight: bold;"';}?>><?php echo number_format($personal,0,",",".");?></td>
					<td>-</td>
					<td class="center"> X </td>
					<td>Rp 100,-</td>
					<td class="center"> = </td>
					<td class="right">Rp <?php echo number_format($personalval,0,",","."); ?>,-</td>
				</tr>			
				<tr>
					<td>Level 1</td>
					<td><?php echo number_format($normal1,0,",","."); ?></td>
					<td <?php if ($salelevel1>$normal1){echo 'style="color: green; font-weight: bold;"';}?>><?php echo number_format($salelevel1,0,",","."); ?></td>
					<td class="center"> X </td>
					<td>Rp 100,-</td>
					<td class="center"> = </td>
					<td class="right">Rp <?php echo number_format($salelevel1val,0,",","."); ?>,-</td>
				</tr>
				<tr>
					<td>Level 2</td>
					<td><?php echo number_format($normal2,0,",","."); ?></td>
					<td <?php if ($salelevel2>$normal2){echo 'style="color: green; font-weight: bold;"';}?>><?php echo number_format($salelevel2,0,",","."); ?></td>
					<td class="center"> X </td>
					<td>Rp 100,-</td>
					<td class="center"> = </td>
					<td class="right">Rp <?php echo number_format($salelevel2val,0,",","."); ?>,-</td>
				</tr>
				<tr>
					<td>Level 3</td>
					<td><?php echo number_format($normal3,0,",","."); ?></td>
					<td <?php if ($salelevel3>$normal3){echo 'style="color: green; font-weight: bold;"';}?>><?php echo number_format($salelevel3,0,",","."); ?></td>
					<td class="center"> X </td>
					<td>Rp 100,-</td>
					<td class="center"> = </td>
					<td class="right">Rp <?php echo number_format($salelevel3val,0,",","."); ?>,-</td>
				</tr>
				<tr>
					<td>Level 4</td>
					<td><?php echo number_format($normal4,0,",","."); ?></td>
					<td <?php if ($salelevel4>$normal4){echo 'style="color: green; font-weight: bold;"';}?>><?php echo number_format($salelevel4,0,",","."); ?></td>
					<td class="center"> X </td>
					<td>Rp 100,-</td>
					<td class="center"> = </td>
					<td class="right">Rp <?php echo number_format($salelevel4val,0,",","."); ?>,-</td>
				</tr>
				<tr>
					<td>Level 5</td>
					<td><?php echo number_format($normal5,0,",","."); ?></td>
					<td <?php if ($salelevel5>$normal5){echo 'style="color: green; font-weight: bold;"';}?>><?php echo number_format($salelevel5,0,",","."); ?></td>
					<td class="center"> X </td>
					<td>Rp 50,-</td>
					<td class="center"> = </td>
					<td class="right">Rp <?php echo number_format($salelevel5val,0,",","."); ?>,-</td>
				</tr>
				<tr>
					<td>Level 6</td>
					<td><?php echo number_format($normal6,0,",","."); ?></td>
					<td <?php if ($salelevel6>$normal6){echo 'style="color: green; font-weight: bold;"';}?>><?php echo number_format($salelevel6,0,",","."); ?></td>
					<td class="center"> X </td>
					<td>Rp 50,-</td>
					<td class="center"> = </td>
					<td class="right">Rp <?php echo number_format($salelevel6val,0,",","."); ?>,-</td>
				</tr>
				<tr>
					<td>Level 7</td>
					<td><?php echo number_format($normal7,0,",","."); ?></td>
					<td <?php if ($salelevel7>$normal7){echo 'style="color: green; font-weight: bold;"';}?>><?php echo number_format($salelevel7,0,",","."); ?></td>
					<td class="center"> X </td>
					<td>Rp 50,-</td>
					<td class="center"> = </td>
					<td class="right">Rp <?php echo number_format($salelevel7val,0,",","."); ?>,-</td>
				</tr>
				<tr>
					<td>Level 8</td>
					<td><?php echo number_format($normal8,0,",","."); ?></td>
					<td <?php if ($salelevel8>$normal8){echo 'style="color: green; font-weight: bold;"';}?>><?php echo number_format($salelevel8,0,",","."); ?></td>
					<td class="center"> X </td>
					<td>Rp 50,-</td>
					<td class="center"> = </td>
					<td class="right">Rp <?php echo number_format($salelevel8val,0,",","."); ?>,-</td>
				</tr>
				<tr>
					<td>Level 9</td>
					<td><?php echo number_format($normal9,0,",","."); ?></td>
					<td <?php if ($salelevel9>$normal9){echo 'style="color: green; font-weight: bold;"';}?>><?php echo number_format($salelevel9,0,",","."); ?></td>
					<td class="center"> X </td>
					<td>Rp 50,-</td>
					<td class="center"> = </td>
					<td class="right">Rp <?php echo number_format($salelevel9val,0,",","."); ?>,-</td>
				</tr>
				<tr>
					<td>Level 10</td>
					<td><?php echo number_format($normal10,0,",","."); ?></td>
					<td <?php if ($salelevel10>$normal10){echo 'style="color: green; font-weight: bold;"';}?>><?php echo number_format($salelevel10,0,",","."); ?></td>
					<td class="center"> X </td>
					<td>Rp 50,-</td>
					<td class="center"> = </td>
					<td class="right">Rp <?php echo number_format($salelevel10val,0,",","."); ?>,-</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td><b>Total Bonus Belanja</b></td>
					<td class="center"> = </td>
					<td class="right"><b>Rp <?php echo number_format($totalkomisi,0,",","."); ?>,-</b></td>
				</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>




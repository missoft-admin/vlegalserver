
<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>
				<a href="{base_url}mmember" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title"><i class="si si-users"></i> {title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('mmember/viewkomisi','class="form-horizontal push-10-t"') ?>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Noid</label>
				<div class="col-md-3">
					<input  type="text" readonly class="form-control input-sm" name="noid" id="noid" placeholder="Noid" value="{noid}" />
				</div>
				<div class="col-md-7">
					<input  type="text" readonly class="form-control input-sm" name="namamembers" id="namamembers" placeholder="Nama Point Distribusi" value="{namamembers}" />
				</div>
			</div>
			<div class="form-group"> 
				<label class="col-md-2 control-label">Periode :</label>
				<div class="col-md-3">
					<select class="form-control input-sm" name="bulan" id="bulan" style="width : 100%">
						<?php  echo opt_month($bulan); ?>
					</select>
				</div>
				
				<div class="col-md-3">
					<select class="form-control input-sm" name="tahun" id="tahun" style="width : 100%">
						<?php for ($th=date('y');$th >= 10;$th--){ ?>
						<option value=<?php echo $th; ?> <?php if ($tahun==$th){echo 'selected="selected"';}?> class="ayrsingle"><?php echo '20'.$th; ?></option>
						<?php } ?>
					</select>	
				</div>
						
			</div>
			
			
			<div class="form-group">
				<label class="col-md-2 control-label"></label>
				<div class="col-md-10">
					<button class="btn btn-success" type="submit">OK</button>
				</div>
			</div>
			<?php echo form_hidden('noid', $noid); ?>
			<?php echo form_close() ?>
	</div>
</div>
<?php if ($personal<30):?>
	<div class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<strong>INFO ! : </strong> Periode ini Syarat Tutup Point Belum Terpenuhi.
	</div>
	
<?php else :?>	
	<div class="alert alert-success">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<strong>INFO ! : </strong> Periode ini  Syarat Tutup Point Terpenuhi..
	</div>
<?php endif;?>	
<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			
		</ul>
		<h3 class="block-title">Detail Bonus Belanja : [{noid}]</h3>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<div class="table-responsive" width="100%">
			<table width="100%" class="table table-bordered table-striped table-responsive" id="datatable_index">
				<thead>
					<tr>                                    
						<td width="5%">No</td>
						<td width="15%">Nomor Penjualan</td>
						<td width="20%">Tanggal</td>
						<td width="10%">Jumlah Belanja</td>
						<td width="20%">Operator</td>				
						<td width="5%">Tools</td>					
					</tr>
				</thead>
				<tbody>
					<?php
				foreach($query as $row): ?>	
					<?php $no = $no + 1 ?>
					<tr>						
						<td><?= $no; ?> </td>
						<td><?php echo anchor('stokis/detail_belanja/'.$row->nojual,$row->nojual) ?> </td>
						<td><? echo date_format(date_create($row->tgljual),'d F Y'); ?> </td>
						<td><? echo number_format($row->total,0,",","."); ?> </td>
						
						<td align="center"></span><? echo $row->namac; ?> </td>						
						<td align="center">
							<?php echo tool_view('stokis/detail_belanja/'.$row->nojual); ?>
						</td>
					</tr>
			<?php endforeach;?>	
				</tbody>
			</table>
		</div>
	</div>
</div>




<script type="text/javascript">
$(document).ready(function(){
<?php
if(!empty($_SESSION['msg'])){
echo $_SESSION['msg'];
$_SESSION['msg']='';
}else{
$_SESSION['msg']='';
}
?>
})
</script>
<?php
$uri=$this->uri->segment(1);
    $uri2=$this->uri->segment(2);
	 	$uri3=$this->uri->segment(3);
$arrmodules = explode(',',$newmodules);
$a='';
$b='';
if($newstatus==1||$newstatus==''){
	$a='checked';
	$b='';
}else
if($newstatus==0){
	$a='';
	$b='checked';
}
	 	?>
<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>
				<button type="button" data-toggle="tooltip" title="" data-original-title="New Group"><i class="si si-user"></i></button>
			</li>
            <li>
                <button title="Refresh List"  id="refresh_list" type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i class="si si-refresh"></i></button>
            </li>
			<li class="dropdown">
				<button type="button" data-toggle="dropdown" aria-expanded="false">More <span class="caret"></span></button>
				<ul class="dropdown-menu dropdown-menu-right">
					<?if ($uri2=='new-group'){?>
						<li class="dropdown-header">New Group </li>
						<li>
							<a tabindex="-1" href="{base_url}setting/group-users">All Group</a>
						</li>
					<?}else{?>			
						<li class="dropdown-header">All Group </li>
						<li>
							<a tabindex="-1" href="{base_url}setting/new-group">New Group</a>
						</li>
					<?}?>
				</ul>
			</li>
            
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<?= ($this->agent->is_mobile())? '<div class="table-responsive">' : '' ?>
		<form method="post" action="{url_proses}">
<input type="hidden" value="<?=(!empty($uri3))?$uri3:''?>" id="hide-ID" name="hide-ID">
		<div class="form-group">
			<label>Group Title</label>
			<input type="text" value="{newtitle}" class="form-control" id="new-title" name="new-title" required pattern="[a-z A-Z]+">
		</div>
		<div class="form-group">
			<label>Modules</label>
			<br><?=str_repeat('&nbsp;',9)?>
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary">
                <input type="checkbox" name="new-modules[]" <?=(in_array('vlegal',$arrmodules))?'checked':''?> value="vlegal"><span></span> V-Legal
            </label>
			<br><?=str_repeat('&nbsp;',9)?>
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary">
                <input type="checkbox" name="new-modules[]" <?=(in_array('report',$arrmodules))?'checked':''?> value="report"><span></span> Report
            </label>
			<br><?=str_repeat('&nbsp;',9)?>
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary">
                <input type="checkbox" name="new-modules[]" <?=(in_array('settings',$arrmodules))?'checked':''?> value="settings"><span></span> Settings [User Management]
            </label>
			<br><?=str_repeat('&nbsp;',9)?>
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary">
                <input type="checkbox" name="new-modules[]" <?=(in_array('data_referensi',$arrmodules))?'checked':''?> value="data_referensi"><span></span> Settings [Data Referensi]
            </label>
            <?php
            if(!empty($uri3)){?>
			<br><?=str_repeat('&nbsp;',9)?>
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary">
                <input type="checkbox" name="new-modules[]" value="publikasi" <?php echo (in_array('publikasi',$arrmodules)) ? 'checked' : '' ?>><span></span> Laporan Publikasi
            </label>
			<br><?=str_repeat('&nbsp;',9)?>
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary">
                <input type="checkbox" name="new-modules[]" value="vlegal_advance" <?php echo (in_array('vlegal_advance',$arrmodules)) ? 'checked' : '' ?>><span></span> V-Legal Advanced
            </label>
			<br><?=str_repeat('&nbsp;',9)?>
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary">
                <input type="checkbox" name="new-modules[]" value="report_advance" <?php echo (in_array('report_advance',$arrmodules)) ? 'checked' : '' ?>><span></span> Report Advanced
            </label>
            <?php
			    }else{
		    ?>
			<br><?=str_repeat('&nbsp;',9)?>
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary">
                <input type="checkbox" name="new-modules[]" value="cetak_vlegal"><span></span> Cetak Dokumen V-Legal
            </label>
			<br><?=str_repeat('&nbsp;',9)?>
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary">
                <input type="checkbox" name="new-modules[]" value="client_setting"><span></span> Client Settings [Data Referensi]
            </label>
			<br><?=str_repeat('&nbsp;',9)?>
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary">
                <input type="checkbox" name="new-modules[]" value="client_bahanbaku"><span></span> Client Bahan Baku
            </label>
			<br><?=str_repeat('&nbsp;',9)?>
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary">
                <input type="checkbox" name="new-modules[]" value="client_produksi"><span></span> Client Produksi
            </label>
			<br><?=str_repeat('&nbsp;',9)?>
			<label class="css-input css-checkbox css-checkbox-sm css-checkbox-primary">
                <input type="checkbox" name="new-modules[]" value="client_ekspor"><span></span> Client Pengeluaran Produk [Ekspor]
            </label>
            <?php
        		}
        	?>
		</div>
		<div class="form-group">
			<label>Status <sub class="text-danger">*set "NonActive" for disable group</sub></label><br>
            <label class="css-input css-radio css-radio-primary push-10-r">
                <input type="radio"<?=$a?> name="new-status" value="1"><span></span> Active
            </label>
            <label class="css-input css-radio css-radio-primary">
                <input type="radio"<?=$b?> name="new-status" value="0"><span></span> NonActive
            </label>
		</div>
		<!-- <div class="pull-left"> -->
			<button class="btn btn-primary ">Save</button>
	</form>
		<!-- </div> -->
		<?= ($this->agent->is_mobile())? '</div>' : '' ?>
		<div class="block-footer">&nbsp;</div>
	</div>
</div>
<input type="hidden" value="" id="hide-usergroup" name="hide-usergroup">
<script type="text/javascript">
$(function(){

function selecta(){
	$o=$('#hide-usergroup').val()
	$.ajax({
		url:'{site_url}json/groupusers',
		dataType:'json',
		beforeSend:function(){
$('#user-group').append('<option>Processing..</option>')			
		},
		success:function(data){
setTimeout(function(){

			$('#user-group').empty();
$('#user-group').select2()
			for(var i = 0; i < data.length; i++){
if(data[i].ugroup_id==$o){$p='selected';}else{$p='';}
				$('#user-group').append(`<option `+$p+` value="`+data[i].ugroup_id+`">`+data[i].ugroup_title+`</option>`);
			}
},500)

		}
	});
}
    $(document).ready(function() {

	})

})
</script>


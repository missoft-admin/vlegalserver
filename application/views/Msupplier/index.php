<script type="text/javascript">
$(document).ready(function(){
<?php
if(!empty($_SESSION['msg'])){
echo $_SESSION['msg'];
$_SESSION['msg']='';
}else{
$_SESSION['msg']='';
}
?>
})
</script>
<?$uri=$this->uri->segment(1);
	 $uri2=$this->uri->segment(2);	?>
<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>
				<button type="button" data-toggle="tooltip" title="" data-original-title="All {tJudul}"><i class="si si-user"></i></button>
			</li>
            <li>
                <button title="Refresh List"  id="refresh_list" type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i class="si si-refresh"></i></button>
            </li>
			<li class="dropdown">
				<button type="button" data-toggle="dropdown" aria-expanded="false">More <span class="caret"></span></button>
				<ul class="dropdown-menu dropdown-menu-right">
					<?if ($uri2=='new-supplier'){?>
						<li class="dropdown-header">New {tJudul} </li>
						<li>
							<a tabindex="-1" href="{base_url}setting/supplier">All {tJudul}</a>
						</li>
					<?}else{?>			
						<li class="dropdown-header">All {tJudul} </li>
						<li>
							<a tabindex="-1" href="{base_url}setting/new-supplier">New {tJudul}</a>
						</li>
					<?}?>
				</ul>
			</li>
            
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<?= ($this->agent->is_mobile())? '<div class="table-responsive">' : '' ?>
		
			<table width="100%" class="table table-bordered table-striped table-responsive" id="list-DT">
				<thead>
					<tr>
						<th width="3%">No.</th>
						<th width="5%">negara</th>
						<th width="10%">nama {tJudul}</th>
						<th width="25%">alamat</th>
						<th width="5%">status</th>
						<th width="3%">action</th>
						<!-- <th hidden></th> -->
					</tr>
				</thead>
			</table>
		<?= ($this->agent->is_mobile())? '</div>' : '' ?>
	</div>
</div>
<script type="text/javascript">
$(function(){

    $(document).ready(function() {
     	// without numbering
     // getDataTable("#list-DT","{url_ajax}");
    	// with numbering
     getDataSSP("#list-DT","{url_ajax}");
	})

   $(document).on("click",".delete-user",function(){
var id=$(this).attr("data-id");
var buton=$(this);
$.ajax({

    url:"{site_url}ajax/delSupplier",
    data:{"id":id},
    dataType:'text',
    success: function(data){
     $('#list-DT').each(function() {
      dt = $(this).dataTable();
      dt.fnDraw();
 ToastrSukses("Supplier "+data+" berhasil dihapus","Info")
  })
}
}); 
   })
   $(document).on("click",".statusAlamat",function(){
var id=$(this).attr("data-id");
var buton=$(this);
$.ajax({

    url:"{site_url}ajax/upStatusSupplier",
    data:{"id":id},
    dataType:'html',
    success: function(data){ 
 ToastrSukses("Status berhasil diubah","Info")
 buton.empty()
 buton.html(data)
}
}); 

  });
})
</script>



<li>
	<a <?=(empty($this->uri->segment(1))||$this->uri->segment(1)=='dashboard')?'class="active"':''?> href="{base_url}dashboard"><i class="si si-speedometer"></i><span class="sidebar-mini-hide">Dashboard</span></a>
</li>
<li class="nav-main-heading"><span class="sidebar-mini-hide">Main Menu</span></li>
<li <?=menuIsOpen('setting')?>>
	<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-layers"></i><span class="sidebar-mini-hide">Setting</span></a>
	<ul>		
		<li>
			<a <?=menuIsActive('users')?><?=menuIsActive('new-users')?> href="{base_url}setting/users">
				<i class="si si-bar-chart"></i>Users
			</a>
			</li>
		<li>
			<a <?=menuIsActive('group-users')?><?=menuIsActive('new-group')?> href="{base_url}setting/group-users">
				<i class="si si-bar-chart"></i>Group Users
			</a>
			</li>
		<li>
			<a <?=menuIsActive('client')?><?=menuIsActive('new-client')?> href="{base_url}setting/client">
				<i class="si si-bar-chart"></i>Clients
			</a>
			</li>
		<li>
			<a <?=menuIsActive('buyers')?><?=menuIsActive('new-buyer')?> href="{base_url}setting/buyers">
				<i class="si si-bar-chart"></i>Buyers
			</a>
			</li>
		<li>
			<a <?=menuIsActive('supplier')?><?=menuIsActive('new-supplier')?> href="{base_url}setting/supplier">
				<i class="si si-bar-chart"></i>Suppliers
			</a>
			</li>
		<li>
			<a <?=menuIsActive('jenis-kayu')?><?=menuIsActive('new-jeniskayu')?> href="{base_url}setting/jenis-kayu">
				<i class="si si-bar-chart"></i>Data Jenis Kayu
			</a></li>
		<li>
			<a <?=menuIsActive('produk')?><?=menuIsActive('new-produk')?> href="{base_url}setting/produk">
				<i class="si si-bar-chart"></i>Data Produk
			</a>
			</li>
		<li>
			<a <?=menuIsActive('sortimen')?><?=menuIsActive('new-sortimen')?> href="{base_url}setting/sortimen">
				<i class="si si-bar-chart"></i>Data Sortimen
			</a>
			</li>
		<li>
			<a <?=menuIsActive('wip')?><?=menuIsActive('new-wip')?> href="{base_url}setting/wip">
				<i class="si si-bar-chart"></i>Data WIP
			</a>
			</li>
		<li>
			<a <?=menuIsActive('hscode')?><?=menuIsActive('new-hscode')?> href="{base_url}setting/hscode">
				<i class="si si-bar-chart"></i>HS Code
			</a>
			</li>
		<li>
			<a <?=menuIsActive('loading')?><?=menuIsActive('new-loading')?> href="{base_url}setting/loading">
				<i class="si si-bar-chart"></i>Port of Loading
			</a></li>
		<li>
			<a <?=menuIsActive('discharge')?><?=menuIsActive('new-discharge')?> href="{base_url}setting/discharge">
				<i class="si si-bar-chart"></i>Port of Discharge
			</a></li>
        <li>
        	<a <?=menuIsActive('settingloading')?><?=menuIsActive('new-settingloading')?> href="{base_url}setting/settingloading">
        		<i class="si si-bar-chart"></i>Port of Loading Setting
        	</a>
        	</li>
	</ul>
</li>


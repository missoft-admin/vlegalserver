<!DOCTYPE html>
<!--[if IE 9]>          <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-focus" lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>V-Legal PT TUV Rheinland Indonesia | {title}</title>

        <meta name="keyword" content="vlegal,tuv">
        <meta name="description" content="V-Legal PT TUV Rheinland Indonesia">
        <meta name="author" content="CV. Kurvasoft">
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="{favicon_url}">

        <link rel="icon" type="image/png" href="{favicon_url}">
        <link rel="icon" type="image/png" href="{favicon_url}">
        <link rel="icon" type="image/png" href="{favicon_url}">
        <link rel="icon" type="image/png" href="{favicon_url}">
        <link rel="icon" type="image/png" href="{favicon_url}">
        <!-- END Icons -->
       
        <meta name="theme-color" content="#ffffff">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Web fonts -->
        <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700"> -->

        <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome-animation/0.1.0/font-awesome-animation.min.css" type="text/css" />
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
        <link rel="stylesheet" href="{js_path}plugins/magnific-popup/magnific-popup.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
        <!-- <link rel="stylesheet" href="{js_path}plugins/datatables/jquery.dataTables.min.css"> -->
        <!-- https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css -->
<!-- https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css -->
        <link rel="stylesheet" href="{js_path}plugins/select2/select2.min.css">
        <link rel="stylesheet" href="{js_path}plugins/select2/select2-bootstrap.min.css">
        <link rel="stylesheet" href="{js_path}plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css">
        <link rel="stylesheet" href="{css_path}bootstrap.min.css">
        <link rel="stylesheet" id="css-main" href="{css_path}oneui.css">
        <!-- <script src="{js_path}core/jquery.min.js"></script> -->
        <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
        <script src="{assets_path}custom/basic-function.js"></script>
        <link rel="stylesheet" id="css-main" href="{toastr_css}">
        <!-- <link rel="stylesheet" href="{js_path}plugins/select2/select2.min.css"> -->
        <!-- <link rel="stylesheet" href="{js_path}plugins/select2/select2-bootstrap.min.css"> -->

        <!-- END Stylesheets -->
    </head>
    <body>
        <script type="text/javascript">
            var site_url        = "{site_url}";
            var ajax_url        = "{ajax_url}";
            var avatar_tpath    = "{avatar_tpath}";
            var assets_path     = "{assets_path}";
            // alert(ajax_url);
        </script>
        <?php $menu = $this->uri->segment(1); ?>
        <div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed">

            <!-- Sidebar -->
            <nav id="sidebar">
                <!-- Sidebar Scroll Container -->
                <div id="sidebar-scroll">
                    <!-- Sidebar Content -->
                    <!-- Adding .sidebar-mini-hide to an element will hide it when the sidebar is in mini mode -->
                    <div class="sidebar-content">
                        <!-- Side Header -->
                        <div class="side-header side-content" style="background-color: #fff;">
                            <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                            <button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close">
                                <i class="fa fa-times"></i>
                            </button>

                            <a class="h5 text-white" href="{base_url}dashboard">
                                <img src="{upload_path}logo/tsiADMIN.png" alt="" style="width: 120px; margin-left: 25%;">
                            </a>
                        </div>
                        <!-- END Side Header -->

                        <!-- Side Content -->
                        <div class="side-content side-content-full">
                            <ul class="nav-main">
                              <?php $this->load->view('module_navigation'); ?>
                            </ul>
                        </div>
                        <!-- END Side Content -->
                    </div>
                    <!-- Sidebar Content -->
                </div>
                <!-- END Sidebar Scroll Container -->
            </nav>
            <!-- END Sidebar -->

            <!-- Header -->
            <header id="header-navbar" class="content-mini content-mini-full">
              <?php $this->load->view('module_header');?>
            </header>
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">
              <!-- Page Content -->
              <?php if($menu != 'dashboard'){ ?>
              <div class="content">
              <?php } ?>

                <!-- Breadcrumb -->
                <?php if($menu != 'dashboard'){ ?>
                <ol class="breadcrumb push-15">
                    <?php echo BackendBreadcrum($breadcrum)?>
                </ol>
                <?php } ?>
                <!-- END Breadcrumb -->

                <?php $this->load->view($template);?>
              <?php if($menu != 'dashboard'){ ?>
              </div>
              <?php } ?>
              <!-- END Page Content -->
            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            <footer id="page-footer" class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix">
                <div class="pull-left">
                    <a class="font-w600" href="#" target="_blank">Tridaya Sinergi Indonesia &copy; 2018</span>
                </div>
                <div class="pull-right">
                    Created with <i class="fa fa-heart text-city"></i> by <a class="font-w600" href="https://tridayasinergi.com" target="_blank">ICT TSI</a>
                </div>
            </footer>
            <!-- END Footer -->
        </div>
        <!-- END Page Container -->

        <!-- OneUI Core JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js -->
        <script src="{js_path}core/bootstrap.min.js"></script>

        <script src="{toastr_js}"></script>
        <script src="{js_path}core/jquery.slimscroll.min.js"></script>
        <script src="{js_path}core/jquery.scrollLock.min.js"></script>
        <script src="{js_path}core/jquery.appear.min.js"></script>
        <script src="{js_path}core/jquery.countTo.min.js"></script>
        <script src="{js_path}core/jquery.placeholder.min.js"></script>
        <script src="{js_path}core/js.cookie.min.js"></script>
        <script src="{js_path}plugins/magnific-popup/magnific-popup.min.js"></script>
            
        <script src="{js_path}app.js"></script>
        <script src="{js_path}basic.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/plug-ins/1.10.19/api/fnPagingInfo.js"></script>
        <script src="{js_path}plugins/select2/select2.full.min.js"></script>
        <script src="{js_path}plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
        <!-- <script src="{js_path}pages/base_tables_datatables.js"></script> -->
        <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
		<script src="{js_path}plugins/jquery-number/jquery.number.min.js"></script>
		
		<script src="{js_path}plugins/jquery-validation/jquery.validate.min.js"></script>
		<script src="{js_path}plugins/masked-inputs/jquery.maskedinput.min.js"></script>
		
		<?
		if (file_exists("./assets/js/addon/".$template.".js")){?>
        <script src="{js_path}addon/{template}.js"></script>
		<?}?>
        
    </body>
</html>

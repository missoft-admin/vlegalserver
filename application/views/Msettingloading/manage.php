<script type="text/javascript">
$(document).ready(function(){
<?php
if(!empty($_SESSION['msg'])){
echo $_SESSION['msg'];
$_SESSION['msg']='';
}else{
$_SESSION['msg']='';
}
?>
})
</script>
<?php
$uri=$this->uri->segment(1);
    $uri2=$this->uri->segment(2);
	 	$uri3=$this->uri->segment(3);
// $arrmodules = explode(',',$newmodules);
$a='';
$b='';
if($newstatus==1||$newstatus==''){
	$a='checked';
	$b='';
}else
if($newstatus==0){
	$a='';
	$b='checked';
}
	 	?>
<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>
				<button type="button" data-toggle="tooltip" title="" data-original-title="<?=(!empty($uri3)?'Update':'New')?> {tJudul}"><i class="si si-user"></i></button>
			</li>
            <li>
                <button title="Refresh List"  id="refresh_list" type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i class="si si-refresh"></i></button>
            </li>
			<li class="dropdown">
				<button type="button" data-toggle="dropdown" aria-expanded="false">More <span class="caret"></span></button>
				<ul class="dropdown-menu dropdown-menu-right">
					<?if ($uri2==$url_kedua){?>
						<li class="dropdown-header">New {tJudul} </li>
						<li>
							<a tabindex="-1" href="{url_index}">All {tJudul}</a>
						</li>
					<?}else{?>			
						<li class="dropdown-header">All {tJudul} </li>
						<li>
							<a tabindex="-1" href="{url_addnew}">New {tJudul}</a>
						</li>
					<?}?>
				</ul>
			</li>
            
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<?= ($this->agent->is_mobile())? '<div class="table-responsive">' : '' ?>
		<form method="post" action="{url_proses}">
<input type="hidden" value="<?=(!empty($uri3))?$uri3:''?>" id="hide-ID" name="hide-ID">
		<div class="form-group">
			<label>Clients</label>
			<select class="form-control new-clients" id="new-client_id" name="new-client_id">
			</select>
		</div>
		<div class="form-group">
			<label>Uraian Loading</label>
			<select class="form-control new-loading" id="new-idloading" name="new-idloading">
			</select>
		</div>
		<!-- <div class="pull-left"> -->
<input type="hidden" value="{newclient_id}" id="hide-clients" name="hide-clients">
<input type="hidden" value="{newidloading}" id="hide-loading" name="hide-loading">
			<button class="btn btn-primary ">Save</button>
	</form>
		<!-- </div> -->
		<?= ($this->agent->is_mobile())? '</div>' : '' ?>
		<div class="block-footer">&nbsp;</div>
	</div>
</div>
<script type="text/javascript">
$(function(){
$hideClient  = $('#hide-clients').val().split(',');
$client  	 = $('.new-clients');
$hideLoading = $('#hide-loading').val().split(',');
$loading  	 = $('.new-loading');

    $(document).ready(function() {
$client.select2({
    minimumInputLength: 2,
    tags: true,
    triggerChange: true,
   placeholder: "Cari Clients",
    ajax: {
        url: '{json_client}',
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (params) {
      var query = {
        search: params.term,
        type: 'public'
      }
      return query;
        },
        processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.client_nama,
                        id: item.client_id
                    }
                })
            };
        }
    },
initSelection: function(element, callback) {
callback({id: $hideClient[0], text: $hideClient[1] });
}
})

$loading.select2({
    minimumInputLength: 2,
    tags: true,
    triggerChange: true,
   placeholder: "Cari Uraian Loading",
    ajax: {
        url: '{json_loading}',
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (params) {
      var query = {
        search: params.term,
        type: 'public'
      }
      return query;
        },
        processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.uraian,
                        id: item.idloading
                    }
                })
            };
        }
    },
initSelection: function(element, callback) {
callback({id: $hideLoading[0], text: $hideLoading[1] });
}
})

$client.change(function(){
$('#hide-clients').val($('.new-clients option:selected').val())
})
$loading.change(function(){
$('#hide-loading').val($('.new-loading option:selected').val())
})
	})

})
</script>




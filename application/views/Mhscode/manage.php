<script type="text/javascript">
$(document).ready(function(){
<?php
if(!empty($_SESSION['msg'])){
echo $_SESSION['msg'];
$_SESSION['msg']='';
}else{
$_SESSION['msg']='';
}
?>
})
</script>
<?php
$uri=$this->uri->segment(1);
    $uri2=$this->uri->segment(2);
	 	$uri3=$this->uri->segment(3);
// $arrmodules = explode(',',$newmodules);
$a='';
$b='';
if($newstatus==1||$newstatus==''){
	$a='checked';
	$b='';
}else
if($newstatus==0){
	$a='';
	$b='checked';
}
	 	?>
<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>
				<button type="button" data-toggle="tooltip" title="" data-original-title="<?=(!empty($uri3)?'Update':'New')?> {tJudul}"><i class="si si-user"></i></button>
			</li>
            <li>
                <button title="Refresh List"  id="refresh_list" type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i class="si si-refresh"></i></button>
            </li>
			<li class="dropdown">
				<button type="button" data-toggle="dropdown" aria-expanded="false">More <span class="caret"></span></button>
				<ul class="dropdown-menu dropdown-menu-right">
					<?if ($uri2==$url_kedua){?>
						<li class="dropdown-header">New {tJudul} </li>
						<li>
							<a tabindex="-1" href="{url_index}">All {tJudul}</a>
						</li>
					<?}else{?>			
						<li class="dropdown-header">All {tJudul} </li>
						<li>
							<a tabindex="-1" href="{url_addnew}">New {tJudul}</a>
						</li>
					<?}?>
				</ul>
			</li>
            
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<?= ($this->agent->is_mobile())? '<div class="table-responsive">' : '' ?>
		<form method="post" action="{url_proses}">
<input type="hidden" value="<?=(!empty($uri3))?$uri3:''?>" id="hide-ID" name="hide-ID">
		<div class="form-group">
			<label>{tJudul}</label>
			<input type="text" value="{oldhscode}" class="form-control" id="old-hscode" name="old-hscode" placeholder="{tJudul}" required>
		</div>
		<div class="form-group">
			<label>{tJudul} 8 Digit</label>
			<input type="text" value="{newhscode_8_f}" class="form-control" id="new-hscode_8_f" name="new-hscode_8_f" placeholder="{tJudul} 8 Digit (Baru)" required>
		</div>
		<div class="form-group">
			<label>Kelompok</label>
			<input type="text" value="{newkelompok}" class="form-control" id="new-kelompok" name="new-kelompok" placeholder="Kelompok {tJudul} (Optional)">
		</div>
		<div class="form-group">
			<label>Uraian</label>
			<textarea class="form-control" rows="6" id="new-uraian" name="new-uraian" placeholder="Uraian {tJudul}">{newuraian}</textarea>
		</div>
		<div class="form-group">
			<label>Status <sub class="text-danger">*set "NonActive" for disable {dJudul}</sub></label><br>
            <label class="css-input css-radio css-radio-primary push-10-r">
                <input type="radio"<?=$a?> name="new-status" value="1"><span></span> Active
            </label>
            <label class="css-input css-radio css-radio-primary">
                <input type="radio"<?=$b?> name="new-status" value="0"><span></span> NonActive
            </label>
		</div>
		<!-- <div class="pull-left"> -->
<input type="hidden" value="{newnegara}" id="hide-negara" name="hide-negara">
			<button class="btn btn-primary ">Save</button>
	</form>
		<!-- </div> -->
		<?= ($this->agent->is_mobile())? '</div>' : '' ?>
		<div class="block-footer">&nbsp;</div>
	</div>
</div>
<script type="text/javascript">
$(function(){

    $(document).ready(function() {

	})

})
</script>


<script type="text/javascript">
$(document).ready(function(){
<?php
if(!empty($_SESSION['msg'])){
echo $_SESSION['msg'];
$_SESSION['msg']='';
}else{
$_SESSION['msg']='';
}
?>
})
</script>
<?php
$uri=$this->uri->segment(1);
    $uri2=$this->uri->segment(2);
	 	$uri3=$this->uri->segment(3);
// $arrmodules = explode(',',$newmodules);
$a='';
$b='';
if($newstatus==1||$newstatus==''){
	$a='checked';
	$b='';
}else
if($newstatus==0){
	$a='';
	$b='checked';
}
	 	?>
<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>
				<button type="button" data-toggle="tooltip" title="" data-original-title="<?=(!empty($uri3)?'Update':'New')?> {tJudul}"><i class="si si-user"></i></button>
			</li>
            <li>
                <button title="Refresh List"  id="refresh_list" type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i class="si si-refresh"></i></button>
            </li>
			<li class="dropdown">
				<button type="button" data-toggle="dropdown" aria-expanded="false">More <span class="caret"></span></button>
				<ul class="dropdown-menu dropdown-menu-right">
					<?if ($uri2=='new-buyer'){?>
						<li class="dropdown-header">New {tJudul} </li>
						<li>
							<a tabindex="-1" href="{base_url}setting/buyers">All {tJudul}</a>
						</li>
					<?}else{?>			
						<li class="dropdown-header">All {tJudul} </li>
						<li>
							<a tabindex="-1" href="{base_url}setting/new-buyer">New {tJudul}</a>
						</li>
					<?}?>
				</ul>
			</li>
            
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<?= ($this->agent->is_mobile())? '<div class="table-responsive">' : '' ?>
		<form method="post" action="{url_proses}">
<input type="hidden" value="<?=(!empty($uri3))?$uri3:''?>" id="hide-ID" name="hide-ID">
		<div class="form-group">
			<label>Negara</label>
			<select class="form-control new-negara" id="new-negara" name="new-negara">
			</select>
		</div>
		<div class="form-group">
			<label>Nama {tJudul}</label>
			<input type="text" value="{newbuyer}" class="form-control" id="new-buyer" name="new-buyer" required pattern="[a-zA-Z .-]+">
		</div>
		<div class="form-group">
			<label>Alamat {tJudul}</label>
			<textarea class="form-control" name="new-alamat" id="new-alamat">{newalamat}</textarea>
		</div>
		<div class="form-group">
			<label>Status <sub class="text-danger">*set "NonActive" for disable {dJudul}</sub></label><br>
            <label class="css-input css-radio css-radio-primary push-10-r">
                <input type="radio"<?=$a?> name="new-status" value="1"><span></span> Active
            </label>
            <label class="css-input css-radio css-radio-primary">
                <input type="radio"<?=$b?> name="new-status" value="0"><span></span> NonActive
            </label>
		</div>
		<!-- <div class="pull-left"> -->
<input type="hidden" value="{newnegara}" id="hide-negara" name="hide-negara">
			<button class="btn btn-primary ">Save</button>
	</form>
		<!-- </div> -->
		<?= ($this->agent->is_mobile())? '</div>' : '' ?>
		<div class="block-footer">&nbsp;</div>
	</div>
</div>
<script type="text/javascript">
$(function(){
$negaraS = $('#hide-negara').val().split(',');

    $(document).ready(function() {
 $('.new-negara').select2({
    minimumInputLength: 2,
    tags: [],
   placeholder: "Cari Negara",
    ajax: {
        url: '{site_url}json/negara',
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (params) {
      var query = {
        search: params.term,
        type: 'public'
      }
      return query;
        },
        processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.negara,
                        id: item.idnegara
                    }
                })
            };
        }
    },
initSelection: function(element, callback) {
callback({id: $negaraS[0], text: $negaraS[1] });
},

    })
 $('.new-negara').change(function(){
$('#hide-negara').val($('.new-negara option:selected').val())
})
	})

})
</script>


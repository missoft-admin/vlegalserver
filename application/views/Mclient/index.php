<script type="text/javascript">
$(document).ready(function(){
<?php
if(!empty($_SESSION['msg'])){
echo $_SESSION['msg'];
$_SESSION['msg']='';
}else{
$_SESSION['msg']='';
}
?>
})
</script>
<?$uri=$this->uri->segment(1);
	 $uri2=$this->uri->segment(2);	?>
<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>
				<button type="button" data-toggle="tooltip" title="" data-original-title="All {tJudul}"><i class="si si-user"></i></button>
			</li>
            <li>
                <button title="Refresh List"  id="refresh_list" type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i class="si si-refresh"></i></button>
            </li>
			<li class="dropdown">
				<button type="button" data-toggle="dropdown" aria-expanded="false">More <span class="caret"></span></button>
				<ul class="dropdown-menu dropdown-menu-right">
					<?if ($uri2=='{url_kedua}'){?>
						<li class="dropdown-header">New {tJudul} </li>
						<li>
							<a tabindex="-1" href="{url_index}">All {tJudul}</a>
						</li>
					<?}else{?>			
						<li class="dropdown-header">All {tJudul} </li>
						<li>
							<a tabindex="-1" href="{url_addnew}">New {tJudul}</a>
						</li>
					<?}?>
				</ul>
			</li>
            
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<?= ($this->agent->is_mobile())? '<div class="table-responsive">' : '' ?>
		
			<table width="100%" class="table table-bordered table-striped table-responsive" id="list-DT">
				<thead>
					<tr>
						<th width="3%">No.</th>
						<th width="15%">nama</th>
						<th width="20%">alamat</th>
						<th width="5%">telepon</th>
						<th width="5%">fax</th>
						<th width="5%">email</th>
						<th width="5%">status</th>
						<th width="10%">action</th>
					</tr>
				</thead>
			</table>
		<?= ($this->agent->is_mobile())? '</div>' : '' ?>
	</div>
</div>

<!-- Modal kesatu -->
<div class="modal fade in" id="modal-detail1" tabindex="-1" role="dialog" aria-hidden="true" style="display: none; padding-right: 17px;">
    <div class="modal-dialog modal-lg modal-dialog-popin" style="overflow-y: scroll; max-height:85%; width: 90%; margin-top: 50px; margin-bottom:50px;">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-primary-dark">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title judulsatu"></h3>
                </div>
                <div class="block-content" id="contentModal-1">

                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button> 
            </div>
        </div>
    </div>
</div>
<!-- end Modal kesatu -->

<!-- Modal kedua -->
<div class="modal fade in" id="modal-detail2" tabindex="-1" role="dialog" aria-hidden="true" style="display: none; padding-right: 17px;">
    <div class="modal-dialog modal-lg modal-dialog-popin" style="overflow-y: scroll; max-height:85%; width: 93%; margin-top: 50px; margin-bottom:50px;">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-primary-dark">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title juduldua"></h3>
                </div>
                <div class="block-content" id="contentModal-2">

                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button> 
            </div>
        </div>
    </div>
</div>
<!-- end Modal kedua -->

<script type="text/javascript">
$(function(){

    $(document).ready(function() {
if(window.location.hash == '#success')
{
	ToastrSukses("<?=$this->label?> baru telah ditambahkan","Info")
}
     	// without numbering
     // getDataTable("#list-DT","{url_ajax}");
    	// with numbering
     getDataSSP("#list-DT","{url_ajax}");

   $(document).on("click",".delete-row",function(){
var buton=$(this),
	id=buton.attr("data-id"),
 	idx=buton.attr('data-idx'),
 	idz=buton.attr('data-idz'),
 	tbl=buton.attr('data-tabel');
$.ajax({
    url:"{delete_row}",
    data:{"id":id,"idx":idx,"idz":idz,"tabel":tbl},
    dataType:'json',
    success: function(data){
    switch (tbl) {
        case 'kit_buyer':
     $('#list-DT1').each(function() {
      dt = $(this).dataTable();
      dt.fnDraw();
 ToastrSukses(data[0]+" "+data[1]+" berhasil dihapus","Info")
  })
            break;
        case 'kit_supplier':
     $('#list-DT2').each(function() {
      dt = $(this).dataTable();
      dt.fnDraw();
 ToastrSukses(data[0]+" "+data[1]+" berhasil dihapus","Info")
  })
            break;
        case 'kit_sortimen':
     $('#list-DT3').each(function() {
      dt = $(this).dataTable();
      dt.fnDraw();
 ToastrSukses(data[0]+" "+data[1]+" berhasil dihapus","Info")
  })
            break;
        case 'kit_wip':
     $('#list-DT4').each(function() {
      dt = $(this).dataTable();
      dt.fnDraw();
 ToastrSukses(data[0]+" "+data[1]+" berhasil dihapus","Info")
  })
            break;
        case 'kit_produk':
     $('#list-DT5').each(function() {
      dt = $(this).dataTable();
      dt.fnDraw();
 ToastrSukses(data[0]+" "+data[1]+" berhasil dihapus","Info")
  })
            break;
    }
}
}); 
   })
   $(document).on("click",".delete-user",function(){
var id=$(this).attr("data-id");
var buton=$(this);
$.ajax({

    url:"{url_delete}",
    data:{"id":id},
    dataType:'text',
    success: function(data){
     $('#list-DT').each(function() {
      dt = $(this).dataTable();
      dt.fnDraw();
 ToastrSukses("{tJudul} "+data+" berhasil dihapus","Info")
  })
}
}); 
   })
   $(document).on("click",".statusAlamat",function(){
var id=$(this).attr("data-id");
var buton=$(this);
$.ajax({

    url:"{url_uStatus}",
    data:{"id":id},
    dataType:'html',
    success: function(data){ 
 ToastrSukses("Status berhasil diubah","Info")
 buton.empty()
 buton.html(data)
}
}); 
});
    $(document).on("click", ".bolder", function () {
        detailmodal1($(this).attr('data-id'))
        $('.judulsatu').html($(this).closest('tr').find("td:eq(1)").text())
    })
    $(document).on("click", ".modal-setting", function () {
        detailmodal2($(this).attr('data-id'))
        $('.juduldua').html('Setting - '+$(this).closest('tr').find("td:eq(1)").text())
    })

    $(document).on("click", ".simpan-setting", function () {
var idclient=$('.hide-idperRow').val(),
	idbuyer=$('.newbuyer option:selected').val(),
	idsupplier=$('.newsupplier option:selected').val(),
	idsortimen=$('.newsortimen option:selected').val(),
	idwip=$('.newwip option:selected').val(),
	idproduk=$('.newproduk option:selected').val();

    $.ajax({
      url:"{url_saveSetting}",
      data:{"idclient":idclient,
      		"idbuyer":idbuyer,
      		"idsupplier":idsupplier,
      		"idsortimen":idsortimen,
      		"idwip":idwip,
      		"idproduk":idproduk},
      dataType:'script',
      success:function(data){
     $('.setG').each(function() {
      dt = $(this).dataTable();
      dt.fnDraw();})
$('.stI').empty()
       return data;
      }
    }).done(function(){
    	setTimeout(function(){
		    getBuyer()
		    getSupplier()
		    getSortimen()
		    getWip()
		    getProduk()
    	},700)
	})
}) 
/** End Document */
}) 
function getBuyer(){
	$('.newbuyer').css('width','100%')
$('.newbuyer').select2({
    minimumInputLength:0,
    placeholder: "-- Pilih Buyer --",
    tags: false,
    triggerChange: true,
    ajax: {
        url: '{json_buyer}',
        dataType: 'json',
        headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        delay: 250,
        type: "GET",
        quietMillis: 50,
        data: function (params) {
      var query = {
        search: params.term,
        page: ((params.page || 1)*30),
        type: 'public'
      }
      return query;
        },
        processResults: function (data,params) {
            var data = $.map(data, function (item) {
                    return {
                        text: item.buyer+' ['+item.idnegara+']',
                        id: item.idbuyer
                    };
          });
      params.page = params.page || 1;

      return {
        results: data,
        pagination: {
          more:(params.page *30)
        }
      };
    },
    cache: true
  },
    dropdownParent: $('#modal-detail2'),
    dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
    escapeMarkup: function (m) { return m; }
    }) 
  }
function getSupplier(){
	$('.newsupplier').css('width','100%')
$('.newsupplier').select2({
    minimumInputLength:0,
    placeholder: "-- Pilih Supplier --",
    tags: false,
    triggerChange: true,
    ajax: {
        url: '{json_supplier_}',
        dataType: 'json',
        headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        delay: 250,
        type: "GET",
        quietMillis: 50,
        data: function (params) {
      var query = {
        search: params.term,
        page: ((params.page || 1)*30),
        type: 'public'
      }
      return query;
        },
        processResults: function (data,params) {
            var data = $.map(data, function (item) {
                    return {
                        text: item.supplier+' ['+item.idnegara+']',
                        id: item.idsupplier
                    };
          });
      params.page = params.page || 1;

      return {
        results: data,
        pagination: {
          more:(params.page *30)
        }
      };
    },
    cache: true
  },
    dropdownParent: $('#modal-detail2'),
    dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
    escapeMarkup: function (m) { return m; }
    }) 
  }

function getSortimen(){
	$('.newsortimen').css('width','100%')
$('.newsortimen').select2({
    minimumInputLength:0,
    placeholder: "-- Pilih Sortimen --",
    tags: false,
    triggerChange: true,
    ajax: {
        url: '{json_sortimen}',
        dataType: 'json',
        headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        delay: 250,
        type: "GET",
        quietMillis: 50,
        data: function (params) {
      var query = {
        search: params.term,
        page: ((params.page || 1)*30),
        type: 'public'
      }
      return query;
        },
        processResults: function (data,params) {
            var data = $.map(data, function (item) {
                    return {
                        text: item.sortimen,
                        id: item.idsortimen
                    };
          });
      params.page = params.page || 1;

      return {
        results: data,
        pagination: {
          more:(params.page *30)
        }
      };
    },
    cache: true
  },
    dropdownParent: $('#modal-detail2'),
    dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
    escapeMarkup: function (m) { return m; }
    }) 
  }

function getWip(){
	$('.newwip').css('width','100%')
$('.newwip').select2({
    minimumInputLength:0,
    placeholder: "-- Pilih Wip --",
    tags: false,
    triggerChange: true,
    ajax: {
        url: '{json_wip}',
        dataType: 'json',
        headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        delay: 250,
        type: "GET",
        quietMillis: 50,
        data: function (params) {
      var query = {
        search: params.term,
        page: ((params.page || 1)*30),
        type: 'public'
      }
      return query;
        },
        processResults: function (data,params) {
            var data = $.map(data, function (item) {
                    return {
                        text: item.wip,
                        id: item.idwip
                    };
          });
      params.page = params.page || 1;

      return {
        results: data,
        pagination: {
          more:(params.page *30)
        }
      };
    },
    cache: true
  },
    dropdownParent: $('#modal-detail2'),
    dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
    escapeMarkup: function (m) { return m; }
    }) 
  }

function getProduk(){
	$('.newproduk').css('width','100%')
$('.newproduk').select2({
    minimumInputLength:0,
    placeholder: "-- Pilih Produk --",
    tags: false,
    triggerChange: true,
    ajax: {
        url: '{json_produk}',
        dataType: 'json',
        headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        delay: 250,
        type: "GET",
        quietMillis: 50,
        data: function (params) {
      var query = {
        search: params.term,
        page: ((params.page || 1)*30),
        type: 'public'
      }
      return query;
        },
        processResults: function (data,params) {
            var data = $.map(data, function (item) {
                    return {
                        text: item.produk+' ['+item.kodehs+']',
                        id: item.idproduk
                    };
          });
      params.page = params.page || 1;

      return {
        results: data,
        pagination: {
          more:(params.page *30)
        }
      };
    },
    cache: true
  },
    dropdownParent: $('#modal-detail2'),
    dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
    escapeMarkup: function (m) { return m; }
    }) 
  }

    // get detail modal 1
    function detailmodal1(id) {
        var obj = document.getElementById("contentModal-1");
        var url = '{json_detail1}' + id;
        var xmlhttp = new XMLHttpRequest();

        xmlhttp.open("GET", url);

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                obj.innerHTML = xmlhttp.responseText;
            } else {
                obj.innerHTML = "<div></div>";
            }
        }
        xmlhttp.send(null);
    }
    // get detail modal 2
    function detailmodal2(id) {
        var obj = document.getElementById("contentModal-2");
        var url = '{json_detail2}' + id;
        var xmlhttp = new XMLHttpRequest();

        xmlhttp.open("GET", url);

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                obj.innerHTML = xmlhttp.responseText;
    getDataSSP("#list-DT1","{url_buyer}?idclient="+id);
	getDataSSP("#list-DT2","{url_supplier}?idclient="+id);
	getDataSSP("#list-DT3","{url_sortimen}?idclient="+id);
	getDataSSP("#list-DT4","{url_wip}?idclient="+id);
	getDataSSP("#list-DT5","{url_produk}?idclient="+id);
    getBuyer()
    getSupplier()
    getSortimen()
    getWip()
    getProduk() 
            } else {
                obj.innerHTML = "<div></div>";
            }
        }
        xmlhttp.send(null);
    } 
})
</script>


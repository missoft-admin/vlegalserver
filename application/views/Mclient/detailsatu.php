<?php
$get =rowArray('clients',array('client_id'=>$client_id));
$getP=rowArray('kit_propinsi',array('propid'=>$get['client_propinsi']));
$getK=rowArray('kit_kabupaten',array('kabid'=>$get['client_kabupaten']));
$getU=rowArray('users',array('user_id'=>$get['client_userid']));
?>
<table width="100%" class="table table-bordered table-striped table-responsive" id="list-DT">
	<tr>
		<th width="25%">Nama Client</th>
		<td>:&nbsp;<?=$get['client_nama']?></td>
	</tr>
	<tr>
		<th>Alamat Client</th>
		<td>:&nbsp;<?=$get['client_alamat']?></td>
	</tr>
	<tr>
		<th>Provinsi</th>
		<td>:&nbsp;<?=$getP['nama']?></td>
	</tr>
	<tr>
		<th>Kabupaten</th>
		<td>:&nbsp;<?=$getK['nama']?></td>
	</tr>
	<?php
for($ap=1;$ap<=5;$ap++):
	?>
	<tr>
		<th>Alamat Pabrik <?=$ap?></th>
		<td>:&nbsp;<?=$get['client_alamatpabrik'.$ap]?></td>
	</tr>
<?php endfor;?>
	<tr>
		<th>Telepon</th>
		<td>:&nbsp;<?=$get['client_telp']?></td>
	</tr>
	<tr>
		<th>Fax</th>
		<td>:&nbsp;<?=$get['client_fax']?></td>
	</tr>
	<tr>
		<th>E-Mail</th>
		<td>:&nbsp;<?=$get['client_email']?></td>
	</tr>
	<tr>
		<th>Website</th>
		<td>:&nbsp;<?=$get['client_website']?></td>
	</tr>
	<tr>
		<th>Contact Person</th>
		<td>:&nbsp;<?=$get['client_namacp']?></td>
	</tr>
	<tr>
		<th>Jabatan CP</th>
		<td>:&nbsp;<?=$get['client_jabatancp']?></td>
	</tr>
	<tr>
		<th>Telp CP</th>
		<td>:&nbsp;<?=$get['client_telpcp']?></td>
	</tr>
	<tr>
		<th>E-Mail CP</th>
		<td>:&nbsp;<?=$get['client_emailcp']?></td>
	</tr>
	<tr>
		<th>Pejabat Dokumen V-Legal</th>
		<td>:&nbsp;<?=$get['client_namadv']?></td>
	</tr>
	<tr>
		<th>Jabatan</th>
		<td>:&nbsp;<?=$get['client_jabatandv']?></td>
	</tr>
	<tr>
		<th>Telp</th>
		<td>:&nbsp;<?=$get['client_telpdv']?></td>
	</tr>
	<tr>
		<th>E-Mail</th>
		<td>:&nbsp;<?=$get['client_emaildv']?></td>
	</tr>
	<tr>
		<th>Akta Pendirian</th>
		<td>:&nbsp;<?=$get['client_aktapendirian']?></td>
	</tr>
	<tr>
		<th>Akta Perubahan</th>
		<td>:&nbsp;<?=$get['client_aktaperubahan']?></td>
	</tr>
	<tr>
		<th>SIUP</th>
		<td>:&nbsp;<?=$get['client_siup']?></td>
	</tr>
	<tr>
		<th>Tanggal SIUP</th>
		<td>:&nbsp;<?=encode_date($get['client_siuptgl'])?></td>
	</tr>
	<tr>
		<th>Tgl. Kadaluarsa SIUP</th>
		<td>:&nbsp;<?=encode_date($get['client_siuptglkadaluarsa'])?></td>
	</tr>
	<tr>
		<th>TDP</th>
		<td>:&nbsp;<?=$get['client_tdp']?></td>
	</tr>
	<tr>
		<th>Tanggal TDP</th>
		<td>:&nbsp;<?=encode_date($get['client_tdptgl'])?></td>
	</tr>
	<tr>
		<th>Tgl. Kadaluarsa TDP</th>
		<td>:&nbsp;<?=encode_date($get['client_tdptglkadaluarsa'])?></td>
	</tr>
	<tr>
		<th>NPWP</th>
		<td>:&nbsp;<?=$get['client_npwp']?></td>
	</tr>
	<tr>
		<th>Kantor Pelayanan NPWP</th>
		<td>:&nbsp;<?=$get['client_npwpkantor']?></td>
	</tr>
	<tr>
		<th>SPPKP</th>
		<td>:&nbsp;<?=$get['client_npwpsppkp']?></td>
	</tr>
	<tr>
		<th>SKT</th>
		<td>:&nbsp;<?=$get['client_npwpskt']?></td>
	</tr>
	<tr>
		<th>ETPIK</th>
		<td>:&nbsp;<?=$get['client_etpik']?></td>
	</tr>
	<tr>
		<th>Tanggal ETPIK</th>
		<td>:&nbsp;<?=encode_date($get['client_etpiktgl'])?></td>
	</tr>
	<tr>
		<th>Produk ETPIK</th>
		<td>:&nbsp;<?=$get['client_etpikproduk']?></td>
	</tr>
	<tr>
		<th>No. Sertifikat</th>
		<td>:&nbsp;<?=$get['client_sertifikat']?></td>
	</tr>
<?php
for($j=1;$j<=5;$j++):
?>
	<tr>
		<th>IUI/IUT/TDI Data <?=$j?></th>
		<td>:&nbsp;<?=$get['client_iui_jenis_ijin'.$j].'-'.$get['client_primer_jenis_ijin'.$j]?></td>
	</tr>
	<tr>
		<th>Nomor</th>
		<td>:&nbsp;<?=$get['client_nomor_ijin'.$j]?></td>
	</tr>
	<tr>
		<th>Instansi Penerbit</th>
		<td>:&nbsp;<?=$get['client_instansi_ijin'.$j]?></td>
	</tr>
	<tr>
		<th>Tanggal Terbit</th>
		<td>:&nbsp;<?=encode_date($get['client_tglterbit_ijin'.$j])?></td>
	</tr>
	<tr>
		<th>Jenis Produk</th>
		<td>:&nbsp;<?=$get['client_jenisproduk_ijin'.$j]?></td>
	</tr>
<?php endfor;?>
	<tr>
		<th>Client Username</th>
		<td>:&nbsp;<?=$getU['user_name']?></td>
	</tr>
</table>
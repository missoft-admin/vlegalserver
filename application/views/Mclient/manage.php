<script type="text/javascript">
$(document).ready(function(){
<?php
if(!empty($_SESSION['msg'])){
echo $_SESSION['msg'];
$_SESSION['msg']='';
}else{
$_SESSION['msg']='';
}
?>
})
</script>
<?php
$uri=$this->uri->segment(1);
    $uri2=$this->uri->segment(2);
	 	$uri3=$this->uri->segment(3);
// $arrmodules = explode(',',$newmodules);
$a='';
$b='';
if($newstatus==1||$newstatus==''){
	$a='checked';
	$b='';
}else
if($newstatus==0){
	$a='';
	$b='checked';
}
	 	?>
<style type="text/css">
td {
    cursor: pointer;
}
 .editor{
    display: none;
}
</style>
<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>
				<button type="button" data-toggle="tooltip" title="" data-original-title="<?=(!empty($uri3)?'Update':'New')?> {tJudul}"><i class="si si-user"></i></button>
			</li>
            <li>
                <button title="Refresh List"  id="refresh_list" type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i class="si si-refresh"></i></button>
            </li>
			<li class="dropdown">
				<button type="button" data-toggle="dropdown" aria-expanded="false">More <span class="caret"></span></button>
				<ul class="dropdown-menu dropdown-menu-right">
					<?if ($uri2=='new-client'){?>
						<li class="dropdown-header">New {tJudul} </li>
						<li>
							<a tabindex="-1" href="{url_index}">All {tJudul}</a>
						</li>
					<?}else{?>			
						<li class="dropdown-header">All {tJudul} </li>
						<li>
							<a tabindex="-1" href="{url_addnew}">New {tJudul}</a>
						</li>
					<?}?>
				</ul>
			</li>
            
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<form method="post" action="javascript:void(0)">
<input type="hidden" value="<?=(!empty($uri3))?$uri3:''?>" id="hide-ID" name="hide-ID">
<input type="hidden" value="{newclient_propinsi}" id="hide-provinsi" name="hide-provinsi">
<input type="hidden" value="{newclient_kabupaten}" id="hide-kabupaten" name="hide-kabupaten">
<input type="hidden" value="{newclient_userid}" id="hide-userid" name="hide-userid">
<input type="hidden" value='[["","","","","","",""],["","","","","","",""],["","","","","","",""],["","","","","","",""],["","","","","","",""]]' id="hide-jsonTable" name="hide-jsonTable">
		<div class="row">
<div class="col-sm-6">
<div class="block block-bordered">
	<div class="block-header bg-gray-lighter">
		<h3 class="block-title">Data Umum</h3>
	</div>
	<div class="block-content">
<?= ($this->agent->is_mobile())? '<div class="table-responsive">' : '' ?>
		<div class="form-group">
			<label>Nickname</label>
			<input type="text" value="{newclient_nick}" class="form-control" id="new-client_nick" name="new-client_nick" required pattern="[a-zA-Z.-]+">
		</div>
		<div class="form-group">
			<label>Nama</label>
			<input type="text" value="{newclient_nama}" class="form-control" id="new-client_nama" name="new-client_nama" required pattern="[a-zA-Z .-]+">
		</div>
		<div class="form-group">
			<label>Alamat</label>
			<textarea rows="3" class="form-control" name="new-client_alamat" id="new-client_alamat">{newclient_alamat}</textarea>
		</div>
		<div class="form-group">
			<label>Provinsi & Kabupaten</label>
			<div class="row">
				<div class="col-sm-6">
			<select class="form-control new-provinsi col-sm-6" id="new-provinsi" name="new-provinsi" required>
			</select>
			</div>
				<div class="col-sm-6">
			<select class="form-control new-kabupaten col-sm-6" id="new-kabupaten" name="new-kabupaten" placeholder="Pilih Kabupaten Dulu">
			</select>
			</div>
		</div>
		</div>
		<div class="form-group">
			<label>Alamat Pabrik</label>
			<?php
			for($u=1;$u<=5;$u++){
echo '<textarea rows="3" class="form-control" name="new-client_alamatpabrik'.$u.'" id="new-client_alamat" placeholder="Alamat Pabrik '.$u.'">{newclient_alamatpabrik'.$u.'}</textarea>'.br(1);				
					}?>
		</div>
		<div class="form-group">
			<label>Telepon & Fax</label>
			<div class="row">
				<div class="col-sm-6">
			<input type="text" value="{newclient_telp}" class="form-control" id="new-client_telp" name="new-client_telp" required placeholder="Telepon">
			</div>
				<div class="col-sm-6">
			<input type="text" value="{newclient_fax}" class="form-control" id="new-client_fax" name="new-client_fax" required placeholder="Fax">
			</div>
		</div>
		</div>
		<div class="form-group">
			<label>E-mail & Website</label>
			<div class="row">
				<div class="col-sm-6">
			<input type="email" value="{newclient_email}" class="form-control" id="new-client_email" name="new-client_email" required placeholder="myemail@email.com">
			</div>
				<div class="col-sm-6">
			<input type="url" value="{newclient_website}" class="form-control" id="new-client_website" name="new-client_website" placeholder="http://my-website.com">
			</div>
		</div>
		</div>
		<div class="form-group">
			<label>Contact Person</label>
			<div class="row">
				<div class="col-sm-6">
			<input type="text" value="{newclient_namacp}" class="form-control" id="new-client_namacp" name="new-client_namacp" required placeholder="Nama Contact Person">
			</div>
				<div class="col-sm-6">
			<input type="text" value="{newclient_jabatancp}" class="form-control" id="new-client_jabatancp" name="new-client_jabatancp" required placeholder="Jabatan Contact Person">
			</div>
				<div class="col-sm-6">
			<input type="text" value="{newclient_telpcp}" class="form-control" id="new-client_telpcp" name="new-client_telpcp" required placeholder="Telp Contact Person">
			</div>
				<div class="col-sm-6">
			<input type="email" value="{newclient_emailcp}" class="form-control" id="new-client_emailcp" name="new-client_emailcp" required placeholder="E-mail Contact Person">
			</div>
		</div>
		</div>
		<div class="form-group">
			<label>Pengaju Dokumen V-Legal</label>
			<div class="row">
				<div class="col-sm-6">
			<input type="text" value="{newclient_namadv}" class="form-control" id="new-client_namadv" name="new-client_namadv" required placeholder="Nama Pejabat">
			</div>
				<div class="col-sm-6">
			<input type="text" value="{newclient_jabatandv}" class="form-control" id="new-client_jabatandv" name="new-client_jabatandv" required placeholder="Jabatan">
			</div>
				<div class="col-sm-6">
			<input type="text" value="{newclient_telpdv}" class="form-control" id="new-client_telpdv" name="new-client_telpdv" required placeholder="Telp">
			</div>
				<div class="col-sm-6">
			<input type="email" value="{newclient_emaildv}" class="form-control" id="new-client_emaildv" name="new-client_emaildv" required placeholder="E-mail">
			</div>
		</div>
		</div>
		<div class="form-group">
			<label>Username</label>
			<select class="form-control new-username" id="new-username" name="new-username">
			</select>
		</div>
		<div class="form-group">
			<label>Status <sub class="text-danger">*set "NonActive" for disable {dJudul}</sub></label><br>
            <label class="css-input css-radio css-radio-primary push-10-r">
                <input type="radio"<?=$a?> name="new-status" value="1"><span></span> Active
            </label>
            <label class="css-input css-radio css-radio-primary">
                <input type="radio"<?=$b?> name="new-status" value="0"><span></span> NonActive
            </label>
		</div>
		<!-- <div class="pull-left"> -->
		<!-- </div> -->
		<?= ($this->agent->is_mobile())? '</div>' : '' ?>
		<div class="block-footer">&nbsp;</div>
	</div>
</div>
	</div>
<div class="col-sm-6">
<div class="block block-bordered">
	<div class="block-header bg-gray-lighter">
		<h3 class="block-title">Data Perijinan</h3>
	</div>
	<div class="block-content">
<?= ($this->agent->is_mobile())? '<div class="table-responsive">' : '' ?>
		<div class="form-group">
			<label>Akta</label>
			<div class="row">
				<div class="col-sm-6">
			<input type="text" value="{newclient_aktapendirian}" placeholder="Akta Pendirian" class="form-control" id="new-client_aktapendirian" name="new-client_aktapendirian" required>
			</div>
				<div class="col-sm-6">
			<input type="text" value="{newclient_aktaperubahan}" placeholder="Akta Perubahan" class="form-control" id="new-client_aktaperubahan" name="new-client_aktaperubahan" required>
			</div>
		</div>
		</div>
		<div class="form-group">
			<label>SIUP</label>
			<input type="text" value="{newclient_siup}" placeholder="No. SIUP" class="form-control" id="new-client_siup" name="new-client_siup" required>
			<div class="row">
				<div class="col-sm-6">
			<input type="text" value="{newclient_siuptgl}" placeholder="Tanggal SIUP" class="form-control" id="new-client_siuptgl" name="new-client_siuptgl" required>
			</div>
				<div class="col-sm-6">
			<input type="text" value="{newclient_siuptglkadaluarsa}" placeholder="Tanggal Kadaluarsa SIUP" class="form-control" id="new-client_siuptglkadaluarsa" name="new-client_siuptglkadaluarsa" required>
			</div>
		</div>
		</div>
		<div class="form-group">
			<label>TDP</label>
			<input type="text" value="{newclient_tdp}" placeholder="No. TDP" class="form-control" id="new-client_tdp" name="new-client_tdp" required>
			<div class="row">
				<div class="col-sm-6">
			<input type="text" value="{newclient_tdptgl}" placeholder="Tanggal TDP" class="form-control" id="new-client_tdptgl" name="new-client_tdptgl" required>
			</div>
				<div class="col-sm-6">
			<input type="text" value="{newclient_tdptglkadaluarsa}" placeholder="Tanggal Kadaluarsa TDP" class="form-control" id="new-client_tdptglkadaluarsa" name="new-client_tdptglkadaluarsa" required>
			</div>
		</div>
		</div>
		<div class="form-group">
			<label>NPWP</label>
			<input type="text" value="{newclient_npwp}" placeholder="No. NPWP" class="form-control" id="new-client_npwp" name="new-client_npwp" required>
			<input type="text" value="{newclient_npwpkantor}" placeholder="Kantor Pelayanan" class="form-control" id="new-client_npwpkantor" name="new-client_npwpkantor" required>
			<div class="row">
				<div class="col-sm-6">
			<input type="text" value="{newclient_npwpsppkp}" placeholder="SPPKP" class="form-control" id="new-client_npwpsppkp" name="new-client_npwpsppkp" required>
			</div>
				<div class="col-sm-6">
			<input type="text" value="{newclient_npwpskt}" placeholder="SKT" class="form-control" id="new-client_npwpskt" name="new-client_npwpskt" required>
			</div>
		</div>
		</div>
		<div class="form-group">
			<label>ETPIK</label>
			<input type="text" value="{newclient_etpik}" placeholder="No. Etpik" class="form-control" id="new-client_etpik" name="new-client_etpik" required>
			<div class="row">
				<div class="col-sm-6">
			<input type="text" value="{newclient_etpiktgl}" placeholder="Tanggal Terbit" class="form-control" id="new-client_etpiktgl" name="new-client_etpiktgl" required>
			</div>
				<div class="col-sm-6">
			<input type="text" value="{newclient_etpikproduk}" placeholder="Jenis Produk" class="form-control" id="new-client_etpikproduk" name="new-client_etpikproduk" required>
			</div>
		</div>
		</div>
		<div class="form-group">
			<label>No. Sertifikat</label>
			<input type="text" value="{newclient_sertifikat}" class="form-control" id="new-client_sertifikat" name="new-client_sertifikat" required>
		</div>
		</div>

		<?= ($this->agent->is_mobile())? '</div>' : '' ?>
		<div class="block-footer">&nbsp;</div>
	</div>
</div>
</div>
			<button type="submit" style="display: none;" class="btn btn-primary simpan-hidden">Save</button>
	</form>
<table class="table table-stripped table-responsive table-bordered">
	<thead class="bg-primary">
		<tr>
			<th width="6%">Jenis Ijin</th>
			<th width="7%">Tipe Ijin</th>
			<th width="10%">Nomor</th>
			<th width="15%">Instansi Penerbit</th>
			<th width="10%">Tanggal Terbit</th>
			<th width="15%">Jenis Produk</th>
			<th width="5%">Aksi</th>
		</tr>
	</thead>
	<tbody id="tbody-iuiiuttdi">
	<?php
	for($u=1;$u<=5;$u++){
		?>
	<tr>
	<td>
	<span class='span-nama caption' data-id='{hide-ID}'></span>
	<!-- <input type='text' class='iui_jenis_ijin<?=$u?> form-control editor' value='' data-id='{hide-ID}' /> -->
			<select class="iui_jenis_ijin<?=$u?> form-control editor" data-id='{hide-ID}' name="new-iui_jenis_ijin<?=$u?>">
				<option value="">Pilih</option>
				<option value="IUI">IUI</option>
				<option value="IUT">IUT</option>
				<option value="TDI">TDI</option>
			</select>
	</td>
	<td>
	<span class='span-nama caption' data-id='{hide-ID}'></span>
	<!-- <input type='text' class='primer_jenis_ijin<?=$u?> form-control editor' value='' data-id='{hide-ID}' /> -->
			<select class="primer_jenis_ijin<?=$u?> form-control editor" data-id='{hide-ID}' name="new-primer_jenis_ijin<?=$u?>">
				<option value="">Pilih</option>
				<option value="Primer">Primer</option>
				<option value="Lanjutan">Lanjutan</option>
			</select>
	</td>
	<td>
	<span class='span-nama caption' data-id='{hide-ID}'></span>
	<input type='text' class='nomor_ijin<?=$u?> form-control editor' value='' data-id='{hide-ID}' />
	
	</td>
	<td>
	<span class='span-nama caption' data-id='{hide-ID}'></span>
	<input type='text' class='instansi_ijin<?=$u?> form-control editor' value='' data-id='{hide-ID}' />
	
	</td>
	<td>
	<span class='span-nama caption' data-id='{hide-ID}'></span>
	<input type='text' class='tglterbit_ijin<?=$u?> form-control editor tgleditor' id='tgleditor' value='' data-id='{hide-ID}' />
	
	</td>
	<td>
	<span class='span-nama caption' data-id='{hide-ID}'></span>
	<input type='text' class='jenisproduk_ijin<?=$u?> form-control editor' value='' data-id='{hide-ID}' />
	
	</td>
	<td>
	<a href="javascript:void(0)" class="btn btn-xs btn-info edit-row" title="Edit <?=$this->label?>">
                <i class="far fa-edit"></i>
                </a><a href="javascript:void(0)" class="btn btn-xs btn-danger delete-row" data-id="{hide-ID}" title="Delete <?=$this->label?>"><i class="far fa-trash-alt"></i></a>
	</td>
	</tr>
	<?php
		}
	?>
	</tbody>
</table>
			<button type="button" class="btn btn-primary simpan-form">Save</button>
</div>
		
		<div class="block-footer">&nbsp;</div>
	</div>
<script type="text/javascript">
$(function(){
$hideProvinsi 	= $('#hide-provinsi').val().split(',');
$hideKabupaten 	= $('#hide-kabupaten').val().split(',');
$hideUsers 		= $('#hide-userid').val().split(',');
$provinsi  		= $('.new-provinsi');
$kabupaten  	= $('.new-kabupaten');
$users  		= $('.new-username');

$(window).on('load',function() {
	getIuiiuttdi()
	})
    $(document).ready(function() {
getDatePicker('#new-client_siuptgl')
getDatePicker('#new-client_siuptglkadaluarsa')
getDatePicker('#new-client_tdptgl')
getDatePicker('#new-client_tdptglkadaluarsa')
getDatePicker('#new-client_etpiktgl')
getProvinsi();
getUsers();
getKabupaten($('#hide-provinsi').val())
$provinsi.change(function(){
$kabupaten.css('width','100%')
var a=$('.new-provinsi option:selected').val().split(',');
$('#hide-provinsi').val(a[1])
getKabupaten(a[0])
})
$kabupaten.change(function(){
var a=$('.new-kabupaten option:selected').val().split(',');
$('#hide-kabupaten').val(a[1])
})
$users.change(function(){
var a=$('.new-username option:selected').val();
$('#hide-userid').val(a)
})

$(document).on("click","td a.edit-row",function(){
	$tr=$(this).closest('tr');
$tr.find("span[class~='caption']").hide(); 
$tr.find("input[class~='editor']").fadeIn().focus();
$tr.find("select[class~='editor']").fadeIn().focus();
$tr.find("textarea[class~='editor']").fadeIn().focus();
});
$(document).on("click","td a.delete-row",function(){
	$tr=$(this).closest('tr');
$tr.find("span[class~='caption']").html(''); 
$tr.find("input[class~='editor']").val('');
$tr.find("select[class~='editor']").val('');
$tr.find("textarea[class~='editor']").val('');
$tr.find("span[class~='caption']").fadeIn(); 
$tr.find("input[class~='editor']").hide();
$tr.find("select[class~='editor']").hide();
$tr.find("textarea[class~='editor']").hide();
});
$('form').submit(function(){
var formD = $('form').serialize();
$.ajax({
type: 'POST',
url: '{url_proses}',
data: formD,
dataType: 'script',
success:function(script){
// $('input').val('')
// $('span').html('')
// $('textarea').val('')
// getProvinsi();
// getUsers();
// getKabupaten($('#hide-provinsi').val())
	// return script[0];
	return script[1];
            setTimeout(function() {
                }, 2000);
}

	})
})

$(document).on("click",".simpan-form",function(){
var formD = $('form').serialize();
	$('.simpan-hidden').click(); 
})
$(document).on("click","td",function(){
$(this).find("span[class~='caption']").hide(); 
$(this).find("input[class~='editor']").fadeIn().focus();
$(this).find("select[class~='editor']").fadeIn().focus();
$(this).find("textarea[class~='editor']").fadeIn().focus();
getDatePicker('.tgleditor')
});

$(document).on("change",".tgleditor",function(e){
var target=$(e.target);
var value=target.val();
var id=target.attr("data-id");
var modul="";
for(var t=1;t<=5;t++){
	if(target.is(".tglterbit_ijin"+t)){
modul +="client_tglterbit_ijin"+t; 
	$(".jenisproduk_ijin"+t).focus()
}
}
        var dTable = $('table tbody#tbody-iuiiuttdi tr').get().map(function(row) {
          return $(row).find('td').get().map(function(cell) {
            return $(cell).find('span').text();
            // return $(cell).find('input').val();
          });
        });

        var a=$("#hide-jsonTable").val(JSON.stringify(dTable));
	     target.hide();
	     target.siblings("span[class~='caption']").html(value).fadeIn();
ToastrSukses("Data Perijinan <?=$this->label?> telah ditambahkan","Info")
})
$(document).on("change","select.editor",function(e){
var target=$(e.target);
var value=target.val();
var id=target.attr("data-id");
var modul="";
for(var t=1;t<=5;t++){
if(target.is(".iui_jenis_ijin"+t)){
modul +="client_iui_jenis_ijin"+t;
	$(".primer_jenis_ijin"+t).focus()
}else if(target.is(".primer_jenis_ijin"+t)){
modul +="client_primer_jenis_ijin"+t; 
	$(".nomor_ijin"+t).focus()
}
}
        var dTable = $('table tbody#tbody-iuiiuttdi tr').get().map(function(row) {
          return $(row).find('td').get().map(function(cell) {
            return $(cell).find('span').text();
            // return $(cell).find('input').val();
          });
        });

        var a=$("#hide-jsonTable").val(JSON.stringify(dTable));
	     target.hide();
	     target.siblings("span[class~='caption']").html(value).fadeIn();
ToastrSukses("Data Perijinan <?=$this->label?> telah ditambahkan","Info")
})
$(document).on("keypress",".editor",function(e){
if(e.keyCode==13){
var target=$(e.target);
var value=target.val();
var id=target.attr("data-id");
var modul="";
for(var t=1;t<=5;t++){
if(target.is(".nomor_ijin"+t)){
modul +="client_nomor_ijin"+t; 
	$(".instansi_ijin"+t).focus()
}else if(target.is(".instansi_ijin"+t)){
modul +="client_instansi_ijin"+t; 
	$(".tglterbit_ijin"+t).focus()
}else if(target.is(".jenisproduk_ijin"+t)){
modul +="client_jenisproduk_ijin"+t;
}
}
        var dTable = $('table tbody#tbody-iuiiuttdi tr').get().map(function(row) {
          return $(row).find('td').get().map(function(cell) {
            return $(cell).find('span').text();
            // return $(cell).find('input').val();
          });
        });

        var a=$("#hide-jsonTable").val(JSON.stringify(dTable));
	     target.hide();
	     target.siblings("span[class~='caption']").html(value).fadeIn();
ToastrSukses("Data Perijinan <?=$this->label?> telah ditambahkan","Info")
}
});
// end document
	});

function ajaxPush(target,id,value,modul){
	if(value!=''){
	$.ajax({
	    url:"{site_url}setting/upRow",
	    data:{"id":id,"value":value,"modul":modul},
	    dataType:'script',
	    success: function(a){
	return a;
	    }
	});}
}

function getProvinsi($kode){
$provinsi.select2({
    // minimumInputLength: 1,
    tags: false,
    triggerChange: true,
   placeholder: "-- Pilih Provinsi --",
    ajax: {
        url: '{json_provinsi}',
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (params) {
      var query = {
        search: params.term,
        type: 'public',
        kode: $kode
        // id: $('#hide-provinsi').val()
      }
      return query;
        },
        processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.nama,
                        id: item.kode+','+item.propid
                    }
                })
            };
        }
    },
    })
      var teks = $hideProvinsi[1];
      // Set the value, creating a new option if necessary
      if ($provinsi.find("option[value='" + teks + "']").length) {
        $provinsi.val(teks).trigger("change");
      } else { 
        // Create the DOM option that is pre-selected by default
        var teks = new Option($hideProvinsi[1], $hideProvinsi[0], true, true);
        // Append it to the select
        $provinsi.append(teks).trigger('change');
      } }

function getKabupaten($kode){
$kabupaten.select2({
    placeholder: "-- Pilih Provinsi Dulu --",
    tags: false,
    triggerChange: true,
    ajax: {
        url: '{json_kabupaten}',
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (params) {
      var query = {
        search: params.term,
        type: 'public',
        id: $('#hide-kabupaten').val(),
        kode: $kode
      }
      return query;
        },
        processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.nama,
                        id: item.kode+','+item.kabid
                    }
                })
            };
        }
    },
    })
      var teks = $hideKabupaten[1];
      // Set the value, creating a new option if necessary
      if ($kabupaten.find("option[value='" + teks + "']").length) {
        $kabupaten.val(teks).trigger("change");
      } else { 
        // Create the DOM option that is pre-selected by default
        var teks = new Option($hideKabupaten[1], $hideKabupaten[0], true, true);
        // Append it to the select
        $kabupaten.append(teks).trigger('change');
      } }

function getUsers(){
$users.select2({
    placeholder: "-- Pilih Member --",
    tags: false,
    triggerChange: true,
    ajax: {
        url: '{json_users}',
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (params) {
      var query = {
        search: params.term,
        type: 'public'
      }
      return query;
        },
        processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.name+' ['+item.user_name+']',
                        id: item.user_id
                    }
                })
            };
        }
    },
    })
      var teks = $hideUsers[1];
      // Set the value, creating a new option if necessary
      if ($users.find("option[value='" + teks + "']").length) {
        $users.val(teks).trigger("change");
      } else { 
        // Create the DOM option that is pre-selected by default
        var teks = new Option($hideUsers[1], $hideUsers[0], true, true);
        // Append it to the select
        $users.append(teks).trigger('change');
      }   
  }
function getIuiiuttdi(){
var $me  	=$('#tbody-iuiiuttdi'),
	content,
	idclient=$('#hide-ID');
	$.ajax({
		url:'{json_table}',
		data:{"id":idclient.val()},
		dataType:'json',
		// type:'get',
                method:"get", 
		beforeSend:function(){
// $me.append(`<tr align="center">
// 			<td colspan="6"><i>Tunggu..</i></td>
// 			</tr>`);
// $('#tgleditor').removeClass('tgleditor');
		},
		success:function(data){
// $me.empty()
var a=i+1,show=data.client,o=0,r;
			if(!data.client){
$me.html(`<tr align="center">
<td colspan="6"><i>Tidak ada data</i></td>
</tr>`);
}
$me.empty()
			for(var z=0;z<5;z++){
switch(show[z].iui_jenis_ijin){
	case 'IUI':
		$iui='selected';
		$iut='';
		$tdi='';
	break;
	case 'IUT':
		$iui='';
		$iut='selected';
		$tdi='';
	break;
	case 'TDI':
		$iui='';
		$iut='';
		$tdi='selected';
	break;
	default:
		$iui='';
		$iut='';
		$tdi='';
		break;

}
switch(show[z].primer_jenis_ijin){
	case 'Primer':
		$pri='selected';
		$lan='';
	break;
	case 'Lanjutan':
		$pri='';
		$lan='selected';
	break;
	default:
		$pri='';
		$lan='';
		break;

}
o=z+1;
	content +=`<tr>
	<td>
	<span class='span-nama caption' data-id='`+show[z].id+`'>`+show[z].iui_jenis_ijin+`</span>
			<select class="iui_jenis_ijin`+o+` form-control editor" data-id='`+show[z].id+`' name="new-iui_jenis_ijin`+o+`">
				<option value="">Pilih</option>
				<option `+$iui+` value="IUI">IUI</option>
				<option `+$iut+` value="IUT">IUT</option>
				<option `+$tdi+` value="TDI">TDI</option>
			</select>
	</td>
	<td>
	<span class='span-nama caption' data-id='`+show[z].id+`'>`+show[z].primer_jenis_ijin+`</span>
			<select class="primer_jenis_ijin`+o+` form-control editor" data-id='`+show[z].id+`' name="new-primer_jenis_ijin`+o+`">
				<option value="">Pilih</option>
				<option `+$pri+` value="Primer">Primer</option>
				<option `+$lan+` value="Lanjutan">Lanjutan</option>
			</select>
	</td>
	<td>
	<span class='span-nama caption' data-id='`+show[z].id+`'>`+show[z].nomor_ijin+`</span>
	<input type='text' class='nomor_ijin`+o+` form-control editor' value='`+show[z].nomor_ijin+`' data-id='`+show[z].id+`' />
	
	</td>
	<td>
	<span class='span-nama caption' data-id='`+show[z].id+`'>`+show[z].instansi_ijin+`</span>
	<input type='text' class='instansi_ijin`+o+` form-control editor' value='`+show[z].instansi_ijin+`' data-id='`+show[z].id+`' />
	
	</td>
	<td>
	<span class='span-nama caption' data-id='`+show[z].id+`'>`+show[z].tglterbit_ijin+`</span>
	<input type='text' class='tglterbit_ijin`+o+` form-control editor tgleditor' id='tgleditor' value='`+show[z].tglterbit_ijin+`' data-id='`+show[z].id+`' />
	
	</td>
	<td>
	<span class='span-nama caption' data-id='`+show[z].id+`'>`+show[z].jenisproduk_ijin+`</span>
	<input type='text' class='jenisproduk_ijin`+o+` form-control editor' value='`+show[z].jenisproduk_ijin+`' data-id='`+show[z].id+`' />
	
	</td>
	<td>
	<a href="javascript:void(0)" class="btn btn-xs btn-info edit-row" title="Edit <?=$this->label?>">
                <i class="far fa-edit"></i>
                </a><a href="javascript:void(0)" class="btn btn-xs btn-danger delete-row" data-id="`+show[z].id+`" title="Delete <?=$this->label?>"><i class="far fa-trash-alt"></i></a>
	</td>
	</tr>`;
		}
			$me.html(content)
		}
	}).done(function(data, textStatus, jqXHR) {
		
	});
}

})
</script>


<?php

function backend_info()
{
    $CI =& get_instance();
    $data = array(
        'site_url'             => site_url(),
        'base_url'             => base_url(),
        'assets_path'          => base_url().'assets/',
        'css_path'             => base_url().'assets/css/',
        'fonts_path'           => base_url().'assets/fonts/',
        'img_path'             => base_url().'assets/img/',
        'js_path'              => base_url().'assets/js/',
        'plugins_path'         => base_url().'assets/js/plugins/',
        'upload_path'          => base_url().'assets/upload/',
        'ajax'                 => base_url().'assets/ajax/',
        'toastr_css'           => base_url().'assets/toastr/toastr.min.css',
        'toastr_js'            => base_url().'assets/toastr/toastr.min.js',

        'user_id'              => $CI->session->userdata('user_id'),
        'user_avatar'          => $CI->session->userdata('user_avatar'),
        'username'            => $CI->session->userdata('user_name'),
        'user_permission'      => $CI->session->userdata('user_permission'),
        'user_last_login'      => human_date($CI->session->userdata('user_last_login')),

        'title_web'            => 'V-Legal PT TUV Rheinland Indonesia',
        'favicon_url'          => base_url().'assets/img/favicons/ico.png'
       
    );
    return $data;
}

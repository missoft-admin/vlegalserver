<?php

    function get_kode($kode = 'KODE', $namakode, $tbl, $str_length=null)
    {
        $CI =& get_instance();
        $CI->db->like($namakode, $kode, 'after');
        $CI->db->from($tbl);
        $query = $CI->db->count_all_results();

        if ($query > 0) {
            $autono = $query + 1;
            $autono = $kode.str_pad($autono, $str_length, '0', STR_PAD_LEFT);
        } else {
            $left = "";
            for ($i=1; $i<=$str_length; $i++) {
                $left .= "0";
            }
            $left = substr($left, 0, $str_length-1)."1";
            $autono = $kode.$left;
        }
        return $autono;
    }

    function createKode($table,$id,$namakode){
        $CI =& get_instance();
        $CI->db->select('RIGHT(t1.'.$id.',4) as kode', FALSE)
                ->order_by($id,'DESC')
                ->limit(1);
          $query = $CI->db->get($table.' t1');
          if($query->num_rows() <> 0){
           $data =$query->row();      
           $kode =intval($data->kode) + 1;    
          }
          else {
           $kode =1;
          }
          $kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT);
          $kodejadi = $namakode.date("ymd").$kodemax;
          return $kodejadi;  
    }

    function getDoubleWhere($kondisi1,$where1,$kondisi2,$where2,$table){      
        $CI =& get_instance();
        return $CI->db->where($kondisi1,$where1)
                      ->where($kondisi2,$where2)
                      ->get($table.' t1');
    }

    function getTripleWhere($kondisi1,$where1,$kondisi2,$where2,$kondisi3,$where3,$table){      
        $CI =& get_instance();
        return $CI->db->where($kondisi1,$where1)
                      ->where($kondisi2,$where2)
                      ->where($kondisi3,$where3)
                      ->get($table.' t1');
    }

    function get_by_field($fieldkey, $fieldval, $tbl)
    {
        $CI =& get_instance();
        $CI->db->where($fieldkey, $fieldval);
        $query = $CI->db->get($tbl);
        return $query->row();
    }

    function rowWhere($kondisi,$where,$table){      
        $CI =& get_instance();
        return $CI->db->where($kondisi,$where)
                        ->get($table)
                        ->row();
    }

    function rowArray($table,$where=array()){      
        $CI =& get_instance();
        if(!empty($where)){
            $CI->db->where($where);
        }
        return $CI->db->get($table)
                        ->row_array();
    }

    function resultWhere($kondisi,$where,$table){      
        $CI =& get_instance();
        return $CI->db->where($kondisi,$where)
                        ->get($table)
                        ->result();
    }

    function resultArray($table,$where=array()){      
        $CI =& get_instance();
        if(!empty($where)){
            $CI->db->where($where);
        }
        return $CI->db->get($table)
                        ->result_array();
    }

    function getLike($like,$table,$select='',$where='',$limit=''){      
        $CI =& get_instance();
        if(!empty($select)){
                $CI->db->select($select);
            }
        if(!empty($limit)){
                $CI->db->limit($limit,0);
            }
        if(!empty($where)){
                $CI->db->where($where);
            }
        return $CI->db->like($like)
                      ->get($table)
                      ->result();
    }

    function getSelect($table,$select='',$where=''){      
        $CI =& get_instance();
        if(!empty($select)){
                $CI->db->select($select);
            }
        if(!empty($where)){
                $CI->db->where($where);
            }
        return $CI->db->get($table)
                      ->result_array();
    }

    function getwhere($kondisi,$where,$table){      
        $CI =& get_instance();
        return $CI->db->where($kondisi,$where)
                        ->get($table.' t1');
    }

    function getwherejoin($kondisi,$where,$table,$table2,$join){
        $CI =& get_instance();
        return $CI->db->select('*')
                        ->where($kondisi,$where)
                        ->join($table2,$join)
                        ->get($table);
    }

    function getlikejoin($kondisi,$like,$table,$table2,$join){
        $CI =& get_instance();
        return $CI->db->select('*')
                        ->like($kondisi,$like)
                        ->join($table2,$join)
                        ->get($table);
    }

    function getData($table){
        $CI =& get_instance();
        return $CI->db->get($table)->result();
    }
    function get1Data($table){      
        $CI =& get_instance();
        return $CI->db->get($table)->row();
    }
    function getLastID($table,$sID,$where=''){      
        $CI =& get_instance();
        if(!empty($where)){
                $CI->db->where($where);
            }
        return $CI->db->select($sID)
                      ->order_by($sID,"desc")
                      ->limit(1)
                      ->get($table)
                      ->row_array();
    }

    function get_all($tbl, $where = array(), $order_by = null)
    {
        $CI =& get_instance();
        $CI->db->where($where);
        if ($order_by!==null) {
            $CI->db->order_by($order_by, 'DESC');
        }
        $query = $CI->db->get($tbl);
        return $query->result();
    }

// Manipulasi database
    function ReplaceData($where,$data,$table){
        $CI =& get_instance();
        return $CI->db->where($where)->replace($table, $data);
    }

    function deleteData($where,$table) {
        $CI =& get_instance();
        return $CI->db->where($where)
                      ->delete($table);  
    }

    function saveData($table,$data) {
        $CI =& get_instance();
        return $CI->db->insert($table, $data);  
    }

    function updateData($where,$data,$table){
        $CI =& get_instance();
        return $CI->db->where($where)
                    ->update($table,$data);
    }

    function updateRow($where,$value,$modul){
    //     $CI =& get_instance();
    //     return $CI->db->where($where)
    //                 ->update($table,$data);
    // $this->db->where($where);
    // return $this->db->update("___menu",array($modul=>$value));
  } 
<?php

function menuIsOpen($menu)
{
    $CI =& get_instance();
    return ($CI->uri->segment(1) == $menu) ? "class=\"open\"" : "";
}
function menuIsActive($menu)
{
    $CI =& get_instance();
    return ($CI->uri->segment(2) == $menu) ? "class=\"active\"" : "";
}
function BackendBreadcrum($data)
{
    $result = array();
    foreach ($data as $row) {
        if ($row[1] == '#') {
            $url = '<li><a class="text-muted" href="#">'.$row[0].'</a></li>';
        } elseif ($row[1] == '') {
            $url = '<li><a class="link-effect" href="#">'.$row[0].'</a></li>';
        } else {
            $url = '<li><a class="link-effect" href="'.site_url($row[1]).'">'.$row[0].'</a></li>';
        }
        array_push($result, $url);
    }

    return implode('', $result);
}
function hapus_koma($angka,$delimiter = ","){
	return str_replace($delimiter,"",$angka);
}
function get_tgl_akhir($thn,$bln){
	$bulan[1]='31';
	$bulan[2]='28';
	$bulan[3]='31';
	$bulan[4]='30';
	$bulan[5]='31';
	$bulan[6]='30';
	$bulan[7]='31';
	$bulan[8]='31';
	$bulan[9]='30';
	$bulan[10]='31';
	$bulan[11]='30';
	$bulan[12]='31';

	if ($thn%4==0){
	$bulan[2]=29;
	}
	return $bulan[$bln];
}
function front_breadcrum($data)
{
	$result = array();
	foreach($data as $row){	
		if($row[1] == '#'){ 
			$url = anchor(current_url().'#',$row[0]);
		}elseif($row[1] == ''){
			$url = $row[0];
		}else{
			$url = anchor(site_url($row[1]),$row[0]);
		}			
		
		$url = '<li>'.$url.'</li>';	
		array_push($result,$url);
	}
	return implode("", $result);
}

/* END Breadcrum Function */

/* Breadcrum Function */

function admin_breadcrum($data)
{
	$result = array();
	foreach($data as $row){	
		if($row[1] == '#'){ 
			$url = '<li><a href="#">'.$row[0].'</a></li><span class="divider">&gt;</span>';
		}elseif($row[1] == ''){
			$url = '<li class="active">'.$row[0].'</li>';
		}else{
			$url = '<li><a href="'.site_url($row[1]).'">'.$row[0].'</a></li><span class="divider">&gt;</span>';
		}			
		array_push($result,$url);
	}
	return implode("", $result);
}

/* END Breadcrum Function */

/* CMS Function  */

function display_permission($perm)
{
	$permission = array('0' => 'Private','1' => 'Public');
	return  $permission[$perm];
}

function display_publish($perm)
{
	$permission = array('0' => 'Unpublish','1' => 'Published');
	return  $permission[$perm];
}

/* END CMS Function  */

/* DATE Function */

function tsi_date($date) 
{
	$date = date_format(date_create($date),'d M Y');
	return $date;
}

function tsi_long_date($date) 
{
	$date = date_format(date_create($date),'d M Y H:t:s');
	return $date;
}

function encode_date($date) 
{
	$date = date_format(date_create($date),'d-m-Y');
	return $date;
}

function encode_long_date($date) 
{
	$date = date_format(date_create($date),'d-m-Y H:t:s');
	return $date;
}

function decode_date($date) 
{
	$date = date_format(date_create($date),'Y-m-d');
	return $date;
}

function decode_long_date($date) 
{
	$date = date_format(date_create($date),'Y-m-d H:t:s');
	return $date;
}

function human_date($date) 
{
	$date = date("j F Y",strtotime($date));
	return $date;
}

function human_date_audit($date) 
{
	if($date == "0000-00-00" || $date == null){
		$date = "-";
	}else{
		$date = date("d/m/Y",strtotime($date));
	}
	return $date;
}

function human_date_leader($date) 
{
	$date = date("d F Y",strtotime($date));
	return $date;
}

function event_date($date) 
{
	$date = date("d M",strtotime($date));
	return $date;
}

function bulan_date($date) 
{
	$date = date("F Y",strtotime($date));
	return $date;
}

function tahun_date($date) 
{
	$date = date("Y",strtotime($date));
	return $date;
}

function home_date() 
{
	$date = date("l, j F Y");
	return $date;
}

function human_date_short($date) 
{
	$date = date("d-m-Y",strtotime($date));
	return $date;
}

function human_date_mid($date) 
{
	$date = date("j M Y",strtotime($date));
	return $date;
}

function human_date_time($date) 
{
	$date = date("d-m-Y H:i:s ",strtotime($date));
	return $date;
}

function sitemap_date($date) 
{
	$date = date("Y-m-d",strtotime($date));
	$time = date("h:i:s+07:00",strtotime($date));
	return $date."T".$time;
}

function last_login_date($date) 
{
	if($date != '' && $date != '0000-00-00 00:00:00'){
		$date = date("j F Y h:i:s",strtotime($date));
	}else{
		$date = 'First Login';
	}
	return $date;
}

function human_date_full($date) 
{
	$date = date("l, j F Y",strtotime($date));
	return $date;
}

function human_date_admin($date) 
{
	$date = date("d-m-Y h:i",strtotime($date));
	return $date;
}

/* END DATE Function */


/* Permission Function */

function LastLoginDate($date)
{
    if ($date != '' && $date != '0000-00-00 00:00:00') {
        $date = date('j F Y h:i:s', strtotime($date));
    } else {
        $date = 'First Login';
    }

    return $date;
}

function PermissionUserLoggedIn($session)
{
    if (!$session->userdata('logged')) {
        $session->set_flashdata('error', true);
        $session->set_flashdata('message_flash', 'Access Denied');
        redirect('SignIn');
    }
}

function PermissionUserLogin($session)
{
    if ($session->userdata('logged_in')) {
        $session->set_flashdata('error', true);
        $session->set_flashdata('message_flash', 'Access Denied');
        redirect('dashboard');
    }
}

function ErrorSuccess($session)
{
    if ($session->flashdata('error')) {
        return ErrorMessage($session->flashdata('message_flash'));
    } elseif ($session->flashdata('confirm')) {
        return SuccesMessage($session->flashdata('message_flash'));
    } else {
        return '';
    }
}

function ErrorMessage($message)
{
    return '<div class="alert alert-warning alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h3 class="font-w300 push-15">Perhatian!</h3>
        <p>'.$message.'</p>
      </div>';
}

function SuccesMessage($message)
{
    return '<div class="alert alert-success alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h3 class="font-w300 push-15">Berhasil!</h3>
      <p>'.$message.'</p>
    </div>';
}


function type_user($level)
{	
	if($level !=''){
		$userlevel = array(	
					"SA" => "Super Admin",
					"A" => "Administrator",
					"I" => "Auditor",
					"E" => "Editor",
					"S" => "SER",
					"M" => "Manager Region",
					"B" => "Branch Manager",
					"P" => "Petugas Pusat Pertamina",
					);
		return $userlevel[$level];
	}else{
		return '';
	}
}

/* END Permission Function */

/* Paging Function */

function paging_front($row,$url,$urisegment=3,$perpage=10){
	$config = array();
	$config['base_url'] = site_url($url);
	$config['uri_segment'] = $urisegment;
	$config['total_rows'] = $row;
	$config['per_page'] = $perpage; 
	$config['first_link'] = false;
	$config['next_link'] = '<span class="linkbox">'.lang('next').'</span>';
	$config['prev_link'] = '<span class="linkbox">'.lang('prev').'</span>';
	$config['num_tag_open'] = '<span class="linkbox">';
	$config['num_tag_close'] = '</span>';
	$config['cur_tag_open'] = '<span class="linkbox">';
	$config['cur_tag_close'] = '</span>';
	return $config;
}
function paging_admin_asal($row,$url,$uri_segment = 4,$per_page = 5){
	$config = array();
	$config['base_url'] = site_url($url);
	$config['uri_segment'] = $uri_segment;
	$config['total_rows'] = $row;
	$config['per_page'] = $per_page; 	
	$config['num_links'] = 5;
	$config['full_tag_open'] = '<div class="dataTables_paginate paging_full_numbers" >';
	$config['full_tag_close'] = '</div>';
	$config['first_link'] = 'First';
	$config['last_link'] = 'Last';
	$config['next_link'] = 'Next';
	$config['prev_link'] = 'Previous';
	$config['cur_tag_open'] = '<a class="paginate_active">';
	$config['cur_tag_close'] = '</a>';
	$config['anchor_class'] = 'class="paginate_button"';
	return $config;
}
function paging_admin($row,$url,$uri_segment = 4,$per_page = 5){
	$config = array();
	$config['base_url'] = site_url($url);
	$config['uri_segment'] = $uri_segment;
	$config['total_rows'] = $row;
	$config['per_page'] = $per_page; 	
	$config['num_links'] = 5;
	$config['full_tag_open'] = '<div class="btn-group">';
	$config['full_tag_close'] = '</div>';
	$config['first_link'] = 'First';
	$config['last_link'] = 'Last';
	$config['next_link'] = 'Next';
	$config['prev_link'] = 'Prev';
	$config['cur_tag_open'] = '<button type="button" class="btn ls-light-blue-btn">';
	$config['cur_tag_close'] = '</button>';
	$config['anchor_class'] = 'class="btn btn-default"';
	return $config;
}
/* Paging Function */

/* WYSIWYG Function */

function replace_image_url($content){
	$isi=str_replace('../../','',$content);
	$isi = str_replace(' src="./images', ' src="'.base_url().'images', $isi);
	return $isi;
}

function replace_image_url2($content){
	$isi=str_replace('../','',$content);
	$isi = str_replace(' src="images', ' src="'.base_url().'images', $isi);
	return $isi;
}

/* END WYSIWYG Function */


/*Admin Error Message */
function error_success($session){
	if($session->flashdata('error')){
		return error_message($session->flashdata('message_flash'));
	}elseif($session->flashdata('confirm')){
		return succes_message($session->flashdata('message_flash'));
	}else{
		return '';
	}
}
function error_success_tsi($session){
	if($session->flashdata('error')){
		return error_message_tsi($session->flashdata('message_flash'));
	}elseif($session->flashdata('confirm')){
		return succes_message_tsi($session->flashdata('message_flash'));
	}else{
		return '';
	}
}
function error_message_tsi($message){
   return '<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<strong> Warning ! </strong>'.$message.'</div>';
}

function succes_message_tsi($message){
	 return '<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<strong> Informasi ! </strong>'.$message.'</div>';
}
function error_message($message){
   
	return '<div class="nNote nFailure">
				<p>'.$message.'</p>
			</div>';
}

function succes_message($message){
	return '<div class="nNote nSuccess">
				<p>'.$message.'</p>
			</div>';
}

function error_success_front($session){
	if($session->flashdata('error')){
		return error_message_front($session->flashdata('message_flash'));
	}elseif($session->flashdata('confirm')){
		return succes_message_front($session->flashdata('message_flash'));
	}else{
		return '';
	}
}

function error_message_front($message){

	return '<div class="alert alert-error">
				<h4><strong>Error notification:</strong></h4>'.$message.'
			</div>';
}

function succes_message_front($message){
	return '<div class="alert alert-success">
				<h4><strong>Success notification:</strong></h4>'.$message.'
			</div>';
}


/* helper checklist */

function convert_score_text($str)
{
	$hasil = $str;
	if($str == "A/B/C/D/E/F"){
		$hasil = "A-F";
	}elseif($str == "A/B/C/D/E/F/X"){
		$hasil = "A-F/X";
	}
	
	return $hasil;

}

function format_score($angkat){
	return number_format($angkat,2);
}


function format_angka($angkat){
	return number_format($angkat,0,",",".");
}

function text_tambahan_3_2()
{
	return "Fasilitas \"SPBU\" Retail Outlet berikut sudah dibersihkan secara dwi-bulanan serta dipelihara dengan demikian tidak tampak kerusakan, seluruh penerangan berfungsi baik, tak ada tanda bekas coretan, tak ada cat rusak atau panel yang bergeser  karena hilangnya baut";
}

function type_spbu($level)
{	
	if($level !=''){
		$spbu = array(	
					"1" => "DE-CERTIFIED",
					"2" => "SILVER",
					"3" => "GOLD",
					"4" => "DIAMOND",
					);
		return $spbu[$level];
	}else{
		return '';
	}
}

function convert_month($num){
    switch($num){
        case "01":
            return "Januari";
            break;
        case "02":
            return "Februari";
            break;
        case "03":
            return "Maret";
            break;
        case "04":
            return "April";
            break;
        case "05":
            return "Mei";
            break;
        case "06":
            return "Juni";
            break;
        case "07":
            return "Juli";
            break;
        case "08":
            return "Agustus";
            break;
        case "09":
            return "September";
            break;
        case "10":
            return "Oktober";
            break;
        case "11":
            return "November";
            break;
        case "12":
            return "Desember";
            break;
    }
}

function site_themes() {
	$sql = mysql_query("select themes from z_site_setting ")or die (mysql_error());
	$rest = mysql_fetch_array($sql);
	return $rest['themes'];
}

function themes() {
	global $system_folder,$application_folder;
	$themes = site_url().''.APPPATH.'views/themes/'.site_themes().'/';
	return $themes;
}

function get_personal($periode,$noid,$db){
	$db->select('personal');
	$db->from('mkomisi');
	$db->where('periode',$periode);
	$db->where('noid',$noid);
	$query = $db->get();
	$row = $query->row();
	if($query->num_rows() < 1 || $row->personal ==0){return "<font color='red'>0</font>";}else{return "<font color='blue'>".$row->personal."</font>";}
}	
	
function ec_pb($noterima, $tglterima, $query){
	$mailcontent='<p>Yth Admin<br />Terdapat <strong>Penerimaan Barang</strong> dengan data:<br /></p><table border="0" cellspacing="2" cellpadding="2" style="margin-left:40px"><tr><td><strong>Data Penerimaan</strong></td><td>&nbsp;</td></tr>';
	$mailcontent.='<tr><td width="187">No Penerimaan</td><td width="400">: &nbsp;&nbsp; '.$noterima.'</td></tr><tr><td>Tanggal Input</td><td>: &nbsp;&nbsp; '.$tglterima.'</td></tr><tr><td>&nbsp;</td></tr></table><table border="1" cellspacing="2" cellpadding="2" style="margin-left:40px; text-align:center;"><tr><td width="50">No</td><td width="250">Nama Barang</td><td width="200">Jumlah</td><td width="200"><strong>Stok Update</strong></td></tr>';
	$no=1;
	foreach ($query as $row){
	$mailcontent.='<tr><td>'.$no.'.</td><td>'.$row->namabarang.'</td><td>'.$row->jumlah.'</td><td><strong>'.$row->stok.'</strong></td></tr>';
	$no++;
	}
	$mailcontent.='</table><p>  Salam,<br />Banyu PS</p>';		
	return $mailcontent; 
}

function opt_month($cur) {
	$hasil='';
	for ($i=1;$i<13;$i++){
		$temp=substr("00", 0,2-strlen($i)).$i;
		if ($cur==$temp){
		$hasil.="<option value=\"$temp\" selected=\"SELECTED\" class='ayrsingle'>".get_month_name($temp)."</option>";
		}else{
		$hasil.="<option value=\"$temp\" class='ayrsingle'>".get_month_name($temp)."</option>";
		}
	}
	return $hasil;
}
function get_month_name($cur,$initial=0) {
	switch ($cur)
	{
		case '01':
			$list="Januari";
			break;
		case '02':
			$list="Februari";
			break;
		case '03':
			$list="Maret";
			break;
		case '04':
			$list="April";
			break;
		case '05':
			$list="Mei";
			break;
		case '06':
			$list="Juni";
			break;
		case '07':
			$list="Juli";
			break;
		case '08':
			$list="Agustus";
			break;
		case '09':
			$list="September";
			break;
		case '10':
			$list="Oktober";
			break;
		case '11':
			$list="November";
			break;
		case '12':
			$list="Desember";
			break;
	}
	if ($initial){if ($cur=='08'){$list='Ags';}else{$list=substr($list,0,3);}}
	return $list;
}

function get_membersjual($noidbuy,$db){
	$db->select('mmembersjual.noidbuy,mmembersjual.noidbuyreal,mmembersjual.noid,mmembers.namamembers,mmembersjual.nojual,ttransjualhead.tgljual,mmembersjual.point');
	$db->from('mmembersjual');
	$db->join("mmembers", "mmembersjual.noid = mmembers.noid");
	$db->join("ttransjualhead", "mmembersjual.nojual = ttransjualhead.nojual");
	$db->where('mmembersjual.noidbuy',$noidbuy);
	return $db->get();
}
function get_membersjualdl($noidbuy,$db){
	$db->select('mmembersjual.noidbuy,mmembersjual.noidbuyreal,mmembersjual.noid,mmembers.namamembers,mmembersjual.nojual,ttransjualhead.tgljual,mmembersjual.point');
	$db->from('mmembersjual');
	$db->join("mmembers", "mmembersjual.noid = mmembers.noid");
	$db->join("ttransjualhead", "mmembersjual.nojual = ttransjualhead.nojual");
	$db->where('mmembersjual.nouplinebuy',$noidbuy);
	$db->order_by('mmembersjual.noidbuyreal');
	return $db->get();
}

function ec_penerimaan($noterima){
$CI =& get_instance();
	$query = $CI->db->query("SELECT mcabang.namacabang,tterimahead.noterima,tterimahead.tglterima,tterimahead.nokirim,tterimahead.tglkirim,zusers.nama 
		FROM tterimahead INNER JOIN mcabang ON tterimahead.noidcabang = mcabang.noidcabang INNER JOIN zusers ON tterimahead.createdby = zusers.userid 
		WHERE tterimahead.noterima = '".$noterima."'");
	$row = $query->row();
	$cabang=$row->namacabang;
$mailcontent='<!doctype html><html lang="en"><head><title>Penerimaan Barang</title><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /></head><body bgcolor="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"><table width="650" cellspacing="0" cellpadding="0" border="0" align="center"><tbody><tr><td height="3">';
$mailcontent.='<img width="650" height="3" style="display: block;" src="http://i1054.photobucket.com/albums/s490/sakatirta/mail/top_zpsbe5f4c64.jpg"></td></tr></tbody></table><table width="650" cellspacing="0" cellpadding="0" border="0" align="center" style="border-left: 1px solid #c5c5c5; border-right: 1px solid #c5c5c5;">';
$mailcontent.='<tbody><tr><td bgcolor="#FFFFFF" align="center" height="100" style="font-family: Helvetica, arial, sans-serif; font-size: 22px; color: #191919; font-weight: bold; line-height: 29px;"><img  style="display: block;" src="http://i1054.photobucket.com/albums/s490/sakatirta/mail/logotridaya_zps99557cb9.png">Tridaya Sinergi<br>';
$mailcontent.='<span style="font-weight: normal; font-size: 18px; color: #6c6c6c;">Transaksi Penerimaan Barang.</span></td></tr></tbody></table><table width="650" cellspacing="0" cellpadding="0" border="0" align="center" style="border-left: 1px solid #c5c5c5; border-right: 1px solid #c5c5c5;"><tbody><tr><td bgcolor="#FFFFFF" height="20" valign="top">';
$mailcontent.='<img width="648" height="1" style="display: block;" src="http://i1054.photobucket.com/albums/s490/sakatirta/mail/divider_zps4dbed001.jpg"></td></tr></tbody></table><table width="650" cellspacing="0" cellpadding="0" border="0" align="center" style="border-left: 1px solid #c5c5c5; border-right: 1px solid #c5c5c5;">';
$mailcontent.='<tbody><tr>	<td bgcolor="#FFFFFF" width="30">&nbsp;</td><td bgcolor="#FFFFFF" align="left" style="font-family: Helvetica, arial, sans-serif; font-size: 14px; color: #4f4f4f; line-height: 15px;">';
$mailcontent.='<span style="font-weight: bold;">Salam Sejahtera,<br><br></span>Untuk kita ketahui bersama bahwa telah terjadi proses <b>Transaksi Penerimaan Barang</b>.<br><br>Dengan Rincian Penerimaan Barang tersebut sebagai berikut:<br><br><table border="0" cellspacing="2" cellpadding="2" style="margin-left:40px">';
$mailcontent.='<tr><td width="110">Cabang</td><td width="400">:&nbsp; '.$cabang.'</td></tr>';
$mailcontent.='<tr><td width="110">No Penerimaan</td><td width="400">:&nbsp; '.$row->namacabang.'</td></tr>';
$mailcontent.='<tr><td width="110">Tgl Terima</td><td width="400">:&nbsp; '.date_format(date_create($row->tglterima),'d F Y').'</td></tr>';
$mailcontent.='<tr><td width="110">No Kirim</td><td width="400">:&nbsp; '.$row->nokirim.'</td></tr>';
$mailcontent.='<tr><td width="110">Tgl Kirim</td><td width="400">:&nbsp; '.date_format(date_create($row->tglkirim),'d F Y').'</td></tr>';
$mailcontent.='<tr><td width="110">Operator</td><td width="400">:&nbsp; '.$row->nama.'</td></tr></table>';
$mailcontent.='<table border="1px" cellspacing="2" cellpadding="2" style="margin-left:40px"><thead><tr><th width="210">Nama Barang</th><th width="120" align="center">Jumlah</th><th width="120" align="center">Stok Opname</th><th width="120" align="center">Stok '.$cabang.'</th></tr></thead><tbody>';
$query = $CI->db->query("SELECT mbarang.namabarang,tterimadetail.jumlah,mbarang.stok,mcabangstok.stok AS stokcabang FROM tterimadetail 
		INNER JOIN mbarang ON tterimadetail.kodebarang = mbarang.kodebarang INNER JOIN mcabangstok ON tterimadetail.kodebarang = mcabangstok.kodebarang 
		INNER JOIN tterimahead ON tterimadetail.noterima = tterimahead.noterima AND tterimahead.noidcabang = mcabangstok.noidcabang 
		WHERE tterimahead.noterima = '".$noterima."'");
$tbrg = $query->result();
foreach ($tbrg as $rows){
	$mailcontent.='<tr><td width="210">'.$rows->namabarang.'</td><td width="120" align="center">'.number_format($rows->jumlah,0,",",".").'</td><td width="120" align="center">'.number_format($rows->stok,0,",",".").'</td><td width="120" align="center">'.number_format($rows->stokcabang,0,",",".").'</td></tr>';
}
$mailcontent.='</tbody></table><br>Demikian perberitahuan ini kami sampaikan.<br><br>Terima Kasih,<br><br><br><a style="text-decoration: none; color: #315e8b; font-weight: bold;margin-left:5px;" href="">&#920;?z&#227;n&#8482;</a><br>IT Support<br></td><td bgcolor="#FFFFFF" width="30">&nbsp;</td></tr></tbody></table>';
$mailcontent.='<table width="650" cellspacing="0" cellpadding="0" border="0" align="center" style="border-left: 1px solid #c5c5c5; border-right: 1px solid #c5c5c5;"><tbody><tr><td bgcolor="#FFFFFF" height="20" valign="top">&nbsp;</td></tr></tbody></table>';
$mailcontent.='<table width="650" cellspacing="0" cellpadding="0" border="0" align="center"><tbody><tr><td height="22"><img width="650" height="22" style="display: block;" src="http://i1054.photobucket.com/albums/s490/sakatirta/mail/bottom_zps0fb600b5.jpg"></td></tr></tbody></table>';
$mailcontent.='<table width="650" cellspacing="0" cellpadding="0" border="0" align="center"><tbody><tr><td width="650" bgcolor="#008800" height="10"></td></tr></tbody></table><table width="650" cellspacing="0" cellpadding="0" border="0" align="center"><tbody><tr><td width="650" bgcolor="#66cc66" align="center">';
$mailcontent.='<span style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #ffffff; font-style: normal; line-height: 16px;">';
$mailcontent.='Tridaya Sinergi Official Website : <a style="text-decoration: none; color: #315e8b; font-weight: bold;" href="http://tridayasinergi.com">tridayasinergi.com</a>. &#38; <a style="text-decoration: none; color: #315e8b; font-weight: bold;" href="http://tridayasinergi.net">tridayasinergi.net</a></span></td></tr></tbody></table></body></html>';
return $mailcontent; 
}

function ec_mutasi(){
$period=date("ym",strtotime(date("Y-m-1", strtotime(date("Y-m-1"))) . "-1 month"));
$bln=get_month_name(date("m",strtotime(date("Y-m-1", strtotime(date("Y-m-1"))) . "-1 month")));
$periode=$bln." ".date("Y",strtotime(date("Y-m-1", strtotime(date("Y-m-1"))) . "-1 month"));
$mailcontent='<!doctype html><html lang="en"><head><title>Laporan Barang Awal Bulan</title><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /></head><body bgcolor="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"><table width="650" cellspacing="0" cellpadding="0" border="0" align="center"><tbody><tr><td height="3">';
$mailcontent.='<img width="650" height="3" style="display: block;" src="http://i1054.photobucket.com/albums/s490/sakatirta/mail/top_zpsbe5f4c64.jpg"></td></tr></tbody></table><table width="650" cellspacing="0" cellpadding="0" border="0" align="center" style="border-left: 1px solid #c5c5c5; border-right: 1px solid #c5c5c5;">';
$mailcontent.='<tbody><tr><td bgcolor="#FFFFFF" align="center" height="100" style="font-family: Helvetica, arial, sans-serif; font-size: 22px; color: #191919; font-weight: bold; line-height: 29px;"><img  style="display: block;" src="http://i1054.photobucket.com/albums/s490/sakatirta/mail/logotridaya_zps99557cb9.png">Tridaya Sinergi<br>';
$mailcontent.='<span style="font-weight: normal; font-size: 18px; color: #6c6c6c;">Laporan Barang Periode '.$periode.'.</span></td></tr></tbody></table><table width="650" cellspacing="0" cellpadding="0" border="0" align="center" style="border-left: 1px solid #c5c5c5; border-right: 1px solid #c5c5c5;"><tbody><tr><td bgcolor="#FFFFFF" height="20" valign="top">';
$mailcontent.='<img width="648" height="1" style="display: block;" src="http://i1054.photobucket.com/albums/s490/sakatirta/mail/divider_zps4dbed001.jpg"></td></tr></tbody></table><table width="650" cellspacing="0" cellpadding="0" border="0" align="center" style="border-left: 1px solid #c5c5c5; border-right: 1px solid #c5c5c5;">';
$mailcontent.='<tbody><tr><td bgcolor="#FFFFFF" width="30">&nbsp;</td><td bgcolor="#FFFFFF" align="left" style="font-family: Helvetica, arial, sans-serif; font-size: 14px; color: #4f4f4f; line-height: 15px;"><span style="font-weight: bold;">Salam Sejahtera,<br><br></span>';
$mailcontent.='Untuk kita ketahui bersama bahwa pada awal bulan kami beritahukan aktifitas barang bulan lalu dalam <b>Laporan Barang Periode '.$periode.'</b>.<br><br>Rincian Laporan Barang tersebut sebagai berikut:<br><br>';
$mailcontent.='<H3 align="center" ><u>Periode : '.$periode.'</u></H3>';
$CI =& get_instance();
	$query = $CI->db->query("SELECT mcabang.noidcabang,mcabang.namacabang,mkota.kota FROM mcabang INNER JOIN mkota ON mcabang.kota = mkota.idkota ORDER BY mcabang.noidcabang ASC");
	$cabang = $query->result();

foreach ($cabang as $rowc){
$noidcabang=$rowc->noidcabang;	
$mailcontent.='<table border="0" cellspacing="2" cellpadding="2" style="margin-left:5px">';
$mailcontent.='<tr><td width="110">Nama Cabang</td><td width="400">:&nbsp; '.$rowc->namacabang.'</td></tr>';
$mailcontent.='<tr><td width="110">Kota</td><td width="400">:&nbsp; '.$rowc->kota.'</td></tr>';
$mailcontent.='</table><table border="1px" cellspacing="2" cellpadding="2" style="margin-left:5px"><thead><tr><th>Nama Barang</th><th align="center">Stok Awal</th><th align="center">Penerimaan</th><th align="center">Jual ke Stokies</th><th align="center">Jual ke Member</th><th align="center">Stok Akhir</th></tr></thead><tbody>';
	$query = $CI->db->query("SELECT mbarang.namabarang,mutasicabang.stokawal,mutasicabang.penerimaan,mutasicabang.penjualan2stokies,mutasicabang.penjualan,mutasicabang.stokakhir 
		FROM mutasicabang INNER JOIN mbarang ON mutasicabang.kodebarang = mbarang.kodebarang 
		WHERE mutasicabang.periode = '".$period."' AND mutasicabang.noidcabang =".$noidcabang);
	$mutasi = $query->result();
	foreach ($mutasi as $rows){
		$mailcontent.='<tr><td>'.$rows->namabarang.'</td><td align="center">'.number_format($rows->stokawal,0,",",".").'</td><td align="center">'.number_format($rows->penerimaan,0,",",".").'</td><td align="center">'.number_format($rows->penjualan2stokies,0,",",".").'</td><td align="center">'.number_format($rows->penjualan,0,",",".").'</td><td align="center">'.number_format($rows->stokakhir,0,",",".").'</td></tr>';
	}
$mailcontent.='</tbody></table><br><br>';
}
$mailcontent.='Demikian perberitahuan ini kami sampaikan.<br><br>Terima Kasih,<br><br><br><a style="text-decoration: none; color: #315e8b; font-weight: bold;margin-left:5px;" href="">&#920;?z&#227;n&#8482;</a><br>IT Support<br></td><td bgcolor="#FFFFFF" width="30">&nbsp;</td></tr></tbody></table>';
$mailcontent.='<table width="650" cellspacing="0" cellpadding="0" border="0" align="center" style="border-left: 1px solid #c5c5c5; border-right: 1px solid #c5c5c5;">';
$mailcontent.='<tbody><tr><td bgcolor="#FFFFFF" height="20" valign="top">&nbsp;</td></tr></tbody></table><table width="650" cellspacing="0" cellpadding="0" border="0" align="center"><tbody><tr><td height="22"><img width="650" height="22" style="display: block;" src="http://i1054.photobucket.com/albums/s490/sakatirta/mail/bottom_zps0fb600b5.jpg"></td></tr></tbody></table>';
$mailcontent.='<table width="650" cellspacing="0" cellpadding="0" border="0" align="center"><tbody><tr><td width="650" bgcolor="#008800" height="10"></td></tr></tbody></table><table width="650" cellspacing="0" cellpadding="0" border="0" align="center"><tbody><tr><td width="650" bgcolor="#66cc66" align="center">';
$mailcontent.='<span style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #ffffff; font-style: normal; line-height: 16px;">';
$mailcontent.='Tridaya Sinergi Official Website : <a style="text-decoration: none; color: #315e8b; font-weight: bold;" href="http://tridayasinergi.com">tridayasinergi.com</a>. &#38; <a style="text-decoration: none; color: #315e8b; font-weight: bold;" href="http://tridayasinergi.net">tridayasinergi.net</a></span></td></tr></tbody></table></body></html>';
return $mailcontent; 
}
/* END Breadcrum Function */
function check_infobar()
{	$CI =& get_instance();
	$psebelum=date("ym",strtotime(date("Y-m-1", strtotime(date("Y-m-1"))) . "-1 month"));
	$periode=date("ym");
	
	$query = $CI->db->query("SELECT sum(mutasi.penerimaan) as sterima FROM mutasi INNER JOIN mbarang ON mutasi.kodebarang = mbarang.kodebarang WHERE mutasi.periode = '".$psebelum."' AND mbarang.jenis = 1 LIMIT 1");
	$row = $query->row();
	if ($row->sterima){
		$buffs = $row->sterima;
	}else{
		$buffs = 0;
	}
	$query = $CI->db->query("SELECT sum(mutasi.penerimaan) as terima FROM mutasi INNER JOIN mbarang ON mutasi.kodebarang = mbarang.kodebarang WHERE mutasi.periode = '".$periode."' AND mbarang.jenis = 1 LIMIT 1");
	$row = $query->row();
	if ($row->terima){
		$CI->session->set_userdata('iterima', number_format($row->terima,0,",","."));
		$CI->session->set_userdata('iterimaold', number_format($buffs,0,",","."));
		if ($buffs>0){
			$CI->session->set_userdata('cterima', get_range($row->terima,$buffs));
		}else{
			$CI->session->set_userdata('cterima', 'stag-counter');
		}
	}else{
		$CI->session->set_userdata('iterima', '0');
		$CI->session->set_userdata('cterima', 'zero-count');
	}

	$query = $CI->db->query("SELECT sum(mutasi.penjualan) as sjualm FROM mutasi INNER JOIN mbarang ON mutasi.kodebarang = mbarang.kodebarang WHERE mutasi.periode = '".$psebelum."' AND mbarang.jenis = 1 LIMIT 1");
	$row = $query->row();
	if ($row->sjualm){
		$buffs = $row->sjualm;
	}else{
		$buffs = 0;
	}
	$query = $CI->db->query("SELECT sum(mutasi.penjualan) as jualm FROM mutasi INNER JOIN mbarang ON mutasi.kodebarang = mbarang.kodebarang WHERE mutasi.periode = '".$periode."' AND mbarang.jenis = 1 LIMIT 1");
	$row = $query->row();
	if ($row->jualm){
		$CI->session->set_userdata('ijualm', number_format($row->jualm,0,",","."));
		$CI->session->set_userdata('ijualmold', number_format($buffs,0,",","."));
		if ($buffs>0){
			$CI->session->set_userdata('cjualm', get_range($row->jualm,$buffs));
		}else{
			$CI->session->set_userdata('cjualm', 'stag-counter');
		}		
	}else{
		$CI->session->set_userdata('ijualm', '0');
		$CI->session->set_userdata('cjualm', 'zero-count');
	}
	
	$query = $CI->db->query("SELECT sum(mutasi.penjualan2stokies) as sjuals FROM mutasi INNER JOIN mbarang ON mutasi.kodebarang = mbarang.kodebarang WHERE mutasi.periode = '".$psebelum."' AND mbarang.jenis = 1 LIMIT 1");
	$row = $query->row();
	if ($row->sjuals){
		$buffs = $row->sjuals;
	}else{
		$buffs = 0;
	}	
	$query = $CI->db->query("SELECT sum(mutasi.penjualan2stokies) as juals FROM mutasi INNER JOIN mbarang ON mutasi.kodebarang = mbarang.kodebarang WHERE mutasi.periode = '".$periode."' AND mbarang.jenis = 1 LIMIT 1");
	$row = $query->row();
	if ($row->juals){
		$CI->session->set_userdata('ijuals', number_format($row->juals,0,",","."));
		$CI->session->set_userdata('ijualsold', number_format($buffs,0,",","."));
		if ($buffs>0){
			$CI->session->set_userdata('cjuals', get_range($row->juals,$buffs));
		}else{
			$CI->session->set_userdata('cjuals', 'stag-counter');
		}		
	}else{
		$CI->session->set_userdata('ijuals', '0');
		$CI->session->set_userdata('cjuals', 'zero-count');
	}
	
	$psebelum=date("Y-m",strtotime(date("Y-m-1", strtotime(date("Y-m-1"))) . "-1 month"));
	$periode=date("Y-m");
	$query = $CI->db->query("SELECT Count(noid) AS sinewm FROM mmembers WHERE tgldaftar like '".$psebelum."%' LIMIT 1");
	$row = $query->row();
	if ($row->sinewm){
		$buffs = $row->sinewm;
	}else{
		$buffs = 0;
	}	
	$query = $CI->db->query("SELECT Count(noid) AS inewm FROM mmembers WHERE tgldaftar like '".$periode."%' LIMIT 1");
	$row = $query->row();
	if ($row->inewm){
		$CI->session->set_userdata('inewm', number_format($row->inewm,0,",","."));
		$CI->session->set_userdata('inewmold', number_format($buffs,0,",","."));
		if ($buffs>0){
			$CI->session->set_userdata('cnewm', get_range($row->inewm,$buffs));
		}else{
			$CI->session->set_userdata('cnewm', 'stag-counter');
		}		
	}else{
		$CI->session->set_userdata('inewm', '0');
		$CI->session->set_userdata('cnewm', 'zero-count');
	}
	return ;
}
function get_range($cur,$before){
$before=$before*0.5;
switch ($cur)
    {
    case ($cur>=($before*3)):
        $clas = "max-counter";
        break;
    case ($cur>=($before*2) && $cur<($before*3)):
        $clas = "avg-counter";
        break;
    case ($cur>=($before) && $cur<($before*2)):
        $clas = "min-counter";
        break;
    case ($cur>=1 && $cur<($before)):
        $clas = "low-counter";
        break;
    default:
    	$clas = "zero-count";
    	break;
    } 
	
	return $clas;
}
function get_royaltinew($personal,$jalur1,$jalur2,$jalur3,$jalur4,$jalur5,$omzetp,$omzeta){
	$omzetg=$personal+$jalur1+$jalur2+$jalur3+$jalur4+$jalur5;

	if ($omzetg>=10000 && $personal>=30 && $jalur1 >=1000 && $jalur2 >=1000 && $jalur3 >=1000 && $jalur4 >=1000 && $jalur5 >=1000 ){
		$royalti=(50*$omzetp)*($omzetg/$omzeta);
	}else {
		$royalti=0;
	}
	return $royalti;
}
function get_royalti_um($personal,$omzetp,$omzeta){
	if ($personal>180){$personalmax=180;}else{$personalmax=$personal;}
	$royalti=(50*$omzetp)*($personalmax/$omzeta);
	return $royalti;
}		
function get_royalti_gm($personal,$jalur1,$jalur2,$jalur3,$jalur4,$jalur5,$omzetp,$omzeta){
	$omzetg=$personal+$jalur1+$jalur2+$jalur3+$jalur4+$jalur5;
	if ($omzetg>2250){$omzetgmax=2250;}else{$omzetgmax=$omzetg;}
	$royalti=(50*$omzetp)*($omzetgmax/$omzeta);
	return $royalti;
}
function get_royalti_sm($personal,$jalur1,$jalur2,$jalur3,$jalur4,$jalur5,$omzetp,$omzeta){
	$omzetg=$personal+$jalur1+$jalur2+$jalur3+$jalur4+$jalur5;
	if ($omzetg>4500){$omzetgmax=4500;}else{$omzetgmax=$omzetg;}
	$royalti=(75*$omzetp)*($omzetgmax/$omzeta);
	return $royalti;
}
//Begin New EDIT
function get_royalti_d($personal,$jalur1,$jalur2,$jalur3,$jalur4,$jalur5,$omzetp,$omzeta,$omzetamin,$jmlperaih,$period){
	$omzetg=$personal+$jalur1+$jalur2+$jalur3+$jalur4+$jalur5;
	if ($period<1405){
		$royalti=(175*$omzetp)*($omzetg/$omzeta);
	}else{
		$omzetgmin=min($jalur1,$jalur2,$jalur3,$jalur4,$jalur5);
		$royshare=(100*$omzetp)/$jmlperaih;
		$roymin=(50*$omzetp)*($omzetgmin/$omzetamin);
		$roy=(25*$omzetp)*($omzetg/$omzeta);
		$royalti=$royshare+$roymin+$roy;		
	}
	return $royalti;
}
function get_royalti_pd($personal,$jalur1,$jalur2,$jalur3,$jalur4,$jalur5,$omzetp,$omzeta,$omzetapdmin,$jmlperaihpd,$period){
	if ($period<1405){
		$omzetg=$personal+$jalur1+$jalur2+$jalur3+$jalur4+$jalur5;
		$royalti=(50*$omzetp)*($omzetg/$omzeta);
	}else{
		$omzetgmin=min($jalur1,$jalur2,$jalur3,$jalur4,$jalur5);
		$royshare=(40*$omzetp)/$jmlperaihpd;
		$roymin=(10*$omzetp)*($omzetgmin/$omzetapdmin);
		$royalti=$royshare+$roymin;		
	}
	return $royalti;
}
//End New
function get_royaltimp14($personal,$sj1,$sj2,$sj3,$sj4,$sj5,$sjo1,$sjo2,$sjo3,$sjo4,$sjo5,$omzetp,$omzeta,$omzetapd,$omzetaum,$omzetagm,$omzetasm,$roylvl){
	if ($roylvl==4){
		$omzetg=$personal+$sj1+$sj2+$sj3+$sj4+$sj5;		
		$royalti=(175*$omzetp)*($omzetg/$omzeta);
		if ($sj1 >=5000 && $sj2 >=5000 && $sj3 >=5000 && $sj4 >=5000 && $sj5 >=5000 ){
			$royalti=$royalti+((50*$omzetp)*($omzetg/$omzetapd));
		}	
	}elseif ($roylvl==3){
		$omzetg=$personal+$sjo1+$sjo2+$sjo3+$sjo4+$sjo5;
		if ($omzetg>4500){$omzetgmax=4500;}else{$omzetgmax=$omzetg;}
		$royalti=(75*$omzetp)*($omzetgmax/$omzetasm);
	}elseif ($roylvl==2){
		$omzetg=$personal+$sjo1+$sjo2+$sjo3+$sjo4+$sjo5;
		if ($omzetg>2250){$omzetgmax=2250;}else{$omzetgmax=$omzetg;}		
		$royalti=(50*$omzetp)*($omzetgmax/$omzetagm);
	}elseif ($roylvl==1){
		if ($personal>180){$personalmax=180;}else{$personalmax=$personal;}
		$royalti=(50*$omzetp)*($personalmax/$omzetaum);
	}else{
		$royalti=0;
	}
	return $royalti;
}
function cek_nilai($cur,$max=0)
{	$nilainol='<img src="'.site_url('assets/img/rank/starjemred.png').'">';
	$nilaijem='<img src="'.site_url('assets/img/rank/starjem.png').'">';
	$nilaibin='<img src="'.site_url('assets/img/rank/starrank.png').'"> ';
	$nilaigol='<img src="'.site_url('assets/img/rank/stargold.png').'"> ';			
	$jml='';
	if ($max){
		$temp=($cur/$max)*100;
	}else{
		$temp=$cur;
	}
	if ($temp){		
		switch ($temp)
	    {
	    case ($temp>0 && $temp<=2):
	        $jml = $nilaijem;
	        break;
	    case ($temp>2 && $temp<=5):
	        $jml = str_repeat($nilaijem, 2);
	        break;
	    case ($temp>5 && $temp<=8):
	        $jml = str_repeat($nilaijem, 3);
	        break;
	    case ($temp>8 && $temp<=12):
	        $jml = str_repeat($nilaijem, 4);
	        break;
	    case ($temp>12 && $temp<=17):
	        $jml = str_repeat($nilaijem, 5);
	        break;
	    case ($temp>17 && $temp<=23):
	        $jml = $nilaibin;
	        break;
	    case ($temp>23 && $temp<=30):
	        $jml = str_repeat($nilaibin, 2);
	        break;
	    case ($temp>30 && $temp<=37):
	        $jml = str_repeat($nilaibin, 3);
	        break;
	    case ($temp>37 && $temp<=45):
	        $jml = str_repeat($nilaibin, 4);
	        break;
	    case ($temp>45 && $temp<=54):
	        $jml = str_repeat($nilaibin, 5);
	        break;
	    case ($temp>54 && $temp<=64):
	        $jml = $nilaigol;
	        break;
	    case ($temp>64 && $temp<=75):
	        $jml = str_repeat($nilaigol, 2);
	        break;
	    case ($temp>75 && $temp<=87):
	        $jml = str_repeat($nilaigol, 3);
	        break;	
	    case ($temp>87 && $temp<=99):
	        $jml = str_repeat($nilaigol, 4);
	        break;	
	    case ($temp>99 && $temp<=100):
	        $jml = str_repeat($nilaigol, 5);
	        break;					
	    }
	}else{
		if ($max){$jml = $nilainol;}else{$jml = '';}
	}    
return $jml;		
}

function cek_nilaitotal($tot,$nol=0)
{	
	$nilainol='<img src="'.site_url('assets/img/rank/starjemred.png').'">';
	$nilaijem='<img src="'.site_url('assets/img/rank/starjem.png').'">';
	$nilaibin='<img src="'.site_url('assets/img/rank/starrank.png').'"> ';
	$nilaigol='<img src="'.site_url('assets/img/rank/stargold.png').'"> ';			
	$jml='';
	if ($tot>0){		
		switch ($tot)
	    {
	    case ($tot>0 && $tot<=5):
	        $jml = $nilaijem;
	        break;
	    case ($tot>5 && $tot<=30):
	        $jml = str_repeat($nilaijem, 2);
	        break;
	    case ($tot>30 && $tot<=70):
	        $jml = str_repeat($nilaijem, 3);
	        break;
	    case ($tot>70 && $tot<=134):
	        $jml = str_repeat($nilaijem, 4);
	        break;
	    case ($tot>134 && $tot<=236):
	        $jml = str_repeat($nilaijem, 5);
	        break;
	    case ($tot>236 && $tot<=402):
	        $jml = $nilaibin;
	        break;
	    case ($tot>402 && $tot<=669):
	        $jml = str_repeat($nilaibin, 2);
	        break;
	    case ($tot>669 && $tot<=1100):
	        $jml = str_repeat($nilaibin, 3);
	        break;
	    case ($tot>1100 && $tot<=1797):
	        $jml = str_repeat($nilaibin, 4);
	        break;
	    case ($tot>1797 && $tot<=2924):
	        $jml = str_repeat($nilaibin, 5);
	        break;
	    case ($tot>2924 && $tot<=4747):
	        $jml = $nilaigol;
	        break;
	    case ($tot>4747 && $tot<=7696):
	        $jml = str_repeat($nilaigol, 2);
	        break;
	    case ($tot>7696 && $tot<=12465):
	        $jml = str_repeat($nilaigol, 3);
	        break;	
	    case ($tot>12465 && $tot<=20181):
	        $jml = str_repeat($nilaigol, 4);
	        break;	
	    case ($tot>20181):
	        $jml = str_repeat($nilaigol, 5);
	        break;					
	    }
	}else{
		if ($nol){$jml = $nilainol;}else{$jml = '';}
	}    
return $jml;		
}

function cek_grade($cur)
{	if ($cur>0){	
		switch ($cur)
		    {
		    case ($cur>0 && $cur<=5):
		        $jml = 1;
		        break;
		    case ($cur>5 && $cur<=30):
		        $jml = 2;
		        break;
		    case ($cur>30 && $cur<=70):
		        $jml = 3;
		        break;
		    case ($cur>70 && $cur<=134):
		        $jml = 4;
		        break;
		    case ($cur>134 && $cur<=236):
		        $jml = 5;
		        break;
		    case ($cur>236 && $cur<=402):
		        $jml = 6;
		        break;
		    case ($cur>402 && $cur<=669):
		        $jml = 7;
		        break;
		    case ($cur>669 && $cur<=1100):
		        $jml = 8;
		        break;
		    case ($cur>1100 && $cur<=1797):
		        $jml = 9;
		        break;
		    case ($cur>1797 && $cur<=2924):
		        $jml = 10;
		        break;
		    case ($cur>2924 && $cur<=4747):
		        $jml = 11;
		        break;
		    case ($cur>4747 && $cur<=7696):
		        $jml = 12;
		        break;
		    case ($cur>7696 && $cur<=12465):
		        $jml = 13;
		        break;	
		    case ($cur>12465 && $cur<=20181):
		        $jml = 14;
		        break;	
		    case ($cur>20181):
		        $jml = 15;
		        break;
		    }
	}else{
		$jml = 0;
	}		     		
return $jml;		
}	
function getpermissions($id) {
			switch ($id)
			{
			case 0:
			case 1:
			  $batas=1;
			  break;
			case 2:
			case 3:
			  $batas=2;
			  break;
			case 4:
			case 5:
			case 6:
			case 7:
			  $batas=4;
			  break;			  
			case 8:
			case 9:
			case 10:
			case 11:
			case 12:
			case 13:
			case 14:
			case 15:
			  $batas=8;
			  break;		  
			case 16:
			case 17:
			case 18:
			case 19:
			case 20:
			case 21:
			case 22:
			case 23:
			case 24:
			case 25:
			case 26:
			case 27:
			case 28:
			case 29:
			case 30:
			case 31:
			  $batas=16;
			break;
				default:
			  $batas=32;
			  }	
	$listcb[]=array();
	$fac=$id;
	while ($fac>0){
		$listcb[]=$batas;
		$fac=$fac-$batas;
		$batas=$batas/2;
		while ($fac<$batas){
			$batas=$batas/2;
		}	
	}
	return $listcb;
}
function getauth($allow,$level) {
	$CI =& get_instance();
	$query = $CI->db->query("SELECT nilai FROM zsetting WHERE setting = '".$allow."'");
	if ($row = $query->row()){
		$listcb=getpermissions($row->nilai);
		if(array_search($level, $listcb)){
			return 1;
		}else{
			return 0;
		}		
	}else{
		return 1;
	}
}

function ec_newmember($fulltgl,$tgl){
$mailcontent='<!doctype html><html lang="en"><head><title>Member Baru</title><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /></head><body bgcolor="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"><table width="650" cellspacing="0" cellpadding="0" border="0" align="center"><tbody><tr><td height="3">';
$mailcontent.='<img width="650" height="3" style="display: block;" src="http://i1054.photobucket.com/albums/s490/sakatirta/mail/top_zpsbe5f4c64.jpg"></td></tr></tbody></table><table width="650" cellspacing="0" cellpadding="0" border="0" align="center" style="border-left: 1px solid #c5c5c5; border-right: 1px solid #c5c5c5;">';
$mailcontent.='<tbody><tr><td bgcolor="#FFFFFF" align="center" height="100" style="font-family: Helvetica, arial, sans-serif; font-size: 22px; color: #191919; font-weight: bold; line-height: 29px;"><img style="display: block;" src="http://i1054.photobucket.com/albums/s490/sakatirta/mail/logotridaya_zps99557cb9.png">Tridaya Sinergi<br><span style="font-weight: normal; font-size: 18px; color: #6c6c6c;">';
$mailcontent.='Daftar Member Baru tanggal '.$fulltgl.'.</span></td></tr></tbody></table><table width="650" cellspacing="0" cellpadding="0" border="0" align="center" style="border-left: 1px solid #c5c5c5; border-right: 1px solid #c5c5c5;"><tbody><tr>	<td bgcolor="#FFFFFF" height="20" valign="top">';
$mailcontent.='<img width="648" height="1" style="display: block;" src="http://i1054.photobucket.com/albums/s490/sakatirta/mail/divider_zps4dbed001.jpg"></td></tr></tbody></table><table width="650" cellspacing="0" cellpadding="0" border="0" align="center" style="border-left: 1px solid #c5c5c5; border-right: 1px solid #c5c5c5;">';
$mailcontent.='<tbody><tr><td bgcolor="#FFFFFF" width="30">&nbsp;</td><td bgcolor="#FFFFFF" align="left" style="font-family: Helvetica, arial, sans-serif; font-size: 14px; color: #4f4f4f; line-height: 15px;"><span style="font-weight: bold;">Salam Sejahtera,<br><br></span>';
$mailcontent.='Untuk kita ketahui bersama bahwa kami beritahukan adanya aktifasi member baru pada hari kemarin atau tanggal <b>'.$fulltgl.'</b>.<br><br>';
$mailcontent.='Daftar Member baru tersebut sebagai berikut:<br><br><table border="1px" cellspacing="2" cellpadding="6" style="margin-left:5px"><thead><tr><th>NoID</th><th align="center">Nama</th><th align="center">Kota</th><th align="center">Telp</th><th align="center">HP</th><th align="center">Stokies</th></tr></thead><tbody>';
$CI =& get_instance();
	$query = $CI->db->query("SELECT mmembers.noid,mmembers.namamembers,mkota.kota,mmembers.telepon,mmembers.hp,mstokies.namastokies FROM mmembers
		INNER JOIN mkota ON mmembers.kota = mkota.idkota INNER JOIN mstokies ON mmembers.noidstokies = mstokies.noidstokies WHERE (mmembers.telepon <> '' OR mmembers.hp <> '') AND
		mmembers.tgldaftar LIKE '".$tgl."%' ORDER BY mmembers.namamembers ASC");
$tbrg = $query->result();
foreach ($tbrg as $rows){
	$mailcontent.='<tr><td>'.$rows->noid.'</td><td>'.$rows->namamembers.'</td><td align="center">'.$rows->kota.'</td><td align="center">'.$rows->telepon.'</td><td align="center">'.$rows->hp.'</td><td align="center">'.$rows->namastokies.'</td></tr>';
}
$mailcontent.='</tbody></table><br>Member baru diatas hanya yang mencatumkan No telp atau no HP pada saat aktifasi.<br>Demikian perberitahuan ini kami sampaikan.<br><br>Terima Kasih,<br><br><br><a style="text-decoration: none; color: #315e8b; font-weight: bold;margin-left:5px;" href="">&#920;?z&#227;n&#8482;</a><br>IT Support<br></td><td bgcolor="#FFFFFF" width="30">&nbsp;</td></tr></tbody></table>';
$mailcontent.='<table width="650" cellspacing="0" cellpadding="0" border="0" align="center" style="border-left: 1px solid #c5c5c5; border-right: 1px solid #c5c5c5;"><tbody><tr><td bgcolor="#FFFFFF" height="20" valign="top">&nbsp;</td></tr></tbody></table><table width="650" cellspacing="0" cellpadding="0" border="0" align="center"><tbody><tr><td height="22">';
$mailcontent.='<img width="650" height="22" style="display: block;" src="http://i1054.photobucket.com/albums/s490/sakatirta/mail/bottom_zps0fb600b5.jpg"></td></tr></tbody></table><table width="650" cellspacing="0" cellpadding="0" border="0" align="center"><tbody><tr><td width="650" bgcolor="#008800" height="10"></td></tr></tbody></table><table width="650" cellspacing="0" cellpadding="0" border="0" align="center">';
$mailcontent.='<tbody><tr><td width="650" bgcolor="#66cc66" align="center"><span style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #ffffff; font-style: normal; line-height: 16px;">Tridaya Sinergi Official Website : <a style="text-decoration: none; color: #315e8b; font-weight: bold;" href="http://tridayasinergi.com">tridayasinergi.com</a>. &#38; <a style="text-decoration: none; color: #315e8b; font-weight: bold;" href="http://tridayasinergi.net">tridayasinergi.net</a></span></td></tr></tbody></table></body></html>';
return $mailcontent; 
}
function ec_newstokies($fulltgl,$tgl){
$mailcontent='<!doctype html><html lang="en"><head><title>Stokies Baru</title><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /></head><body bgcolor="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"><table width="650" cellspacing="0" cellpadding="0" border="0" align="center"><tbody><tr><td height="3">';
$mailcontent.='<img width="650" height="3" style="display: block;" src="http://i1054.photobucket.com/albums/s490/sakatirta/mail/top_zpsbe5f4c64.jpg"></td></tr></tbody></table><table width="650" cellspacing="0" cellpadding="0" border="0" align="center" style="border-left: 1px solid #c5c5c5; border-right: 1px solid #c5c5c5;">';
$mailcontent.='<tbody><tr><td bgcolor="#FFFFFF" align="center" height="100" style="font-family: Helvetica, arial, sans-serif; font-size: 22px; color: #191919; font-weight: bold; line-height: 29px;"><img style="display: block;" src="http://i1054.photobucket.com/albums/s490/sakatirta/mail/logotridaya_zps99557cb9.png">Tridaya Sinergi<br><span style="font-weight: normal; font-size: 18px; color: #6c6c6c;">';
$mailcontent.='Daftar Stokies Baru Tanggal '.$fulltgl.'.</span></td></tr></tbody></table><table width="650" cellspacing="0" cellpadding="0" border="0" align="center" style="border-left: 1px solid #c5c5c5; border-right: 1px solid #c5c5c5;"><tbody><tr>	<td bgcolor="#FFFFFF" height="20" valign="top">';
$mailcontent.='<img width="648" height="1" style="display: block;" src="http://i1054.photobucket.com/albums/s490/sakatirta/mail/divider_zps4dbed001.jpg"></td></tr></tbody></table><table width="650" cellspacing="0" cellpadding="0" border="0" align="center" style="border-left: 1px solid #c5c5c5; border-right: 1px solid #c5c5c5;">';
$mailcontent.='<tbody><tr><td bgcolor="#FFFFFF" width="30">&nbsp;</td><td bgcolor="#FFFFFF" align="left" style="font-family: Helvetica, arial, sans-serif; font-size: 14px; color: #4f4f4f; line-height: 15px;"><span style="font-weight: bold;">Salam Sejahtera,<br><br></span>';
$mailcontent.='Untuk kita ketahui bersama bahwa kami beritahukan adanya Stokies baru pada hari kemarin atau tanggal <b>'.$fulltgl.'</b>.<br><br>';
$mailcontent.='Daftar Stokies baru tersebut sebagai berikut:<br><br><table border="1px" cellspacing="2" cellpadding="6" style="margin-left:5px"><thead><tr><th>Stokies</th><th align="center">Pemilik</th><th align="center">Kota</th><th align="center">Telp</th><th align="center">HP</th><th align="center">Cabang</th></tr></thead><tbody>';
$CI =& get_instance();
	$query = $CI->db->query("SELECT mstokies.namastokies,mmembers.namamembers,mkota.kota,mstokies.telepon,mstokies.hp,mcabang.namacabang FROM mstokies
		INNER JOIN mkota ON mstokies.kota = mkota.idkota INNER JOIN mmembers ON mstokies.noid = mmembers.noid INNER JOIN mcabang ON mstokies.noidcabang = mcabang.noidcabang WHERE (mstokies.telepon <> '' OR mstokies.hp <> '') AND
		mstokies.tgldaftar LIKE '".$tgl."%' ORDER BY mstokies.namastokies ASC");
$tbrg = $query->result();
foreach ($tbrg as $rows){
	$mailcontent.='<tr><td>'.$rows->namastokies.'</td><td>'.$rows->namamembers.'</td><td align="center">'.$rows->kota.'</td><td align="center">'.$rows->telepon.'</td><td align="center">'.$rows->hp.'</td><td align="center">'.$rows->namacabang.'</td></tr>';
}
$mailcontent.='</tbody></table><br>Stokies baru diatas hanya yang mencatumkan No telp atau no HP pada formulir.<br>Demikian perberitahuan ini kami sampaikan.<br><br>Terima Kasih,<br><br><br><a style="text-decoration: none; color: #315e8b; font-weight: bold;margin-left:5px;" href="">&#920;?z&#227;n&#8482;</a><br>IT Support<br></td><td bgcolor="#FFFFFF" width="30">&nbsp;</td></tr></tbody></table>';
$mailcontent.='<table width="650" cellspacing="0" cellpadding="0" border="0" align="center" style="border-left: 1px solid #c5c5c5; border-right: 1px solid #c5c5c5;"><tbody><tr><td bgcolor="#FFFFFF" height="20" valign="top">&nbsp;</td></tr></tbody></table><table width="650" cellspacing="0" cellpadding="0" border="0" align="center"><tbody><tr><td height="22">';
$mailcontent.='<img width="650" height="22" style="display: block;" src="http://i1054.photobucket.com/albums/s490/sakatirta/mail/bottom_zps0fb600b5.jpg"></td></tr></tbody></table><table width="650" cellspacing="0" cellpadding="0" border="0" align="center"><tbody><tr><td width="650" bgcolor="#008800" height="10"></td></tr></tbody></table><table width="650" cellspacing="0" cellpadding="0" border="0" align="center">';
$mailcontent.='<tbody><tr><td width="650" bgcolor="#66cc66" align="center"><span style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #ffffff; font-style: normal; line-height: 16px;">Tridaya Sinergi Official Website : <a style="text-decoration: none; color: #315e8b; font-weight: bold;" href="http://tridayasinergi.com">tridayasinergi.com</a>. &#38; <a style="text-decoration: none; color: #315e8b; font-weight: bold;" href="http://tridayasinergi.net">tridayasinergi.net</a></span></td></tr></tbody></table></body></html>';
return $mailcontent; 
}
///New MP14
function getotoritas($id) {
	switch ($id)
	{
	case 0:
	  $oto="Centralization";
	  break;
	case 1:
	  $oto="Decentralization";
	  break;
	case 2:
	  $oto="Warehouse";
	  break;
	case 3:
	  $oto="Mobile";
	  break;
	}	
	return $oto;
}
function getstatuspesan($sta,$jml,$progress,$realisasi) {
	if ($realisasi == 0){
        $oto="Pending";
	}elseif ($sta==0 && $progress == 0){
		$oto="Opened";
	}elseif ($sta<$jml){
		$oto="Progress";
	}elseif ($sta==$jml){
		$oto="Closed";
	}else{
		$oto="ERROR";
	}
	return $oto;
}

function getstatuspesandetail($sta,$jml,$progress) {
	if ($jml == 0){
        $oto="Rejected";
	}elseif ($sta==0 && $progress == 0){
		$oto="Opened";
	}elseif ($progress<$jml){
		$oto="Progress";
	}elseif ($sta == 1){
		$oto="Closed";
	}else{
		$oto="ERROR";
	}
	return $oto;
}
function ec_useronline($namastokies,$username,$pwd){
$mailcontent='<!doctype html><html lang="en"><head><title>Permohonan Stokies Online</title><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /></head><body bgcolor="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"><table width="650" cellspacing="0" cellpadding="0" border="0" align="center"><tbody><tr>';
$mailcontent.='<td height="75"><a style="font-family: Helvetica, arial, sans-serif; font-size: 18px; font-weight: bold; text-decoration: none; color: #ffffff" href="#">Tridaya Sinergi : Reset Password</a>';
$mailcontent.='</td></tr></tbody></table><table width="650" cellspacing="0" cellpadding="0" border="0" align="center"><tbody><tr><td height="3"><img width="650" height="3" style="display: block;" src="http://i1054.photobucket.com/albums/s490/sakatirta/mail/top_zpsbe5f4c64.jpg"></td></tr></tbody></table>';
$mailcontent.='<table width="650" cellspacing="0" cellpadding="0" border="0" align="center" style="border-left: 1px solid #c5c5c5; border-right: 1px solid #c5c5c5;"><tbody><tr>	<td bgcolor="#FFFFFF" align="center" height="100" style="font-family: Helvetica, arial, sans-serif; font-size: 22px; color: #191919; font-weight: bold; line-height: 29px;">';
$mailcontent.='<img style="display: block;" src="http://i1054.photobucket.com/albums/s490/sakatirta/mail/logotridaya_zps99557cb9.png">Tridaya Sinergi<br><span style="font-weight: normal; font-size: 18px; color: #6c6c6c;">Permohonan Stokies Online.</span></td></tr></tbody></table>';
$mailcontent.='<table width="650" cellspacing="0" cellpadding="0" border="0" align="center" style="border-left: 1px solid #c5c5c5; border-right: 1px solid #c5c5c5;"><tbody><tr><td bgcolor="#FFFFFF" height="20" valign="top">';
$mailcontent.='<img width="648" height="1" style="display: block;" src="http://i1054.photobucket.com/albums/s490/sakatirta/mail/divider_zps4dbed001.jpg"></td></tr></tbody></table><table width="650" cellspacing="0" cellpadding="0" border="0" align="center" style="border-left: 1px solid #c5c5c5; border-right: 1px solid #c5c5c5;">';
$mailcontent.='<tbody><tr><td bgcolor="#FFFFFF" width="30">&nbsp;</td>	<td bgcolor="#FFFFFF" align="left" style="font-family: Helvetica, arial, sans-serif; font-size: 14px; color: #4f4f4f; line-height: 15px;">';
$mailcontent.='<span style="font-weight: bold;">Yth. '.$namastokies.',<br><br></span>Sesuai permintaan anda untuk <b>"Latihan Stokies Online"</b>, Untuk itu sekarang anda telah bisa latihan online.<br>Rincian login anda sekarang adalah sebagai berikut:<br><br><table border="0" cellspacing="2" cellpadding="2" style="margin-left:40px">';
$mailcontent.='<tr><td width="110">Username</td><td width="400">:&nbsp; '.$username.'</td></tr><tr><td width="110">Password</td><td width="400">:&nbsp; '.$pwd.'</td></tr></table>';
$mailcontent.='<br>Untuk latihan silahkan akses alamat URL : <a style="text-decoration: none; color: #315e8b; font-weight: bold;" href="http://latstokies.tridayasinergi.net" target="new">latstokies.tridayasinergi.net</a>';
$mailcontent.='<br>Kami sarankan untuk merubah password ini secepatnya. Untuk merubahnya silahkan Login dan paling kiri atas klik &quot;usernameanda&quot; -> &quot;Ganti Password&quot;.<br><br>Salam Sejahtera,<br><br><a style="text-decoration: none; color: #315e8b; font-weight: bold;" href="http://tridayasinergi.com">Tridaya Sinergi.</a><br>';
$mailcontent.='</td><td bgcolor="#FFFFFF" width="30">&nbsp;</td></tr></tbody></table><table width="650" cellspacing="0" cellpadding="0" border="0" align="center" style="border-left: 1px solid #c5c5c5; border-right: 1px solid #c5c5c5;">';
$mailcontent.='<tbody><tr><td bgcolor="#FFFFFF" height="20" valign="top">&nbsp;</td></tr></tbody></table><table width="650" cellspacing="0" cellpadding="0" border="0" align="center"><tbody><tr><td height="22">';
$mailcontent.='<img width="650" height="22" style="display: block;" src="http://i1054.photobucket.com/albums/s490/sakatirta/mail/bottom_zps0fb600b5.jpg"></td></tr></tbody></table><table width="650" cellspacing="0" cellpadding="0" border="0" align="center"><tbody><tr><td width="650" bgcolor="#008800" height="10"></td></tr></tbody></table>';
$mailcontent.='<table width="650" cellspacing="0" cellpadding="0" border="0" align="center"><tbody><tr><td width="650" bgcolor="#66cc66" align="center"><span style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #ffffff; font-style: normal; line-height: 16px;">';
$mailcontent.='Tridaya Sinergi Official Website : <a style="text-decoration: none; color: #315e8b; font-weight: bold;" href="http://tridayasinergi.com">tridayasinergi.com</a>. &#38; <a style="text-decoration: none; color: #315e8b; font-weight: bold;" href="http://tridayasinergi.net">tridayasinergi.net</a></span></td></tr></tbody></table></body></html>';
return $mailcontent; 
}
function get_nama($noid){
	$CI =& get_instance();
	$CI->db->select('namamembers');
	$CI->db->from('mmembers');
	$CI->db->where('noid',$noid);
	$query = $CI->db->get();
	if($row = $query->row()){return $row->namamembers;}
}
/////Begin New
function get_royaltimp14new($personal,$sj1,$sj2,$sj3,$sj4,$sj5,$sjo1,$sjo2,$sjo3,$sjo4,$sjo5,
		$omzetp,$omzeta,$omzetapd,$omzetaum,$omzetagm,$omzetasm,$roylvl,
		$omzetamin,$jmlperaih,$omzetapdmin,$jmlperaihpd){
	if ($roylvl==4){
		$omzetg=$personal+$sj1+$sj2+$sj3+$sj4+$sj5;
		$omzetgmin=min($sj1,$sj2,$sj3,$sj4,$sj5);
		$royshare=(100*$omzetp)/$jmlperaih;
		$roymin=(50*$omzetp)*($omzetgmin/$omzetamin);
		$roy=(25*$omzetp)*($omzetg/$omzeta);
		$royalti=$royshare+$roymin+$roy;
		if ($sj1 >=5000 && $sj2 >=5000 && $sj3 >=5000 && $sj4 >=5000 && $sj5 >=5000 ){
			$royshare=(40*$omzetp)/$jmlperaihpd;
			$roymin=(10*$omzetp)*($omzetgmin/$omzetapdmin);
			$royalti=$royalti+$royshare+$roymin;
		}	
	}elseif ($roylvl==3){
		$omzetg=$personal+$sjo1+$sjo2+$sjo3+$sjo4+$sjo5;
		if ($omzetg>4500){$omzetgmax=4500;}else{$omzetgmax=$omzetg;}
		$royalti=(75*$omzetp)*($omzetgmax/$omzetasm);
	}elseif ($roylvl==2){
		$omzetg=$personal+$sjo1+$sjo2+$sjo3+$sjo4+$sjo5;
		if ($omzetg>2250){$omzetgmax=2250;}else{$omzetgmax=$omzetg;}		
		$royalti=(50*$omzetp)*($omzetgmax/$omzetagm);
	}elseif ($roylvl==1){
		if ($personal>180){$personalmax=180;}else{$personalmax=$personal;}
		$royalti=(50*$omzetp)*($personalmax/$omzetaum);
	}else{
		$royalti=0;
	}
	return $royalti;
}
function load_icon($nama_file){
	
	return '<img src="'.site_url('assets/img/ico/'.$nama_file.'.png').'">';
}
function get_nama_periode($bulan,$tahun){
	$bulan=convert_month($bulan);
	return $bulan .' '.$tahun;
}
// End New

//TSADMIN NEW 2014
function cetak_jaringan($query)
{
	
    $jaringan = '';
	foreach($query as $row){
        $boxover = "Tanggal Aktifasi : ".$row['tgldaftar'];
        $boxover .= "<br/>Downline Langsung : ".count($row['children']);
        $boxover .= "<br/>Poin Personal ".date("M y").": ".$row['personal'];
        $boxover = '<div class="popbox"><h2>'.$row['namamembers'].'</h2><p>'.$boxover.'</p></div>';
        $fulldownline = (count($row['children']) == 5)? "hasFullDownline" : "";
        $jaringan.= "<li class='popper $fulldownline'>".$row['noid']."<br/>".$row['level'].$boxover;
        if(count($row['children']) > 0){ 
            $jaringan   .= "<ul>";	
            $jaringan   .= cetak_jaringan($row['children']);		
            $jaringan   .= "</ul>";
        }
        $jaringan.= "</li>";
	}
	return $jaringan;
}

function stUser($id)
{
	$data = array('0' => '<span class="btn btn-minw btn-square btn-sm btn-danger">NonActive</span>',
				  '1' => '<span class="btn btn-minw btn-square btn-sm btn-success">Active</span>',
				  ''  => '');
	return  $data[$id];
}

function sql_connect(){
$CI =& get_instance();
 $conn= array(
    'user' => $CI->db->username,
    'pass' => $CI->db->password,
    'db'   => $CI->db->database,
    'host' => $CI->db->hostname);
  return $conn;
}

function encryptURL($url){
  $CI=& get_instance();
  $base64 =$CI->encrypt->encode($url);
  $urisafe = strtr($base64, '+/', '-_');
  return $urisafe;
}

function decryptURL($url){
  $CI=& get_instance();
  $urisafe = strtr($url, '-_', '+/');
  $base64 =$CI->encrypt->decode($urisafe);
  return $base64;
}

?>
<?php
class SessionAdmin {

	var $session_expire = 7200;

	function SessionAdmin(){
		$this->CI =& get_instance();

		header("Expires: Sat, 26 Jul 1997 05:00:00 GMT" );
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT" );
		header("Cache-Control: no-store, no-cache, must-revalidate" );
		header("Cache-Control: post-check=0, pre-check=0", false );
		header("Pragma: no-cache" );

		// session_start();
	}

	function SaltWord(){
		$salt = 'B1m4sbuddh4';
		return $salt;
	}

	function createSession($userdata=array()){
		if(count($userdata)<1) {
			return false;
		}
		
		$_SESSION['authCmsUserClient'] = $userdata;
		unset($userdata);
	}
	function destroySession(){
		$_SESSION['authCmsUserClient'] = '';
		unset($_SESSION['authCmsUserClient']);
	}
	function validateSession(){
		if(!isset($_SESSION['authCmsUserClient']) or $_SESSION['authCmsUserClient']=='' )
			return FALSE;
		if(intval($_SESSION['authCmsUserClient']['login_time'])+$this->session_expire < time())
			return FALSE;
		return TRUE;
	}
	
	function validController(){
		$func = split('/',str_replace($_SERVER['SCRIPT_NAME']."/","",$_SERVER['PHP_SELF']));
		$ctrl = $func[0];
		$sess_ctrl = $_SESSION['authCmsUserClient']['controller'];
		if($ctrl=='home' || $ctrl=='pwd') return TRUE;
		$pos = strrpos($sess_ctrl,$ctrl);		
		if($pos===false)
			return FALSE;
		return TRUE;
	}
	
	function noSession($location=""){
		if ($this->validateSession() == FALSE && $location!="") {
			echo "<script language='javascript'>\n";
			echo "location.href='".$location."'\n";
			echo "</script>\n";
			die();
		}
/*	
		if($this->validController() == FALSE){
			echo "<script language='javascript'>\n";
			echo "alert('Your do not have privillage to access this menu.');history.back();\n";
			echo "</script>\n";
			die();		
		}	
*/		
	}
}

?>
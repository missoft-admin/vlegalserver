<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['packages'] = array();

$autoload['libraries'] = array(
							'upload',
							'database',
							'session',
							'parser',
							'user_agent',
							'datatables',
							'SSP',
							'encrypt',
							'encryption',
							'SessionAdmin'
							   );

$autoload['drivers'] = array('session');

$autoload['helper'] = array('url',
							'form',
							'html',
							'text',
							'path',
							'main',
							'db');

$autoload['config'] = array();

$autoload['language'] = array();

$autoload['model'] = array('all_model',
						   'infobar_model','DataTable_model' => 'datatable');

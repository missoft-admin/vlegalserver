<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'dashboard';

/**
========================  L I N K  G E T  D A T A  J S O N  ========================
*/
$route['json/negara'] 		 = 'A_json/getNegara/$1';
$route['json/supplier'] 	 = 'A_json/getSupplier/$1';
$route['json/clients'] 		 = 'A_json/getClients/$1';
$route['json/loading'] 		 = 'A_json/getLoading/$1';
$route['json/groupusers']	 = 'A_json/getGroupUsers/$1';
$route['json/provinsi']	 	 = 'A_json/getProvinsi/$1';
$route['json/kabupaten']	 = 'A_json/getKabupaten/$1';
$route['json/users']	 	 = 'A_json/getUsers/$1';
$route['json/iui']	 	 	 = 'A_json/getIui/$1';
$route['json/detailclient1/(:any)'] = 'A_json/detailclient1/$1';
$route['json/detailclient2/(:any)'] = 'A_json/detailclient2/$1';
$route['json/buyer']	 	 	 = 'A_json/getBuyer/$1';
$route['json/sortimen']	 	 	 = 'A_json/getSortimen/$1';
$route['json/wip']	 	 	 	 = 'A_json/getWip/$1';
$route['json/produk']	 	 	 = 'A_json/getProduk/$1';

/**
=============================  L I N K  S E T T I N G S =============================
*/
//link authentication
$route['SignIn'] 		= 'auth/signin/$1';
$route['SignUp'] 		= 'auth/signup/$1';
$route['SignOut'] 		= 'auth/do_logout/$1';
$route['Forgot'] 		= 'auth/forgot_password/$1';
	//link proses authentication
	$route['signup_proses'] = 'auth/proses_signup/$1';
	$route['signin_proses'] = 'auth/do_login/$1';
/**
=============================  L I N K  S E T T I N G S =============================
*/
// Link setting users 
$route['setting/users'] 				= 'Musers/showingData/$1';
$route['setting/users/(:any)'] 			= 'Musers/indexUpdate/$1';
$route['setting/new-users'] 			= 'Musers/insertBaru/$1';
$route['setting/new-users/proses']		= 'Musers/saveNew/$1';
$route['setting/update-users/proses']	= 'Musers/FupdateData/$1';
// $route['ajax/users'] 					= 'Musers/books_page/$1';
$route['ajax/users'] 					= 'Musers/getListDT/$1';
$route['ajax/upStatus'] 				= 'Musers/gantiStatus/$1';
$route['ajax/delAkun'] 					= 'Musers/delAkun/$1';

// Link setting group users
$route['setting/group-users'] 			= 'MgroupUsers/showingData/$1';
$route['setting/group-users/(:any)'] 	= 'MgroupUsers/indexUpdate/$1';
$route['setting/new-group'] 			= 'MgroupUsers/insertBaru/$1';
$route['setting/new-group/proses']		= 'MgroupUsers/saveNew/$1';
$route['setting/update-group/proses']	= 'MgroupUsers/FupdateData/$1';
$route['ajax/group-users'] 				= 'MgroupUsers/getListDT/$1';
$route['ajax/upStatusGroup']			= 'MgroupUsers/gantiStatus/$1';
$route['ajax/delGroup'] 				= 'MgroupUsers/delAkun/$1';

// Link setting client
$route['setting/client'] 				= 'Mclient/showingData/$1';
$route['setting/client/(:any)']			= 'Mclient/indexUpdate/$1';
$route['setting/new-client'] 			= 'Mclient/insertBaru/$1';
$route['setting/new-client/proses']		= 'Mclient/saveNew/$1';
$route['setting/update-client/proses'] 	= 'Mclient/FupdateData/$1';
$route['setting/upRow'] 				= 'Mclient/editRow/$1';
$route['setting/saveSetting']			= 'Mclient/saveSetting/$1';
$route['ajax/client'] 					= 'Mclient/getListDT/$1';
$route['ajax/buyer'] 					= 'Mclient/getListDT2/$1';
$route['ajax/supplier_'] 				= 'Mclient/getListDT3/$1';
$route['ajax/sortimen_'] 				= 'Mclient/getListDT4/$1';
$route['ajax/wip_'] 					= 'Mclient/getListDT5/$1';
$route['ajax/produk_'] 					= 'Mclient/getListDT6/$1';
$route['ajax/upStatusClient']			= 'Mclient/gantiStatus/$1';
$route['ajax/delClient'] 				= 'Mclient/delAkun/$1';
$route['ajax/delRow'] 					= 'Mclient/delRow/$1';

// Link setting buyers
$route['setting/buyers'] 				= 'Mbuyers/showingData/$1';
$route['setting/buyers/(:any)']			= 'Mbuyers/indexUpdate/$1';
$route['setting/new-buyer'] 			= 'Mbuyers/insertBaru/$1';
$route['setting/new-buyer/proses']		= 'Mbuyers/saveNew/$1';
$route['setting/update-buyers/proses']	= 'Mbuyers/FupdateData/$1';
$route['ajax/buyers'] 					= 'Mbuyers/getListDT/$1';
$route['ajax/upStatusBuyer']			= 'Mbuyers/gantiStatus/$1';
$route['ajax/delBuyer'] 				= 'Mbuyers/delAkun/$1';

// Link setting supplier
$route['setting/supplier'] 					= 'Msupplier/showingData/$1';
$route['setting/supplier/(:any)']			= 'Msupplier/indexUpdate/$1';
$route['setting/new-supplier'] 				= 'Msupplier/insertBaru/$1';
$route['setting/new-supplier/proses']		= 'Msupplier/saveNew/$1';
$route['setting/update-supplier/proses']	= 'Msupplier/FupdateData/$1';
$route['ajax/supplier'] 					= 'Msupplier/getListDT/$1';
$route['ajax/upStatusSupplier']				= 'Msupplier/gantiStatus/$1';
$route['ajax/delSupplier'] 					= 'Msupplier/delAkun/$1';

// Link setting jenis kayu
$route['setting/jenis-kayu'] 				= 'Mjeniskayu/showingData/$1';
$route['setting/jenis-kayu/(:any)']			= 'Mjeniskayu/indexUpdate/$1';
$route['setting/new-jeniskayu'] 			= 'Mjeniskayu/insertBaru/$1';
$route['setting/new-jeniskayu/proses']		= 'Mjeniskayu/saveNew/$1';
$route['setting/update-jeniskayu/proses']	= 'Mjeniskayu/FupdateData/$1';
$route['ajax/jenis-kayu'] 					= 'Mjeniskayu/getListDT/$1';
$route['ajax/upStatusJeniskayu']			= 'Mjeniskayu/gantiStatus/$1';
$route['ajax/delJeniskayu'] 				= 'Mjeniskayu/delAkun/$1';

// Link setting produk
$route['setting/produk'] 				= 'Mproduk/showingData/$1';
$route['setting/produk/(:any)']			= 'Mproduk/indexUpdate/$1';
$route['setting/new-produk'] 			= 'Mproduk/insertBaru/$1';
$route['setting/new-produk/proses']		= 'Mproduk/saveNew/$1';
$route['setting/update-produk/proses']	= 'Mproduk/FupdateData/$1';
$route['ajax/produk'] 					= 'Mproduk/getListDT/$1';
$route['ajax/upStatusProduk']			= 'Mproduk/gantiStatus/$1';
$route['ajax/delProduk'] 				= 'Mproduk/delAkun/$1';

// Link setting sortimen
$route['setting/sortimen'] 				= 'Msortimen/showingData/$1';
$route['setting/sortimen/(:any)']		= 'Msortimen/indexUpdate/$1';
$route['setting/new-sortimen'] 			= 'Msortimen/insertBaru/$1';
$route['setting/new-sortimen/proses']	= 'Msortimen/saveNew/$1';
$route['setting/update-sortimen/proses']= 'Msortimen/FupdateData/$1';
$route['ajax/sortimen'] 				= 'Msortimen/getListDT/$1';
$route['ajax/upStatusSortimen']			= 'Msortimen/gantiStatus/$1';
$route['ajax/delSortimen'] 				= 'Msortimen/delAkun/$1';

// Link setting wip
$route['setting/wip'] 				= 'Mwip/showingData/$1';
$route['setting/wip/(:any)']		= 'Mwip/indexUpdate/$1';
$route['setting/new-wip'] 			= 'Mwip/insertBaru/$1';
$route['setting/new-wip/proses']	= 'Mwip/saveNew/$1';
$route['setting/update-wip/proses']	= 'Mwip/FupdateData/$1';
$route['ajax/wip'] 					= 'Mwip/getListDT/$1';
$route['ajax/upStatusWip']			= 'Mwip/gantiStatus/$1';
$route['ajax/delWip'] 				= 'Mwip/delAkun/$1';

// Link setting hscode
$route['setting/hscode'] 				= 'Mhscode/showingData/$1';
$route['setting/hscode/(:any)']			= 'Mhscode/indexUpdate/$1';
$route['setting/new-hscode'] 			= 'Mhscode/insertBaru/$1';
$route['setting/new-hscode/proses']		= 'Mhscode/saveNew/$1';
$route['setting/update-hscode/proses']	= 'Mhscode/FupdateData/$1';
$route['ajax/hscode'] 					= 'Mhscode/getListDT/$1';
$route['ajax/upStatusHscode']			= 'Mhscode/gantiStatus/$1';
$route['ajax/delHscode'] 				= 'Mhscode/delAkun/$1';

// Link setting loading
$route['setting/loading'] 				= 'Mloading/showingData/$1';
$route['setting/loading/(:any)']		= 'Mloading/indexUpdate/$1';
$route['setting/new-loading'] 			= 'Mloading/insertBaru/$1';
$route['setting/new-loading/proses']	= 'Mloading/saveNew/$1';
$route['setting/update-loading/proses'] = 'Mloading/FupdateData/$1';
$route['ajax/loading'] 					= 'Mloading/getListDT/$1';
$route['ajax/upStatusLoading']			= 'Mloading/gantiStatus/$1';
$route['ajax/delLoading'] 				= 'Mloading/delAkun/$1';

// Link setting discharge
$route['setting/discharge'] 				= 'Mdischarge/showingData/$1';
$route['setting/discharge/(:any)']			= 'Mdischarge/indexUpdate/$1';
$route['setting/new-discharge'] 			= 'Mdischarge/insertBaru/$1';
$route['setting/new-discharge/proses']		= 'Mdischarge/saveNew/$1';
$route['setting/update-discharge/proses'] 	= 'Mdischarge/FupdateData/$1';
$route['ajax/discharge'] 					= 'Mdischarge/getListDT/$1';
$route['ajax/upStatusDischarge']			= 'Mdischarge/gantiStatus/$1';
$route['ajax/delDischarge'] 				= 'Mdischarge/delAkun/$1';

// Link setting settingloading
$route['setting/settingloading'] 				= 'Msettingloading/showingData/$1';
$route['setting/settingloading/(:any)']			= 'Msettingloading/indexUpdate/$1';
$route['setting/new-settingloading'] 			= 'Msettingloading/insertBaru/$1';
$route['setting/new-settingloading/proses']		= 'Msettingloading/saveNew/$1';
$route['setting/update-settingloading/proses'] 	= 'Msettingloading/FupdateData/$1';
$route['ajax/settingloading'] 					= 'Msettingloading/getListDT/$1';
$route['ajax/upStatusSettingloading']			= 'Msettingloading/gantiStatus/$1';
$route['ajax/delSettingloading'] 				= 'Msettingloading/delAkun/$1';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
